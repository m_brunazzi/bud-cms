<?php

namespace BloomDesign\Bud\Http\Middleware;

use Closure, Auth, Redirect;

class BloomAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if(!Auth::check() || !Auth::user()->admin)
            return Redirect::route('admin.login');
        return $next($request);
    }
}
