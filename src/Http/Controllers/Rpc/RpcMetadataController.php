<?php 

namespace BloomDesign\Bud\Http\Controllers\Rpc;


use BloomDesign\Bud\Http\Controllers\RpcController;
use Illuminate\Http\Request;
 

use Redirect, Response, Option, File, DB;

use Language;
use Metadata;





class RpcMetadataController extends RpcController {

    /**

     * Handle all requests for tags management

     * @return Response Json reponse 

     */

    public function manager(Request $r){



        try{



            $action = $r->input('action');

            if(!$action)

                throw new \Exception("No action specified");



            switch ($action) {

                case 'getMetadata':

                    $default_meta = Metadata::$default_meta;


                    $hasmeta_id = $r->input('hasmeta_id');
                    $hasmeta_type = $r->input('hasmeta_type');
                    $hasmeta = $hasmeta_type::find($hasmeta_id);
                    
                    if(!$hasmeta)
                        throw new \Exception('Related model not found');


                    $metadatas = $hasmeta->meta;
                    
                    $empty_meta = [];
                     
                    $languages = Language::where('online', '=', '1')->get();
                   


                    // verifico che esista un meta per ogni campo di default, se non esiste lo creo, anche senza salvarlo sul db
                    if($metadatas->isEmpty()){

                        foreach( $languages as $language){
                            $empty_meta[$language->id] = '';
                        }

                        foreach(Metadata::$default_meta as $default_meta){

                            $meta = new Metadata;
                            $meta->hasmeta_id = $hasmeta_id;
                            $meta->hasmeta_type = $hasmeta_type;
                            $meta->name = $default_meta;
                            $meta->data = $empty_meta;

                            $metadatas[] = $meta;
                        }
                    }


                    return $this->render(1, ['metadatas' => $metadatas, 'languages' => $languages], 'Metadata loading OK');
                   
                    break;

                case 'saveMetadatas':
                    $metadatas = $r->input('metadatas');
                    

                    $hasmeta_id = $r->input('hasmeta_id');
                    $hasmeta_type = $r->input('hasmeta_type');
                    $hasmeta = $hasmeta_type::find($hasmeta_id);


                    $current_metadata_ids = [];
                    if( !empty($hasmeta->meta) ){
                        foreach($hasmeta->meta as $meta){
                            $current_metadata_ids[] = $meta->id;
                        }
                    }

                    $new_metadata_ids = [];
                    foreach($metadatas as $metadata){

                        if(!empty($metadata['id'])){
                            $m = Metadata::find($metadata['id']);
                        }
                        else{
                            $m = new Metadata;
                        }

                        $m->name = $metadata['name'];
                        $m->data = json_encode($metadata['data']);
                        $m->hasmeta_id = $r->input('hasmeta_id');
                        $m->hasmeta_type = $r->input('hasmeta_type');

                        $m->save();
                        $new_metadata_ids[] = $m->id;

                        // NON SERVE L'ATTACH visto che tanto gia inserisco manualmente i riferimenti
                        // if(!in_array($m->id, $current_metadata_ids)){
                        //     $hasmeta->meta()->attach($m);
                        // }
                    }


                    $to_remove_ids = array_diff($current_metadata_ids,  $new_metadata_ids);

                    Metadata::whereIn('id', $to_remove_ids)->delete();
                    
                    return $this->render(1, [], 'Metadata saving OK');

                    break;

                default:

                    throw new \Exception("Action not valid");

                    break;

            }

                

        }

        catch(\Exception $e){



            return $this->render(0, [], $e->getMessage(), $r->all());

        }



    }







}

