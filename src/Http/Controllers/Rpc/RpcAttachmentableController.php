<?php 



namespace BloomDesign\Bud\Http\Controllers\Rpc;







use BloomDesign\Bud\Http\Controllers\RpcController;

 

use Input, Redirect, Response, Option, File, DB;





use Attachment;



use Image;



class RpcAttachmentableController extends RpcController {

    



    /**

     * Handle all requests for attachments management

     * @return Response Json reponse 

     */

    public function manager(){



        try{



            $action = Input::get('action');

            if(!$action)

                throw new \Exception("No action specified");





            switch ($action) {

                case 'upload':

                    

                    $destination_path = Input::get('destination_path');

                    if(!$destination_path)

                        throw new \Exception('Destination Path not set');



                    $attachmentable = $this->checkRelated(Input::get('attachmentable_type'), Input::get('attachmentable_id'));                  

                    

                    $file = Input::file('file');

                    $name = Input::get('name');

                    $description = Input::get('description');

                    

                    $record = new Attachment;

                    $record->description = $description;

                    $record->online = true;

                    $record->addFile($file, $destination_path);

                    $record->save();

                    

                    $attachmentable->attachment()->save($record);

                    

                    

                    return $this->render(1, ['file' => $record], 'Upload OK');





                    break;

                case 'attach':

                    

                    $attachmentable = $this->checkRelated(Input::get('attachmentable_type'), Input::get('attachmentable_id'));



                    $attachment_id = Input::get('attachment_id');

                    if(!$attachment_id)

                        throw new \Exception('No Attachment id');

                  

                    $attachment =  Attachment::find($attachment_id);

                    if(!$attachment)

                        throw new \Exception('Attachment not found');

                                

                    $attachmentable->attachment()->attach($attachment->id);

                    

                    return $this->render(1, ['attachment_id' => $attachment->id], 'Attach OK');

                        

                    break;

                case 'detach':



                    $attachmentable = $this->checkRelated(Input::get('attachmentable_type'), Input::get('attachmentable_id'));

                    

                    $attachment_id = Input::get('attachment_id');

                    if(!$attachment_id)

                        throw new \Exception('No Attachment id');



                     $attachment =  Attachment::find($attachment_id);

                    if(!$attachment)

                        throw new \Exception('Attachment not found');

                                

                    $attachmentable->attachment()->detach($attachment->id);

           

                    return $this->render(1, ['attachment_id' => $attachment->id], 'Detach OK');



                    break;

                case 'collection':

                    

                    $attachmentable = $this->checkRelated(Input::get('attachmentable_type'), Input::get('attachmentable_id')); 

                    
                    

                    $attachments = $attachmentable->attachment;
                    $attachments_parsed = Array();

                    foreach($attachments as $item){

                        

                        $item->filename_absolute = asset($item->filename);

                        $item->thumbnail = $item->is_image?

                                            Image::resize($item->filename, Option::value('admin_thumbnail_width'), Option::value('admin_thumbnail_height'), true)

                                            :

                                            '';

                        $attachments_parsed[] = $item;

                    }

                    





                    return $this->render(1, ['attachments' => $attachments_parsed], 'Collection OK');



                    break;

                case 'browse':



                    $attachmentable = $this->checkRelated(Input::get('attachmentable_type'), Input::get('attachmentable_id')); 









                    $data['status'] = 1;

                    $attached = []; 

                    foreach($attachmentable->attachment as $item){

                        $attached[] = $item->id;

                    }

                    


                    $search = Input::get('search');
                    $records_per_page = Input::get('records_per_page', 12);
                    

                    $attachments = $attachmentable->attachment()->paginate($records_per_page);
                    $attachments = Array();

                                

                    $originals = new Attachment;

                    if($search){

                        $originals = $originals->where('label', 'LIKE', '%'.$search.'%')->orWhere('name', 'LIKE', '%'.$search.'%')->orWhere('original_filename', 'LIKE', '%'.$search.'%');

                    };

                    $paginated = $originals->paginate($records_per_page);
                    

                    foreach($paginated as $item){

                        

                        $item->filename_absolute = asset($item->filename);

                        $item->thumbnail = $item->is_image?

                                            Image::resize($item->filename, Option::value('admin_thumbnail_width'), Option::value('admin_thumbnail_height'), true)

                                            :

                                            '';

                        

                        $item->is_attached = in_array($item->id, $attached);

                        

                        

                        $attachments[] = $item;

                    }

                    return $this->render(1, ['attachments' => $attachments, 'total' => $paginated->total(), 'records_per_page' => $records_per_page], 'Browse OK', Array('search' => $search));

                    

                    break;

                case 'delete':

                    $record = Attachment::find(Input::get('id'));

            

                    if(!$record)            

                        throw new \Exception('File not found');

            

                    $record->delete();

            

                    return $this->render(1, [], 'Delete OK');

                    

                    break;

                case 'rename':

                    $record = Attachment::find(Input::get('pk'));

            

                    if(!$record)            

                        throw new \Exception('File not found');

                    

                    $record->name = Input::get('value');

                    $record->save();



                    return $this->render(1, [], 'Rename OK');

                    break;

                case 'setMainImage':



                    $attachmentable = $this->checkRelated(Input::get('attachmentable_type'), Input::get('attachmentable_id')); 





                    $attachmentable_type = Input::get('attachmentable_type');

                    $attachmentable_id = Input::get('attachmentable_id');

                    $attachment_id = Input::get('attachment_id');

                    $main = Input::get('is_image_main')=='true'?true:false;

                    

                    DB::table('blm_attachmentable')

                                ->where('attachment_id', '=', $attachment_id)

                                ->where('attachmentable_type', '=', $attachmentable_type)

                                ->where('attachmentable_id', '=', $attachmentable_id)

                                ->update(['is_image_main' => $main]);

                    





                    if($main == true){

                        DB::table('blm_attachmentable')

                                ->where('attachment_id', '<>', $attachment_id)

                                ->where('attachmentable_type', '=', $attachmentable_type)

                                ->where('attachmentable_id', '=', $attachmentable_id)

                                ->update(['is_image_main' => false]);

                    }



                    

                    return $this->render(1, [], 'Set Main Image OK');

                    break;

                case 'setOrder':

                    

                    break;

                default:

                    throw new \Exception("Action not valid");

                    break;

            }

                

        }

        catch(\Exception $e){



            return $this->render(0, [], $e->getMessage(), Input::all());

        }



    }





/*

    private function upload()

    {

        $data = Array();

            

        try{

            

            $attachmentable_type = Input::get('attachmentable_type');

            $attachmentable_id = Input::get('attachmentable_id');

            $destination_path = Input::get('destination_path');

            

            if(!$destination_path)

                throw new \Exception('Destination Path not set');



            // verifico che esista la classe ricevuta 

            if(!class_exists($attachmentable_type))

                throw new \Exception('Attachmentable Model '.$attachmentable_type.' not found ('.$attachmentable_type.')');

            

            // carico il record indicato

            $attachmentable = $attachmentable_type::find($attachmentable_id);

            if(!$attachmentable)

                throw new \Exception('Attachmentable record ('.$attachmentable_type.' > '.$attachmentable_id.') not found');

            

            // gestisco il file ricevuto

            $file = Input::file('file');

            

            

            $name = Input::get('name');

            $description = Input::get('description');

            

            // creo un nuovo record di tipo File

            $record = new Attachment;

            //$record->name = $name?$name:$file->getClientOriginalName();

            $record->description = $description;

            $record->online = true;

            //$record->attachmentable_id = $attachmentable_id;

            //$record->attachmentable_type = $attachmentable_type;

            $record->addFile($file, $destination_path);

            $record->save();

            

            $attachmentable->attachment()->save($record);

            

            

            $data['status'] = 1;

            $data['file'] = $record;

               

        }

        catch(\Exception $e){

        

            $data['status'] = 0;

            $data['message'] = $e->getMessage();

        }

                

        return Response::json($data);

    }

    

    public function attach()

    {

        $data = Array();

            

        try{

            

            $attachmentable_type = Input::get('attachmentable_type');

            $attachmentable_id = Input::get('attachmentable_id');

            $attachment_id = Input::get('attachment_id');

            

            if(!$attachment_id)

                throw new \Exception('No Attachment id');



            // verifico che esista la classe ricevuta 

            if(!class_exists($attachmentable_type))

                throw new \Exception('Attachmentable Model not found');

            

            // carico il record indicato

            $attachmentable = $attachmentable_type::find($attachmentable_id);

            if(!$attachmentable)

                throw new \Exception('Attachmentable record ('.$attachmentable_type.' > '.$attachmentable_id.') not found');

          

            $attachment =  Attachment::find($attachment_id);

            if(!$attachment)

                throw new \Exception('Attachment not found');

          

                        

            $attachmentable->attachment()->attach($attachment->id);

            

            

            $data['status'] = 1;

            $data['attachment_id'] = $attachment->id;

               

        }

        catch(\Exception $e){

        

            $data['status'] = 0;

            $data['message'] = $e->getMessage();

        }

                

        return Response::json($data);

    }

    

    

    public function detach()

    {

        $data = Array();

            

        try{

            

            $attachmentable_type = Input::get('attachmentable_type');

            $attachmentable_id = Input::get('attachmentable_id');

            $attachment_id = Input::get('attachment_id');

            

            if(!$attachment_id)

                throw new \Exception('No Attachment id');



            // verifico che esista la classe ricevuta 

            if(!class_exists($attachmentable_type))

                throw new \Exception('Attachmentable Model not found');

            

            // carico il record indicato

            $attachmentable = $attachmentable_type::find($attachmentable_id);

            if(!$attachmentable)

                throw new \Exception('Attachmentable record ('.$attachmentable_type.' > '.$attachmentable_id.') not found');

          

            $attachment =  Attachment::find($attachment_id);

            if(!$attachment)

                throw new \Exception('Attachment not found');

          

                        

            $attachmentable->attachment()->detach($attachment->id);

            

            

            $data['status'] = 1;

            $data['attachment_id'] = $attachment->id;

               

        }

        catch(\Exception $e){

        

            $data['status'] = 0;

            $data['message'] = $e->getMessage();

        }

                

        return Response::json($data);

    }

    

    public function collection()

    {

        $data = Array();

        try{

            

            $attachmentable_type = Input::get('attachmentable_type');

            $attachmentable_id = Input::get('attachmentable_id');

            

            // verifico che esista la classe ricevuta 

            if(!class_exists($attachmentable_type))

                throw new \Exception('Eloquent Model '.$attachmentable_type.'not found');

            

            // carico il record indicato

            $attachmentable = $attachmentable_type::with(Array('attachment' => function($query){$query->orderBy('order'); }))->find($attachmentable_id);

            if(!$attachmentable)

                throw new \Exception('Record not found');

            

            $data['status'] = 1;

            

            

            

            $attachments = Array();

            foreach($attachmentable->attachment as $item){

                

                $item->thumbnail = $item->is_image?

                                    Image::resize($item->filename, Option::value('admin_thumbnail_width'), Option::value('admin_thumbnail_height'), true)

                                    :

                                    '';

                $attachments[] = $item;

            }

            $data['data'] = $attachments;

            

        }

        catch(\Exception $e){

            $data['status'] = 0;

            $data['message'] = $e->getMessage();

        }

                

        return Response::json($data);

    }



    

    public function browse()

    {

        $data = Array();

        try{

            $data['status'] = 1;

            $attached = []; // array che contiene gli id dei file associati all'eventuale Attachmentable specificato

            

            

            

            $attachmentable_type = Input::get('attachmentable_type');

            $attachmentable_id = Input::get('attachmentable_id');

            $search = Input::get('search');

            

            if($attachmentable_type && $attachmentable_id){

            

                // verifico che esista la classe ricevuta 

                if(!class_exists($attachmentable_type))

                    throw new \Exception('Eloquent Model '.$attachmentable_type.'not found');



                // carico il record indicato



                $attachmentable = $attachmentable_type::with(Array('attachment' => function($query){$query->orderBy('order'); }))->find($attachmentable_id);

                if(!$attachmentable)

                    throw new \Exception('Record not found');

                

                

                foreach($attachmentable->attachment as $item){

                    $attached[] = $item->id;

                }



            }

            $attachments = Array();

                        

            $originals = new Attachment;

            if($search){

                $originals = $originals->where('name', 'LIKE', '%'.$search.'%');

            };

            

            foreach($originals->get() as $item){

                

                $item->thumbnail = $item->is_image?

                                    Image::resize($item->filename, Option::value('admin_thumbnail_width'), Option::value('admin_thumbnail_height'), true)

                                    :

                                    '';

                

                

                $item->is_attached = in_array($item->id, $attached);

                

                

                $attachments[] = $item;

            }

            $data['data'] = $attachments;

            $data['search'] = $search;

            

        }

        catch(\Exception $e){

            $data['status'] = 0;

            $data['message'] = $e->getMessage();

        }

                

        return Response::json($data);

    }



    

    

    

            

    

    

    

    public function delete()

    {

        try{

            

            $record = Attachment::find(Input::get('id'));

            

            if(!$record)            

                throw new \Exception('File not found');

            

            $record->delete();

            

            $data['status'] = 1;

            $data['message'] = 'Immagine cancellata correttamente';

               

        }

        catch(\Exception $e){

        

            $data['status'] = 0;

            $data['message'] = $e->getMessage();

        }

                

        return Response::json($data);

    }

    

    

    public function rename()

    {

        try{

            

           $record = Attachment::find(Input::get('pk'));

            

            if(!$record)            

                throw new \Exception('File not found');

            

            $record->name = Input::get('value');

            $record->save();

            

            $data['status'] = 1;

            $data['message'] = 'File rinominato correttamente';

               

        }

        catch(\Exception $e){

        

            $data['status'] = 0;

            $data['message'] = $e->getMessage();

        }

                

        return Response::json($data);

    }

    

    public function setMainImage()

    {

        try{

            

            $attachmentable_type = Input::get('attachmentable_type');

            $attachmentable_id = Input::get('attachmentable_id');

            $attachment_id = Input::get('attachment_id');

            $main = Input::get('is_image_main')=='true'?true:false;

            

            

            

//            DB::enableQueryLog();

            DB::table('blm_attachmentable')

                        ->where('attachment_id', '=', $attachment_id)

                        ->where('attachmentable_type', '=', $attachmentable_type)

                        ->where('attachmentable_id', '=', $attachmentable_id)

                        ->update(['is_image_main' => $main]);

            

            if($main == true){

                DB::table('blm_attachmentable')

                        ->where('attachment_id', '<>', $attachment_id)

                        ->where('attachmentable_type', '=', $attachmentable_type)

                        ->where('attachmentable_id', '=', $attachmentable_id)

                        ->update(['is_image_main' => false]);

            }

            

//            print_r(DB::getQueryLog());

//            die();

            

            $data['status'] = 1;

            $data['attachment_id'] = $attachment_id;

            $data['attachmentable_id'] = $attachmentable_id;

            $data['attachmentable_type'] = $attachmentable_type;

            $data['is_image_main'] = $main;

            $data['message'] = 'Stato modificato correttamente';

               

        }

        catch(\Exception $e){

            $data['status'] = 0;

            $data['message'] = $e->getMessage();

        }

                

        return Response::json($data);

    }

    

    

    public function setOrder()

    {

        try{

            $attachment_ids = Input::get('attachment_ids');

            $attachmentable_type = Input::get('attachmentable_type');

            $attachmentable_id = Input::get('attachmentable_id');

           

            $counter = 0;

            if(empty($attachment_ids))

                throw new \Exception('Nessun allegato specificato');

            

            foreach($attachment_ids as $item){

                DB::table('blm_attachmentable')

                        ->where('attachment_id', '=', $item)

                        ->where('attachmentable_type', '=', $attachmentable_type)

                        ->where('attachmentable_id', '=', $attachmentable_id)

                        ->update(['order' => $counter]);

                $counter++;

            }

            $data['status'] = 1;

            $data['attachmentable_id'] = $attachmentable_id;

            $data['attachmentable_type'] = $attachmentable_type;

            $data['attachment_ids'] = implode(',', $attachment_ids);

            $data['message'] = 'Ordine modificato correttamente';

        }

        catch(\Exception $e){

            $data['status'] = 0;

            $data['message'] = $e->getMessage();

        }

                

        return Response::json($data);

    }

    */



}

