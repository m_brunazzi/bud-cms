<?php 



namespace BloomDesign\Bud\Http\Controllers\Rpc;



use BloomDesign\Bud\Http\Controllers\RpcController;

 

use BaseController, Form, Input, Redirect, Response, Option;



use Language;

use Tag;

use Article;

use Retailer;



class RpcTypeaheadController extends RpcController {

    

    

    public function __construct(){

        Language::initialize();

    }

    

    public function query($t)

    {

        $result = Array();

            

        try{

            

            $q = Input::get('q');

            switch($t){

                case 'tag':

                    $rows = Tag::where('label', 'LIKE', '%'.$q.'%')->get();

                    $result['type'] = 'tag';

                    $result['results'] = count($rows);

                    $result['data'] = $rows;

                    $result['status'] = 1;

                    break;

                case 'article':

                    Article::translate();

                    $rows = Article::where('label', 'LIKE', '%'.$q.'%')->orWhere('name', 'LIKE', '%'.$q.'%')->get();

                    $result['type'] = 'article';

                    $result['results'] = count($rows);

                    $result['data'] = $rows;

                    $result['status'] = 1;

                    break;

                case 'article_category':

                    Category::translate();

                    $rows = Category::where('label', 'LIKE', '%'.$q.'%')->orWhere('name', 'LIKE', '%'.$q.'%')->get();

                    $result['type'] = 'article_category';

                    $result['results'] = count($rows);

                    $result['data'] = $rows;

                    $result['status'] = 1;

                    break;

                default:

                    throw new \Exception('Richiesta non valida');

            }

        }

        catch(\Exception $e){

            $result['status'] = 0;

            $result['message'] = $e->getMessage();

        }

        return Response::json($result);

    }

    

}



