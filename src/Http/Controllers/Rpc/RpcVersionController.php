<?php 
namespace BloomDesign\Bud\Http\Controllers\Rpc;



use BloomDesign\Bud\Http\Controllers\RpcController;

use Response, Input, Option, \Carbon\Carbon, DB, Log;
use Language, Version, User;

class RpcVersionController extends RpcController {

    function manager(){
         try{

            Language::initialize();

            $action = Input::get('action');
            if(!$action)
                throw new \Exception("No action specified");


            switch ($action) {

                case 'getVersions':
                    $versionable_id = Input::get('versionable_id');
                    $versionable_type = Input::get('versionable_type');
                    $versionable = $versionable_type::find($versionable_id);


                    if(!$versionable)
                        throw new \Exception('Versionable model not found');


                    $versions = $versionable->version;

                    //$versions = $versions->sortByDesc('created_at');

                    if(!empty($versions) && $versions->isNotEmpty()){
                        $versions = $versions->map(function($item){

                            $user = User::find($item->user_id);
                            $item->user = $user;

                            $preview_url = $item->versionable->getPreviewUrl($item->id);
                            $item->preview_url = $preview_url;
                            return $item;
                        });
                    }
                    

                    return $this->render(1, ['versions' => $versions, 'versioning_copies' => $versionable_type::$versioning_copies], 'Versions loading OK', Input::all());
                case 'deleteVersion':

                    $version_id = Input::get('version_id');
                    $version = Version::find($version_id);
                    if(!$version)
                        throw new \Exception('Version not found');
                    $version->delete();


                    return $this->render(1, [], 'Version '.$version_id.' deleted', Input::all());
                    break;
                case 'restoreVersion':
                    $version_id = Input::get('version_id');
                    $version = Version::find($version_id);
                    if(!$version)
                        throw new \Exception('Version not found');
                    
                    $version->restore();

                    return $this->render(1, [], 'Version '.$version_id.' restored', Input::all());
                    break;
                // case 'getPreviewUrl':
                //     $version_id = Input::get('version_id');
                //     $version = Version::find($version_id);

                //     if(!$version)
                //         throw new \Exception('Version not found');
                    
                //     $preview_url = $version->versionable()->getPreviewUrl();
                //     return $this->render(1, ['preview_url' => $preview_url], 'Version '.$version_id.' preview url loaded', Input::all());
                //     break;
                case 'saveNotes':
                    $version_id = Input::get('version_id');
                    $notes = Input::get('notes');
                    $version = Version::find($version_id);
                    if(!$version)
                        throw new \Exception('Version not found');
                    $version->notes = $notes;
                    $version->save();
                    
                    return $this->render(1, [], 'Notes saved for version '.$version_id, Input::all());
                    break;
                


                default:
                    throw new \Exception('Action not valid');
                    break;
            }
        }
        
        catch(\Exception $e){

            return $this->render(0, [], $e->getMessage(), Input::all());
        }
    }


}