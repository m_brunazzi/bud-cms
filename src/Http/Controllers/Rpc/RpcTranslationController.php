<?php 
namespace BloomDesign\Bud\Http\Controllers\Rpc;



use BloomDesign\Bud\Http\Controllers\RpcController;

use Response, Input, Option, \Carbon\Carbon;
use \BloomDesign\Bud\App\Models\Language;
use Lang;

class RpcTranslationController extends RpcController {

    private $translations_path = '';
    private $translations_path_test = '';
    private $canWrite = null;

    function __construct(){
        $this->translations_path = base_path ().'/resources/lang/';
        $this->translations_path_test = base_path ().'/resources/lang.test/';

        $this->canWrite = is_writable($this->translations_path);
    }

    function manager(){
         try{

            $action = Input::get('action');
            if(!$action)
                throw new \Exception("No action specified");


            switch ($action) {
                case 'loadGroups':
                    



                    break;
                case 'loadTranslations':
                    $selected_group = Input::get('group');
                    $translations_path = $this->translations_path;
                            
                    $language_codes = [];
                    $languages = Language::all();

                    // Loading DB languages codes
                    foreach($languages as $language){
                        $language_codes[] = $language->code;
                    }

                    // Loading filesystem languages
                    $dirs = array_filter(glob($this->translations_path.'*'), 'is_dir');
                    foreach($dirs as $item){
                        $code = basename($item);
                        if(!in_array($code, $language_codes))
                            $language_codes[] = $code;
                    }
                    
                    
                    $groups = Array();
                    $translations = Array();
                    $keys = [];

                    $tmp = [];

                    foreach($language_codes as $code){
                        
                        $translations_dotted = [];
                        

                        foreach (glob($this->translations_path.$code."/*.php") as $file) {
                            $group = basename($file, '.php');

                            if(!in_array($group, $groups))
                                    $groups[] = $group;

                            if(!$selected_group || $selected_group == $group){


                                $lang_file = Lang::getLoader()->load($code, $group);

                                $translations_dotted = array_dot(array($group => $lang_file));
                                $keys = array_merge($keys, array_keys($translations_dotted));
                                
                            }

                            foreach($translations_dotted as $key => $value){
                                $tmp[$key][$code] = $value;
                                
                            }

                        }

                    }

                    foreach($tmp as $key => $value){
                        $translations[] = Array(
                                            'key' => $key,
                                            'translation' => $value
                                            );
                    }


                    return $this->render(1, ['keys' => $keys, 'canWrite' => $this->canWrite, 'translations' => $translations, 'languages' => $language_codes, 'groups' => $groups], 'Traduzioni caricate correttamente', Input::all());
                    break;

                case 'saveTranslations':


                    $translations = Input::get('translations');

                    $data = [];
                    if(count($translations) > 0){
                        foreach($translations as $row){

                            if(isset($row['translation']) && count($row['translation']) > 0){
                                foreach($row['translation'] as $language => $value){

                                    $tokens = explode('.', $row['key']);
                                    $group = array_shift($tokens);
                                    $slug_reduced = implode('.', $tokens);
                                    $data[$language][$group][$slug_reduced] = $value;
                                }
                            }
                        }
                    }
                    
                    if(!empty($data)){
                        
                        foreach($data as $language => $row){
                            foreach($row as $group => $values_dot){
                                
                                $contents = "<?php\n\nreturn ";
                                
                                $values = array();
                                foreach ($values_dot as $key => $value) {
                                    array_set($values, $key, $value);
                                }
                                $contents .= var_export($values, TRUE).";\n";

                                $destination_folder = $this->translations_path.$language;

                                if(!file_exists($destination_folder)){
                                    mkdir($destination_folder, 0777, true);
                                }
                                
                                $output_file = $destination_folder."/{$group}.php";
                                
                                $fp = fopen($output_file, "w");
                                fputs($fp, $contents);
                                fclose($fp);
                                
                            }
                        }
                        
                    }

                    return $this->render(1, [], 'Traduzioni salvate correttamente', Input::all());
                    break;
                default:
                    throw new \Exception('Action not valid');
                    break;
            }
        }
        
        catch(\Exception $e){

            return $this->render(0, [], $e->getMessage(), Input::all());
        }
    }



}