<?php 

namespace BloomDesign\Bud\Http\Controllers\Rpc;



use BloomDesign\Bud\Http\Controllers\RpcController;
 
use Input, Redirect, Response, Option, File, DB;


use Password;


class RpcPasswordableController extends RpcController {
    

    

    /**
     * Handle all requests for password management
     * @return Response Json reponse 
     */
    public function manager(){

        try{

            $action = Input::get('action');
            if(!$action)
                throw new \Exception("No action specified");


            switch ($action) {
                case 'set':
                    
                    $passwordable = $this->checkRelated(Input::get('passwordable_type'), Input::get('passwordable_id'));      

                    $has_password = Input::get('has_password');
                    $password = Input::get('password');
            
                    if($has_password == 1){
                        $p = $passwordable->password;
                        if(!$p)
                            $p = new Password;
            
                        $p->password = md5($password);
                        $p->save();
            
                        $passwordable->password()->save($p);

                        return $this->render(1, [], 'Password SET OK');
                    }
                    else{
                        $p = $passwordable->password;
                        if($p)
                            $p->delete();
            
                        return $this->render(1, [], 'Password UNSET OK');
                    }
           
                break;
                default:
                    throw new \Exception("Action not valid");
                    break;
            }
                
        }
        catch(\Exception $e){

            return $this->render(0, [], $e->getMessage(), Input::all());
        }

    }



}
