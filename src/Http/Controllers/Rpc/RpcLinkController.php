<?php 
namespace BloomDesign\Bud\Http\Controllers\Rpc;



use BloomDesign\Bud\Http\Controllers\RpcController;

use Response, Input, Option, \Carbon\Carbon, DB, Log;
use Language, Link;

class RpcLinkController extends RpcController {

    function manager(){
         try{

            Language::initialize();

            $action = Input::get('action');
            if(!$action)
                throw new \Exception("No action specified");


            switch ($action) {

                /*
                case 'getLanguages':
                  
                    $languages = Language::where('online', '=', '1')->get();
                    return $this->render(1, ['languages' => $languages], 'Languages load OK', Input::all());
                    break;
                  */  

                case 'getLinks':
                    $linkable_id = Input::get('linkable_id');
                    $linkable_type = Input::get('linkable_type');
                    $linkable = $linkable_type::find($linkable_id);


                    
                    if(!$linkable)
                        throw new \Exception('Linkable model not found');


                    $links = $linkable->link;
                    // verifico che esista un link per ogni lingua, se non esiste lo creo, anche senza salvarlo sul db
                     
                    $languages = Language::where('online', '=', '1')->get();
                    $language_names  = [];
                    foreach( $languages as $language){
                        $language_names[$language->id] = $language->language;
                    }

                    if($links){
                        foreach($languages as $language){

                            $link = $links->filter(function($item) use($language){
                                return $item->language_id == $language->id;
                            })->first();

                            if(!$link){
                                $new = new Link;
                                $new->language_id = $language->id;
                                $new->link = '';

                                $links->push($new);
                            }
                        }
                    }

                    $sorted = $links->sortBy('language_id');
                    //dd($links);

                    return $this->render(1, ['links' => $sorted, 'languages' => $language_names, 'base_url' => url('/')], 'Links loading OK', Input::all());
                   
                    break;
                case 'createLink':



                    $language_id = Input::get('language_id');
                    if(!$language_id)
                        throw new \Exception('No language specified');

                    $linkable_id = Input::get('linkable_id');
                    $linkable_type = Input::get('linkable_type');
                    $linkable = $linkable_type::find($linkable_id);
                    
                    if(!$linkable)
                        throw new \Exception('Linkable model not found');


                    $link = $linkable->createLink($language_id);
                    if(!$link){
                        throw new \Exception('Cannot create link, please check record translations');
                    }

                    return $this->render(1, ['link' => $link], 'Link creation OK', Input::all());
                    break;

                case 'saveLink':

                     $errors = []; // in questo array memorizzo gli eventuali link che non vanno bene perche gia esistenti



                    $linkable_id = Input::get('linkable_id');
                    $linkable_type = Input::get('linkable_type');
                    $linkable = $linkable_type::find($linkable_id);

                    if(!$linkable)
                        throw new \Exception('Linkable model not found');


                    $link = Input::get('link');

                    

                    if(isset($link['id'])){
                        $l = Link::find($link['id']);
                    }
                    else{
                        $l = new Link;
                    }


                    
                    // controllo che il link non esista gia
                    
                    
                   if(Link::check($link['link'], $linkable)){

                        if($link['link'] != ''){
                            $l->link = $link['link'];
                            $l->language_id = $link['language_id'];
                            $l->save();  

                            $linkable->link()->save($l); 
                        }
                        else if(!empty($l->id)){
                            // se il link ricevuto è vuoto, lo cancello dal db
                            $l->delete();
                        }
                   }
                   else{
                        $errors[$link['language_id']] = $link['link'];
                   }

                    

                    

                    if(empty($errors))
                        return $this->render(1, ['errors' => $errors], 'Link saving OK', Input::all());
                    else
                        return $this->render(0, ['errors' => $errors], 'Please check links marked in red', Input::all());

                        

                    

                    break;
                
                case 'saveLinks':


                    $errors = []; // in questo array memorizzo gli eventuali link che non vanno bene perche gia esistenti



                    $linkable_id = Input::get('linkable_id');
                    $linkable_type = Input::get('linkable_type');
                    $linkable = $linkable_type::find($linkable_id);

                    if(!$linkable)
                        throw new \Exception('Linkable model not found');


                    $links = Input::get('links');

                    foreach($links as $link){

                        if(isset($link['id'])){
                            $l = Link::find($link['id']);
                        }
                        else{
                            $l = new Link;
                        }


                        $tmp = $link['link'];
                        // controllo che il link non esista gia
                        
                       if(Link::check($tmp, $linkable)){
                            if($tmp != ''){
                                $l->link = $tmp;
                                $l->language_id = $link['language_id'];
                                $l->save();  

                                $linkable->link()->save($l); 
                            }
                            else if(!empty($l->id)){
                                // se il link ricevuto è vuoto, lo cancello dal db
                                $l->delete();
                            }
                       }
                       else{
                            $errors[] = $tmp;
                       }

                    }

                    

                    if(empty($errors))
                        $message = 'Links saving OK';
                    else
                        $message = 'Please check links marked in red';

                        

                    return $this->render(1, ['errors' => $errors], $message, Input::all());
                    
                   
                    break;
              

                default:
                    throw new \Exception('Action not valid');
                    break;
            }
        }
        
        catch(\Exception $e){

            return $this->render(0, [], $e->getMessage(), Input::all());
        }
    }


}