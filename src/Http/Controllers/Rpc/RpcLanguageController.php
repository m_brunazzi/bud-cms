<?php 
namespace BloomDesign\Bud\Http\Controllers\Rpc;



use BloomDesign\Bud\Http\Controllers\RpcController;

use Response, Input, Option, \Carbon\Carbon, DB;
use Language;

class RpcLanguageController extends RpcController {

    function manager(){
         try{

            Language::initialize();

            $action = Input::get('action');
            if(!$action)
                throw new \Exception("No action specified");


            switch ($action) {
                case 'getLanguages':
                    
                    $languages = Language::where('online', '=', '1')->get();
                    return $this->render(1, ['languages' => $languages], 'Languages load OK', Input::all());
                    break;
                

                default:
                    throw new \Exception('Action not valid');
                    break;
            }
        }
        
        catch(\Exception $e){

            return $this->render(0, [], $e->getMessage(), Input::all());
        }
    }


}