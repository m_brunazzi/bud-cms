<?php 
namespace BloomDesign\Bud\Http\Controllers\Rpc;



use BloomDesign\Bud\Http\Controllers\RpcController;

use Response, Input, Option, \Carbon\Carbon;
//use App\Models\Contact;
use Menu, MenuNode;

class RpcMenuController extends RpcController {

    private $menu_id;

    function manager(){
         try{

            $action = Input::get('action');
            if(!$action)
                throw new \Exception("No action specified");


            switch ($action) {

                case 'getNode':
                    $node_id = Input::get('node_id');

                    $node = MenuNode::find($node_id);
                    return $this->render(1, ['node' => $node], 'Node load OK');
                    break;
                case 'deleteNode':
                    $node_id = Input::get('node_id');

                    $node = MenuNode::find($node_id)->delete();
                    return $this->render(1, ['node_id' => $node_id], 'Node delete OK');
                    break;
                
                case 'getNodes':

                    $this->menu_id = Input::get('menu_id');
                    $language_id = Input::get('language_id');

                    if(!$this->menu_id  || !$language_id)
                        throw new \Exception('Wrong Parameters');

                    $nodes = Menu::find($this->menu_id)->getNodes($language_id);
           
                    return $this->render(1, ['nodes' => $nodes], 'Menu load OK');
                    break;
                case 'saveNode':
                    $data = Input::get('node');
                    
                    if(!empty($data['id'])){
                        $node = MenuNode::find($data['id']);
                    }
                    else{
                        $node = new MenuNode;
                    }

                    $node->title = @$data['title'];
                    $node->url = @$data['url'];
                    $node->url_blank_page = @$data['url_blank_page'];
                    $node->parent_id = @$data['parent_id'];
                    $node->menu_id = @$data['menu_id'];
                    $node->language_id = @$data['language_id'];
                    $node->extra = @$data['extra'];

                    $node->save();

                    return $this->render(1, ['node' => $node], 'Taxonomy save OK');


                    break;
                case 'saveTaxonomy':

                    $this->menu_id = Input::get('menu_id');
                    $language_id = Input::get('language_id');
                    $taxonomy = Input::get('taxonomy');
                    if(!$taxonomy){
                        throw new \Exception('Empty Taxonomy');
                    }
                    //parse_str($taxonomy, $data);
                    

                    
                    $this->saveTaxonomy($taxonomy);

                    
                   
                    $nodes = Menu::find($this->menu_id)->getNodes($language_id,0,true);


                    return $this->render(1, ['nodes' => $nodes], 'Taxonomy save OK');
                    break;
                default:
                    throw new \Exception('Action not valid');
                    break;
            }
        }
        
        catch(\Exception $e){

            return $this->render(0, [], $e->getMessage(), Input::all());
        }
    }

    private function saveTaxonomy($taxonomy, $parent_id = 0){
        
        if(count($taxonomy) > 0){
                        
            $order = 0;
            foreach($taxonomy as $tax_node){
                
                $node = MenuNode::find($tax_node['id']);
                if($node && $node->menu_id = $this->menu_id){
                    $node->order = $order++;
                    $node->parent_id = $parent_id;
                    $node->save();

                    if(isset($tax_node['children']) && count($tax_node['children']) > 0){
                        $this->saveTaxonomy($tax_node['children'], $tax_node['id']);
                    }

                }
               
            }
            return true;
        }
        else return false;
    }

}