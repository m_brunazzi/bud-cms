<?php 

namespace BloomDesign\Bud\Http\Controllers\Rpc;


use BloomDesign\Bud\Http\Controllers\RpcController;

 

use Input, Redirect, Response, Option, File, DB;

use Tag;





class RpcTaggableController extends RpcController {

    /**

     * Handle all requests for tags management

     * @return Response Json reponse 

     */

    public function manager(){



        try{



            $action = Input::get('action');

            if(!$action)

                throw new \Exception("No action specified");





            switch ($action) {

                case 'loadFavourites':
                    $favourites = Tag::favourites(10, 'App\Models\Article')->map(function($item){
                        return $item->label;
                    });

                    return $this->render(1, ['favourites' => $favourites], 'Favourite Tags loading OK');

                    break;

                case 'loadTags':
                    $taggable = $this->checkRelated(Input::get('taggable_type'), Input::get('taggable_id'));      

                    $tags = $taggable->tag->map(function($item){
                        return $item->label;
                    });

                    return $this->render(1, ['tags' => $tags], 'Tags loading OK');

                case 'saveTags':

                    $taggable = $this->checkRelated(Input::get('taggable_type'), Input::get('taggable_id'));      
                    
                    $tags = Tag::find_or_create(Input::get('tags'));
                    $taggable->tag()->sync($tags);
                    return $this->render(1, [], 'Tags saving OK');
                break;

                default:

                    throw new \Exception("Action not valid");

                    break;

            }

                

        }

        catch(\Exception $e){



            return $this->render(0, [], $e->getMessage(), Input::all());

        }



    }







}

