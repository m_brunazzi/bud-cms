<?php 
namespace BloomDesign\Bud\Http\Controllers\Rpc;



use BloomDesign\Bud\Http\Controllers\RpcController;

use Response, Input, Option, \Carbon\Carbon;
use \BloomDesign\Bud\App\Models\Language;
use Lang, Carousel, CarouselSlide;

class RpcCarouselController extends RpcController {


    

    function manager(){
         try{

            $action = Input::get('action');
            if(!$action)
                throw new \Exception("No action specified");


            switch ($action) {
              
                case 'loadSlides':

                    $carousel_id = Input::get('carousel_id');
                    if(!$carousel_id)
                        throw new \Exception("Invalid Carousel ID");

                    $slides = CarouselSlide::where('carousel_id', '=', $carousel_id)->orderBy('order')->get();
                    
                    return $this->render(1, ['slides' => $slides], 'Slides caricate correttamente', Input::all());
                    break;
                case 'editSlide':

                    $slide_id = Input::get('slide_id');
                    if(!$slide_id)
                        throw new \Exception("Invalid Slide ID");

                    $slide = CarouselSlide::find($slide_id);
                    if(!$slide)
                        throw new \Exception("No Slide found with ID ".$slide_id);                    

                    $form_url = route('admin.CarouselSlide.edit', $slide_id);
                    
                    
                    return $this->render(1, ['form_url' => $form_url], 'Reindirizzamento al modulo per la modifica', Input::all());
                    break;
                case 'createSlide':

                    $form_url = route('admin.CarouselSlide.create').'?carousel_id='.Input::get('carousel_id');
                    
                    
                    return $this->render(1, ['form_url' => $form_url], 'Reindirizzamento al modulo per la creazione', Input::all());
                    break;
                case 'saveOnline':

                    $slide_id = Input::get('slide_id');
                    if(!$slide_id)
                        throw new \Exception("Invalid Slide ID");

                    $slide = CarouselSlide::find($slide_id);
                    if(!$slide)
                        throw new \Exception("No Slide found with ID ".$slide_id);                    

                    $slide->online = Input::get('online');
                    $slide->save();
                                        
                    return $this->render(1, [], 'Stato aggiornato correttamente', Input::all());
                    break;

                case 'deleteSlide':

                    $slide_id = Input::get('slide_id');
                    if(!$slide_id)
                        throw new \Exception("Invalid Slide ID");

                    $slide = CarouselSlide::find($slide_id);
                    if(!$slide)
                        throw new \Exception("No Slide found with ID ".$slide_id);                    

                    //$slide->delete();
                    
                    return $this->render(1, [], 'Slide cancellata correttamente', Input::all());
                    break;

                case 'saveCarousel':

                    $taxonomy = Input::get('taxonomy');
                    if(!$taxonomy)
                        throw new \Exception("Invalid Taxonomy");

                    $carousel_id = Input::get('carousel_id');
                    if(!$carousel_id)
                        throw new \Exception("Invalid Taxonomy");

                    parse_str($taxonomy, $data);

                    $order = 0;

                    

                    if(count($data['slide']) > 0){
                        

                        foreach($data['slide'] as $index => $id){

                            $slide = CarouselSlide::find($id);
                            if($slide && $slide->carousel_id = $carousel_id){
                                $order++;
                                $slide->order = ++$order;
                                
                                $slide->save();    
                            }
                        }
                    }

                    
                    return $this->render(1, [], 'Ordinamento salvato correttamente', Input::all());
                    break;

                default:
                    throw new \Exception('Action not valid');
                    break;
            }
        }
        
        catch(\Exception $e){

            return $this->render(0, [], $e->getMessage(), Input::all());
        }
    }



}