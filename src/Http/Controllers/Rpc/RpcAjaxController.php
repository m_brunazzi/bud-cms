<?php 

namespace BloomDesign\Bud\Http\Controllers\Rpc;


use BloomDesign\Bud\Http\Controllers\RpcController;
 
use BaseController, Form, Input, Redirect, Response, Option;

use Language;
use Tag;
use Article;
use Category;

class RpcAjaxController extends RpcController {
    
    
    public function __construct(){
        Language::initialize();
    }
    
    /*
    public function query()
    {
        $result = Array();
            
        try{
            $t = Input::get('t');
            $q = Input::get('q');
            switch($t){
                case 'tag':
                    $rows = Tag::where('name', 'LIKE', '%'.$q.'%')->get();
                    $result['type'] = 'tag';
                    $result['results'] = count($rows);
                    $result['data'] = $rows;
                    $result['status'] = 1;
                    break;
                case 'article':
                    Article::translate();
                    $rows = Article::where('label', 'LIKE', '%'.$q.'%')->orWhere('name', 'LIKE', '%'.$q.'%')->get();
                    $result['type'] = 'article';
                    $result['results'] = count($rows);
                    $result['data'] = $rows;
                    $result['status'] = 1;
                    break;
                case 'article_category':
                    Category::translate();
                    $rows = Category::where('label', 'LIKE', '%'.$q.'%')->orWhere('name', 'LIKE', '%'.$q.'%')->get();
                    $result['type'] = 'article_category';
                    $result['results'] = count($rows);
                    $result['data'] = $rows;
                    $result['status'] = 1;
                    break;
                default:
                    throw new \Exception('Richiesta non valida');
            }
        }
        catch(\Exception $e){
            $result['status'] = 0;
            $result['message'] = $e->getMessage();
        }
        return Response::json($result);
    }
    */

    public function html_editor_upload_image(){

        $data = [];

        try{

            $image = Input::file('image');
            if(!$image){
                throw new \Exception('Immagine non valida');
            }

            $destination_folder = Option::value('images_folder');
            $filename = uniqid().'.'.$image->getClientOriginalExtension();
            $image->move($destination_folder, $filename);
            $data['filename'] = asset($destination_folder.'/'.$filename);

            return $this->render(1, $data, 'Image uploaded');

        }
        catch(\Exception $e){
            return $this->render(0, [], $e->getMessage(), Input::all());
        }


        

    }


    public function rpc_test(){

        return $this->render(1, [], 'Request OK', Input::all());
    }
}

