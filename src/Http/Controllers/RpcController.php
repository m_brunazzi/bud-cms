<?php 

namespace BloomDesign\Bud\Http\Controllers;

use Response;

class RpcController extends BaseController {

     private $status = null; // 0 -> Error | 1 -> Success
     private $data = null; // Response data
     private $message = null; // optional message
     private $parameters = null; // Request parameters
     


     public function setData($status = 0, $data = [], $message = 'Wrong Request', $parameters = []){

     	if($status === 1 || $status === 0)
     		$this->status = $status;

     	if(is_array($data) && $data != [])
     		$this->data = $data;

     	if($message != '')
     		$this->message = $message;

     	if(is_array($parameters) && $parameters != [])
     		$this->parameters = $parameters;
     }


     public function render($status = 0, $data = [], $message = 'Wrong Request', $parameters = []){



     	if($status === 1 || $status === 0)
     		$this->status = $status;

     	if(is_array($data) && $data != [])
     		$this->data = $data;

     	if($message != '')
     		$this->message = $message;

     	if(is_array($parameters) && $parameters != [])
     		$this->parameters = $parameters;

     	$response = Array(
     		'status' => $this->status,
     		'data' => $this->data,
     		'message' => $this->message,
     		'parameters' => $this->parameters,
            'time' => time()
     		);

     	return Response::json($response);

     }

     protected function checkRelated($type, $id){
        try{

           
            // Check for existing class 
            if(!class_exists($type))
                throw new \Exception('Related Model '.$type.' not found');
            
            // Try to load attachmentable
            $related = $type::find($id);
            if(!$related)
                throw new \Exception('Attachmentable record ('.$type.' > '.$id.') not found');

            return $related;
        }
        catch(\Exception $e){
            throw $e;
        }
    }

 }