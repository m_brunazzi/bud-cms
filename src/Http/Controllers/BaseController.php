<?php

namespace BloomDesign\Bud\Http\Controllers;

abstract class BaseController extends \App\Http\Controllers\Controller
{
    
    public $head_assets = Array();
    
    public function __construct()
    {
       
        
    }
    
    function asset_css($css){
        $this->head_assets['css'][] = $css;
    }

    function asset_js($js){
        $this->head_assets['js'][] = $js;
    }
    
}
