<?php 

namespace BloomDesign\Bud\Http\Controllers;

use Input;
use View;
use Auth;
use Language;
use MenuNode;

class PublicController extends BaseController {
        
        var $query_string = ''; // Parametri ricevuti in get senza il parametro page (per la paginazione)
        var $search_string = ''; 
        var $language_current = null;

        function __construct(){


            $this->middleware(function ($request, $next) {
                Language::initialize();
            
                $language = Input::get('lang');
                if($language){
                    $this->language_current = Language::setCurrent($language);
                }
                else{
                    $this->language_current = Language::getCurrent();
                }

                return $next($request);
            });
            
            // View::share('head_assets',  $this->head_assets);
            // View::share('languages',  Language::where('online',1)->get());
            // View::share('language_current', Language::getCurrent());
            View::share('user',  Auth::user());
                        
            View::composer('www.*', function($view)
            {
                $view->with('head_assets',  $this->head_assets);
                $view->with('languages',  Language::where('online',1)->get());
                $view->with('language_current', Language::getCurrent());
                //$view->with('user',  Auth::user());
            });
            
            
            
        }

       

}