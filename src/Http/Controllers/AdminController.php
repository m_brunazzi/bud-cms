<?php 

namespace BloomDesign\Bud\Http\Controllers;


use Language;
use Input, View, Form, Auth;



class AdminController extends BaseController {
                            
            
    
        
	public $search_string = '';
	public $query_string = ''; // Parametri ricevuti in get senza il parametro page (per la paginazione)
        
        public $advanced_Option = false;
        public $languages;
        
        function __construct(){
            
            Language::initialize();
            
            
            $this->languages = Language::getLanguagesOptions();
            
            $this->search_string = Input::get('search');
            
            
            //$this->asset_js(asset('bower_components/jquery/dist/jquery.min.js'));
            
            //$this->asset_js(asset('bower_components/lodash/dist/lodash.min.js'));
            
            //$this->asset_js(asset('bower_components/tinymce/tinymce.min.js'));
            //$this->asset_js(asset('bower_components/tinymce/jquery.tinymce.min.js'));
            
            

            //$this->asset_js(asset('bower_components/bootstrap-sass/assets/javascripts/bootstrap.js'));
            //$this->asset_css(asset('assets/css/bootstrap.min.css'));




            
            // $this->asset_js(asset('bower_components/tagmanager/tagmanager.js')); // TAGS
            // $this->asset_css(asset('bower_components/tagmanager/tagmanager.css')); // TAGS

            // $this->asset_js(asset('bower_components/typeahead.js/dist/typeahead.bundle.js'));

            // $this->asset_js(asset('bower_components/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js'));
            // $this->asset_css(asset('bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css'));
            
            // $this->asset_js(asset('bower_components/toastr/toastr.min.js'));
            // $this->asset_css(asset('bower_components/toastr/toastr.min.css'));
            
            // $this->asset_js(asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js'));
            // $this->asset_css(asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')); // DATEPICKER
            //$this->asset_css(asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css')); // DATEPICKER
            

            //$this->asset_js(asset('bower_components/vue/dist/vue.js'));
            //$this->asset_js(asset('assets/js/bud-vue.js'));
            
            // $this->asset_js(asset('bower_components/summernote/dist/summernote.min.js'));
            // $this->asset_css(asset('bower_components/summernote/dist/summernote.css'));
            
           
            // $this->asset_css(asset('bower_components/font-awesome/css/font-awesome.min.css')); // DATEPICKER
            // $this->asset_js(asset('bower_components/uploader/src/dmuploader.min.js')); // UPLOADER




            // $this->asset_js(asset('bower_components/jquery-ui-sortable/jquery-ui-sortable.min.js')); // UPLOADER
            // //$this->asset_js(asset('bower_components/nestedSortable/jquery.mjs.nestedSortable.js')); // UPLOADER
            // $this->asset_js(asset('bower_components/nestedSortable/jquery.ui.nestedSortable.js')); // UPLOADER




            $this->asset_js(asset('assets/js/bud-admin.js'));
            $this->asset_css(asset('assets/css/bud-admin.css'));


            
            


            
            
            
            
            
            


            
            

            
            //$this->asset_css(asset('assets/js/redactor/redactor.css'));
            // $this->asset_js(asset('assets/js/redactor/redactor.min.js')); // WYSIWYG editor
            
            
            
          
            
            
            View::composer('bud::admin._layouts.*', function($view)
            {
                $view->with('head_assets',  $this->head_assets);
            });
            
            View::composer('bud::admin.*', function($view)
            {
                $view->with('search_string',  $this->search_string);
                $view->with('languages',  $this->languages);
                $view->with('current_user',  Auth::user());
            });
            
            
            
            
        }
        
      
        

}