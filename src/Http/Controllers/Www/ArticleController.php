<?php 
namespace BloomDesign\Bud\Http\Controllers\Www;

use BloomDesign\Bud\Http\Controllers\PublicController;

use Auth, Form, Input, Redirect, Sentry, View, Notification, Option;

use Meta;
use Article;
use Category;

class ArticleController extends PublicController {
    
    function show($article_id, $description = null){
        
        $article = Article::translate()->with(Array('picture' => function($q){ $q = $q->orderBy('order');}, 'attachment' => function($q){ $q = $q->orderBy('order');}))->find($article_id);
        
        Meta::set('title', $article->name);
        Meta::set('description', $article->description);
        if(@$article->picture->first())
            Meta::set('image', asset(@$article->picture->first()->filename));
        
        return View::make('bud::www.Article.show')
                ->with('article', $article);
         
    }
    
    function category($category_id, $description = null){
        
        Article::translate();
        $category = Category::translate()->with('article', 'picture', 'attachment')->find($category_id);
        
        Meta::set('title', $category->name);
        Meta::set('description', $category->description);
        if($category->picture->first())
            Meta::set('image', asset($category->picture->first()->filename));
        
        return View::make('bud::www.Category.show')
                ->with('category', $category);
    }
    
    
    
}