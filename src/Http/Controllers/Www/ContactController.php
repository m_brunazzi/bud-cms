<?php 
namespace BloomDesign\Bud\Http\Controllers\Www;
 
use BloomDesign\Bud\Http\Controllers\PublicController;
use Auth, BaseController, Form, Input, Redirect, View, Notification, Option, Mail, Validator;

use Article;

class ContactController extends PublicController {
    
    function form(){
        return View::make('bud::www.Contact.form');
    }
    
    function result(){
        
        $rules = Array(
                        'full_name' => 'required',
                        'email' => 'required|email',
                        'body' => 'required',
                        );
                        
        $validator = Validator::make(Input::all(), $rules);
        
        if ($validator->fails())
        {
            Notification::warning('Verificare i campi obbligatori');
            return Redirect::back()->withInput();
        }
        
        try{
            
            $data = Input::all();
            $body = '<table border="1">';
            $body .= '<tr><td>Nome</td><td><strong>'.Input::get('full_name').'</strong></td></tr>';
            $body .= '<tr><td>Telefono</td><td><strong>'.Input::get('phone').'</strong></td></tr>';
            $body .= '<tr><td>Email</td><td><strong>'.Input::get('email').'</strong></td></tr>';
            $body .= '<tr><td>Oggetto</td><td><strong>'.Input::get('subject').'</strong></td></tr>';
            $body .= '<tr><td>Messaggio</td><td><strong>'.Input::get('body').'</strong></td></tr>';
            $body .= '<tr><td>IP della richiesta</td><td><strong>'.$_SERVER['REMOTE_ADDR'].'</strong></td></tr>';
            $body .= '</table>';
            
            $data['body'] = $body;
            
            Mail::send('bud::emails.generic', $data, function($message) use ($data)
            {
                $message->to(Option::value('contact_email'))
                ->from($data['email'], $data['full_name'])
                ->subject(Option::value('bud.site_name80656').' - '.@$data['subject']);
            });
           
            return View::make('www.Contact.result')
                ->with('status', 'ok')
                ->with('message', 'La sua richiesta è stata inviata correttamente. Riceverete al più presto una risposta dai nostri incaricati.');
        }
        catch(\Exception $e){
            Notification::warning('Errore nell\'invio: '.$e->getMessage());
            return Redirect::back()->withInput();
        }
        
        return View::make('bud::www.Contact.result');
    }
    
}