<?php 
namespace BloomDesign\Bud\Http\Controllers\Www;
 
use BloomDesign\Bud\Http\Controllers\PublicController;
use Auth, BaseController, Form, Input, Redirect, View, Notification, Option;

use Article;

class MainController extends PublicController {
    
    
    function homepage(){
        
        
        return View::make('bud::www.homepage');
    }
    
}