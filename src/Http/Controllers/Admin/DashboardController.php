<?php 

namespace BloomDesign\Bud\Http\Controllers\Admin;


use View, Notification, Cache;
use BloomDesign\Bud\Http\Controllers\AdminController;

class DashboardController extends AdminController {

	public function login()
        {
            return View::make('bud::admin.auth.login');
        }
        
	public function index()
	{
            
         return View::make('bud::admin.dashboard');
	}
        
        public function clearCache()
	{
            Cache::flush();
            Notification::success('Cache svuotata con successo');
            return Redirect::route('bud::admin.dashboard');
	}
}