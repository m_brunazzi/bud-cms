<?php
namespace BloomDesign\Bud\Http\Controllers\Admin;


use BloomDesign\Bud\Http\Controllers\AdminController;
use Auth, BaseController, Form, Input, Redirect, Sentry, View, Notification, Option;
use Illuminate\Http\Request;
use Article;
use Category;
use Language;
use Tag;
use Group;
use Carbon\Carbon;

class ArticleController extends AdminController {
    
        public function __construct(){
            parent::__construct();
            // Article::setAvailableLanguages(Language::getLanguages());
            // Article::setLanguage(Language::getCurrent());
        }
    
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
                $rows = new Article;
                $rows = $rows->filter($this->search_string); //->with('category');
                
                $category_id = Input::get('category_id');
                if($category_id){
                   $rows = $rows->whereHas('category', function ($query) use ($category_id) {
                        $query = $query->where('blm_article_category.category_id', '=', $category_id);
                    });
                }

                
                
                $rows = $rows->paginate(Option::value('admin_items_per_page'));
                
		return View::make('bud::admin.Article.list')
                        ->with('search' , $this->search_string)
                        ->with('rows' , $rows)
                        ->with('options_category' , collection_options(Category::get(), 'label'))
                        ->with('query_string' , $this->query_string);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
            
                        
            // uso il modulo generico, passandogli i parametri che servono per la personalizzazione
            
            $form = Array(
                'breadcrumbs' =>  Array(
                    'Dashboard' => route('admin.dashboard') ,
                    'Articoli' => route('admin.Article.index'),
                    'Modulo' => ''
                    ),
                'action' => 'admin.Article.store',
                'model_name' => 'Article'
                
            );

            return View::make('bud::admin.Article.form')
                    ->with('options_group' , collection_options(Group::orderBy('id')->get(), 'name'))
                    ->with('form', $form)
                    ->with('categories', Category::all());
                
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            
            if (Article::validate()){
                $record = new Article;
                $record->label = Input::get('label');
                $record->parent_id = Input::get('parent_id')?Input::get('parent_id'):null;
                //$record->parent_id = $request->input('parent_id',0);

                $record->name = Article::encodeTranslations(Input::get('name'));
                $record->summary = Article::encodeTranslations(Input::get('summary'));
                $record->description = Article::encodeTranslations(Input::get('description'));
                $record->date = Input::get('date')?:Carbon::now();
                $record->template = Input::get('template');
                $record->online = Input::get('online');
              
                $record->save();
                
                $record->category()->sync(is_array(Input::get('category'))?Input::get('category'):Array());
                
                $record->authorizedGroup()->sync(Input::get('authorized_group')?:[]);
                
            
              
                Notification::success('Record salvato correttamente.');
                return Redirect::route('admin.Article.edit', $record->id);
            }
            
            return Redirect::back()->withInput()->withErrors(Article::$errors['validation']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
            $record = Article::with(['tag', 'father'])->find($id);
            
             // uso il modulo generico, passandogli i parametri che servono per la personalizzazione
            
            $form = Array(
                'title' => $record->label,
                'breadcrumbs' =>  Array(
                    'Dashboard' => route('admin.dashboard') ,
                    'Articoli' => route('admin.Article.index'),
                    'Modulo' => ''
                    ),
                'model_name' => 'Article'
            );

            return View::make('bud::admin.Article.form')
                        ->with('form', $form)                        
                        ->with('options_group' , collection_options(Group::orderBy('id')->get(), 'name'))
                        ->with('record', $record)
                        ->with('categories', Category::all());
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{


 
            if (Article::validate()){
		        $record = Article::find($id);
                $record->label = Input::get('label');
                $record->parent_id = Input::get('parent_id')?Input::get('parent_id'):null;
                //$record->parent_id = $request->input('parent_id',0);

                $record->name = Article::encodeTranslations(Input::get('name'));
                $record->summary = Article::encodeTranslations(Input::get('summary'));
                $record->description = Article::encodeTranslations(Input::get('description'));
                $record->date = Input::get('date')?Input::get('date'):"00/00/0000";
                $record->online = Input::get('online')?1:0;
                $record->template = Input::get('template');
                $record->save();
                
                $record->category()->sync(is_array(Input::get('category'))?Input::get('category'):Array());
                
                

                $record->authorizedGroup()->sync(Input::get('authorized_group')?:[]);
                                
             
                Notification::success('Record salvato correttamente.');
                return Redirect::route('admin.Article.edit', $record->id);
            }
            
            return Redirect::back()->withInput()->withErrors(Article::$errors['validation']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$record = Article::find($id);
        $record->link()->delete();
        $record->delete();
        Notification::success('Record cancellato correttamente.');
        return Redirect::route('admin.Article.index');
        
	}

}