<?php
namespace BloomDesign\Bud\Http\Controllers\Admin;


use BloomDesign\Bud\Http\Controllers\AdminController;
use Auth, BaseController, Form, Input, Redirect, Sentry, View, Notification, Option;
use Illuminate\Http\Request;
use AttachmentType;
use Attachment;
use Language;

class AttachmentTypeController extends AdminController {
    
        public function __construct(){
            parent::__construct();
            AttachmentType::setAvailableLanguages(Language::getLanguages());
            AttachmentType::setLanguage(Language::getCurrent());
        }
    
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $rows = new AttachmentType;
        $rows = $rows->filter($this->search_string);
            
        $rows = $rows->paginate(Option::value('admin_items_per_page'));
        
        return View::make('bud::admin.AttachmentType.list')
                ->with('search' , $this->search_string)
                ->with('rows' , $rows)
                ->with('query_string' , $this->query_string);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
            
                        
            // uso il modulo generico, passandogli i parametri che servono per la personalizzazione
            
            $form = Array(
                'breadcrumbs' =>  Array(
                    'Dashboard' => route('admin.dashboard') ,
                    'Tipi di allegato' => route('admin.AttachmentType.index'),
                    'Modulo' => ''
                    ),
                'action' => 'admin.AttachmentType.store',
                'model_name' => 'AttachmentType'
                
            );

            return View::make('bud::admin.AttachmentType.form')
                    ->with('form', $form);
                
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            
            if (AttachmentType::validate()){
                $record = new AttachmentType;
                $record->label = Input::get('label');

                $record->name = AttachmentType::encodeTranslations(Input::get('name'));
                $record->description = AttachmentType::encodeTranslations(Input::get('description'));
                $record->online = Input::get('online');
              
                $record->save();
             
            
              
                Notification::success('Record salvato correttamente.');
                return Redirect::route('admin.AttachmentType.edit', $record->id);
            }
            
            return Redirect::back()->withInput()->withErrors(AttachmentType::$errors['validation']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
            $record = AttachmentType::find($id);
            
             // uso il modulo generico, passandogli i parametri che servono per la personalizzazione
            
            $form = Array(
                'title' => $record->label,
                'breadcrumbs' =>  Array(
                    'Dashboard' => route('admin.dashboard') ,
                    'Tipi di allegato' => route('admin.AttachmentType.index'),
                    'Modulo' => ''
                    ),
                'model_name' => 'AttachmentType'
            );

            return View::make('bud::admin.AttachmentType.form')
                        ->with('form', $form)                        
                        ->with('record', $record);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{


 
            if (AttachmentType::validate()){
		        $record = AttachmentType::find($id);
                $record->label = Input::get('label');

                $record->name = AttachmentType::encodeTranslations(Input::get('name'));
                $record->description = AttachmentType::encodeTranslations(Input::get('description'));
                $record->online = Input::get('online');
              
                $record->save();
             
     
             
                Notification::success('Record salvato correttamente.');
                return Redirect::route('admin.AttachmentType.edit', $record->id);
            }
            
            return Redirect::back()->withInput()->withErrors(AttachmentType::$errors['validation']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$record = AttachmentType::find($id);
        $record->delete();
        Notification::success('Record cancellato correttamente.');
        return Redirect::route('admin.AttachmentType.index');
        
	}

}