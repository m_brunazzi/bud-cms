<?php

namespace BloomDesign\Bud\Http\Controllers\Admin;

use BloomDesign\Bud\Http\Controllers\AdminController;
use BaseController, Form, Input, Redirect, Validator, View, Notification, Option;
use BloomDesign\Bud\App\Models\Group;


use App\Services\Validators\GroupValidator;

class GroupController extends AdminController{
        
        var $admin_group_id = null;
    
        public function __construct(){
            parent::__construct();
            
            
        }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
                $rows = Group::with('user');
                
                $rows = $rows->paginate(Option::value('admin_items_per_page'));
                
                $query_string = array_except(Input::all(), array('page'));
                
		return View::make('bud::admin.Group.list')
                        ->with('rows' , $rows)
                        ->with('admin_group_id' , $this->admin_group_id)
                        ->with('query_string' , $query_string);
                
                
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
            
            $record = new Group;
            
          	$form = Array(
                'breadcrumbs' =>  Array(
                    'Dashboard' => route('admin.dashboard') ,
                    'Utenti' => route('admin.User.index'),
                    'Modulo' => ''
                    ),
            );
              
            return View::make('bud::admin.Group.form')
                        ->with('record' , $record)
                        ->with('form' , $form);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            
            $validation = Validator::make(Input::all(), [
	            'name' => 'required|unique:groups'
	            
	        ]);

        
 
            if ($validation->passes()){
		$record = new Group;
                $record->name = Input::get('name');
                $record->description = Input::get('description');
                $record->save();
                
                                
                Notification::success('Record salvato correttamente.');
                return Redirect::route('admin.Group.edit', $record->id);
            }
            
            return Redirect::back()->withInput()->withErrors($validation->errors);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
            
            
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
            $record = Group::find($id);

            $form = Array(
                'breadcrumbs' =>  Array(
                    'Dashboard' => route('admin.dashboard') ,
                    'Utenti' => route('admin.User.index'),
                    'Modulo' => ''
                    ),
            );
            
            return View::make('bud::admin.Group.form')
                        ->with('record' , $record)
                        ->with('form' , $form);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
            $validation = Validator::make(Input::all(), [
	            'name' => 'required|unique:groups'
	            
	        ]);
 
            if ($validation->passes()){
		$record = Group::find($id);
                $record->name = Input::get('name');
                $record->description = Input::get('description');
                $record->save();
                
                              
                Notification::success('Record salvato correttamente.');
                return Redirect::route('admin.Group.edit', $record->id);
            }
            
            return Redirect::back()->withInput()->withErrors($validation->errors);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
                if($id != $this->admin_group_id){
                    $record = Group::find($id);
                
                    $record->delete();
                
                    Notification::success('Record cancellato correttamente.');
                    return Redirect::route('admin.Group.index');
                }
                else{
                    Notification::success('Impossibile cancellare il gruppo degli amministratori');
                    return Redirect::route('admin.Group.index');
                    
                }
	}

}