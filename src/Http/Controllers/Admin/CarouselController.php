<?php 

namespace BloomDesign\Bud\Http\Controllers\Admin;

use BloomDesign\Bud\Http\Controllers\AdminController;

use Auth, BaseController, Form, Input, Redirect, Sentry, View, Notification, Option;

use Carousel;
use Language;
use Tag;


class CarouselController extends AdminController {
        
        public function __construct(){
            parent::__construct();
            
            
        }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
                $rows = new Carousel;
                
                if($this->search_string){
                    $rows = $rows->filter(Array('search' => $this->search_string));
                }
                
                $rows = $rows->paginate(Option::value('admin_items_per_page'));
                
		return View::make('bud::admin.Carousel.list')
                        ->with('search' , $this->search_string)
                        ->with('rows' , $rows)
                        ->with('query_string' , $this->query_string);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
                        
            // uso il modulo generico, passandogli i parametri che servono per la personalizzazione
            
            $form = Array(
                'breadcrumbs' =>  Array(
                    'Dashboard' => route('admin.dashboard') ,
                    'Carousel' => route('admin.Carousel.index'),
                    'Modulo' => ''
                    ),
                'action' => 'admin.Carousel.store',
                'model_name' => 'Carousel',
                'hide_attachments' => true,
                
                
            );
            
            return View::make('bud::admin.Carousel.form')
                    ->with('tags_favourites', Tag::favourites(10))  
                    ->with('form', $form);
                
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
 
            if (Carousel::validate()){
		      
                $record = new Carousel;
                $record->label = Input::get('label');
                if(!Input::get('slug')){
                    $slug = str_slug(Input::get('label'));
                }
                else{
                    $slug = str_slug(Input::get('slug'));
                }
                $record->slug = $slug;
                $record->save();
            
              
                Notification::success('Record salvato correttamente.');
                return Redirect::route('admin.Carousel.edit', $record->id);
            }
            
            return Redirect::back()->withInput()->withErrors(Carousel::$errors['validation']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
            $record = Carousel::with('slide')->find($id);
            
             // uso il modulo generico, passandogli i parametri che servono per la personalizzazione
            
            $form = Array(
                'title' => $record->label,
                'breadcrumbs' =>  Array(
                    'Dashboard' => route('admin.dashboard') ,
                    'Carousel' => route('admin.Carousel.index'),
                    'Modulo' => ''
                    ),
                'model_name' => 'Carousel',
                'hide_attachments' => true,
                
                
            );

            return View::make('bud::admin.Carousel.form')
                        ->with('form', $form)                        
                        ->with('tags_favourites', Tag::favourites(10))                        
                        ->with('record', $record);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
 
            if (Carousel::validate()){

		        $record = Carousel::find($id);
                $record->label = Input::get('label');
                if(!Input::get('slug')){
                    $slug = str_slug(Input::get('label'));
                }
                else{
                    $slug = str_slug(Input::get('slug'));
                }
                $record->slug = $slug;
                $record->save();
                                
             
                Notification::success('Record salvato correttamente.');
                return Redirect::route('admin.Carousel.edit', $record->id);
            }
            
            return Redirect::back()->withInput()->withErrors(Carousel::$errors['validation']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$record = Carousel::find($id);
                $record->delete();
                
                Notification::success('Record cancellato correttamente.');
                return Redirect::route('admin.Carousel.index');
	}

}