<?php 
namespace BloomDesign\Bud\Http\Controllers\Admin;

use BloomDesign\Bud\Http\Controllers\AdminController;
use Auth, BaseController, Form, Input, Redirect, Sentry, View, Notification, Option;
use Illuminate\Http\Request;
use AttachmentType;
use Attachment;
use Language;
use Group;


use App\Services\Validators\AttachmentValidator;

class AttachmentController extends AdminController {
    
    
        private function extract_attachmentable_type($model){
            $tokens = explode('\\', $model);
            return end($tokens);
            
        }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
                
        $rows = new Attachment;
        $rows = $rows->filter($this->search_string);

        if(Input::get('no_relations')){
        	$rows = $rows->select('blm_attachment.*')->leftJoin('blm_attachmentable', 'blm_attachment.id', '=', 'blm_attachmentable.attachment_id')->whereNull('blm_attachmentable.id');
        }

        $type = Input::get('type');
        if($type == 'images'){
        	$rows = $rows->where('blm_attachment.is_image', '=', 1);
        }
        else if($type == 'no_images'){
        	$rows = $rows->where('blm_attachment.is_image', '=', 0);
        }

        if(Input::get('order')){
            $rows = $rows->orderByRaw(Input::get('order'));
        }

            
        $rows = $rows->paginate(Option::value('admin_items_per_page'));
        
        return View::make('bud::admin.Attachment.list')
                ->with('search' , $this->search_string)
                ->with('rows' , $rows)
                ->with('query_string' , $this->query_string);
        
        
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

        $form = Array(
                'title' => 'Nuovo allegato',
                'breadcrumbs' =>  Array(
                    'Dashboard' => route('admin.dashboard') ,
                    'Allegati' => route('admin.Attachment.index'),
                    'Modulo' => ''
                    ),
                'model_name' => 'Attachment',
                'hide_attachments' => true
            );

		return View::make('bud::admin.Attachment.form')
                ->with('options_type' , collection_options(AttachmentType::orderBy('id')->get(), 'label'))
                ->with('options_group' , collection_options(Group::orderBy('id')->get(), 'name'))
                ->with('options_language' , collection_options(Language::where('online', '=', true)->orderBy('id')->get(), 'language'))
                ->with('form', $form);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        if (Attachment::validate()){
            $record = new Attachment;
            $record->label = Input::get('label');
            $record->name = Attachment::encodeTranslations(Input::get('name'));
            $record->description = Attachment::encodeTranslations(Input::get('description'));
            $record->type_id = Input::get('type_id');
            $record->online = Input::get('online');

            $record->filename = $record->handleUpload('filename', Option::value('files_folder'));

            $record->save();

            $record->authorizedGroup()->sync(Input::get('authorized_group')?:[]);
            $record->language()->sync(Input::get('language_ids')?:[]);
            
            Notification::success('Record salvato correttamente.');
            return Redirect::route('admin.Attachment.edit', $record->id);
        }
    
        return Redirect::back()->withInput()->withErrors($validation->errors);
            
            
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$this->edit($id);
	}
        
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $record = Attachment::find($id);

        $form = Array(
            'title' => $record->label,
            'breadcrumbs' =>  Array(
                'Dashboard' => route('admin.dashboard') ,
                'Allegati' => route('admin.Attachment.index'),
                'Modulo' => ''
                ),
            'model_name' => 'Attachment',
            'hide_attachments' => true
        );
        
        return View::make('bud::admin.Attachment.form')
                ->with('record', $record)
                ->with('form', $form)
                ->with('options_type' , collection_options(AttachmentType::orderBy('id')->get(), 'label'))
                ->with('options_language' , collection_options(Language::where('online', '=', true)->orderBy('id')->get(), 'language'))
                ->with('options_group' , collection_options(Group::orderBy('id')->get(), 'name'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        if (Attachment::validate()){
            $record = Attachment::find($id);
            $record->label = Input::get('label');
            $record->name = Attachment::encodeTranslations(Input::get('name'));
            $record->description = Attachment::encodeTranslations(Input::get('description'));
            $record->type_id = Input::get('type_id');
            $record->online = Input::get('online');

            $record->filename = $record->handleUpload('filename', Option::value('files_folder'));

            $record->save();

            $record->authorizedGroup()->sync(Input::get('authorized_group')?:[]);
            $record->language()->sync(Input::get('language_ids')?:[]);
            
            Notification::success('Record salvato correttamente.');
            return Redirect::route('admin.Attachment.edit', $record->id);
        }
    
        return Redirect::back()->withInput()->withErrors($validation->errors);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$record = Attachment::find($id);
        $record->delete();
        Notification::success('Record cancellato correttamente.');
        return Redirect::back();
	}

}