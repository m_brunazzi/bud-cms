<?php 
namespace BloomDesign\Bud\Http\Controllers\Admin;
use BloomDesign\Bud\Http\Controllers\AdminController;

use Auth, BaseController, Form, Input, Redirect, Sentry, View, Notification, Option, DB;

use Language;
use Category, Article;
use Tag;


class CategoryController extends AdminController {

        public function __construct(){
            parent::__construct();
            
            
        }
    
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
            Category::translate();
            
                $rows = new Category;
                $rows = $rows->filter($this->search_string);
                
                $rows = $rows->with('article')->paginate(Option::value('admin_items_per_page'));
                
		return View::make('bud::admin.Category.list')
                        ->with('rows' , $rows)
                        ->with('query_string' , $this->query_string);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
            
                        
            // uso il modulo generico, passandogli i parametri che servono per la personalizzazione
            
            $form = Array(
                'breadcrumbs' =>  Array(
                    'Dashboard' => route('admin.dashboard') ,
                    'Categorie' => route('admin.Category.index'),
                    'Modulo' => ''
                    ),
                'action' => 'admin.Category.store',
                'model_name' => 'Category'
                
            );
            

            $record = new Category;
            
                        
            return View::make('bud::admin.Category.form')
                    ->with('form', $form)
                    ->with('record', $record);
                
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
  
            if (Category::validate()){
                
		$record = new Category;
                $record->label = Input::get('label');
                $record->name = Category::encodeTranslations(Input::get('name'));
                $record->description = Category::encodeTranslations(Input::get('description'));
                $record->parent_id = Input::get('parent_id');
                $record->online = Input::get('online') == 1?true:false;
                $record->save();
                
                           
                Notification::success('Record salvato correttamente.');
                return Redirect::route('admin.Category.edit', $record->id);
            }
            
            return Redirect::back()->withInput()->withErrors(Article::$errors['validation']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
            $record = Category::with('tag')->find($id);
            
             // uso il modulo generico, passandogli i parametri che servono per la personalizzazione
            
            
            $form = Array(
                'title' => $record->label,
                'breadcrumbs' =>  Array(
                    'Dashboard' => route('admin.dashboard') ,
                    'Categorie' => route('admin.Category.index'),
                    'Modulo' => ''
                    ),
                'model_name' => 'Category'
                
            );

            return View::make('bud::admin.Category.form')
                        ->with('form', $form)                        
                        ->with('record', $record);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
 
            if (Category::validate()){
		        $record = Category::find($id);
                $record->label = Input::get('label');
                $record->name = Category::encodeTranslations(Input::get('name'));
                $record->description = Category::encodeTranslations(Input::get('description'));
                $record->parent_id = Input::get('parent_id');
                $record->online = Input::get('online') == 1?true:false;
                $record->save();
              
                Notification::success('Record salvato correttamente.');
                return Redirect::route('admin.Category.edit', $record->id);
            }
            
            return Redirect::back()->withInput()->withErrors(Category::$errors['validation']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$record = Category::find($id);
                $record->delete();
                Notification::success('Record cancellato correttamente.');
                return Redirect::route('admin.Category.index');
	}

}