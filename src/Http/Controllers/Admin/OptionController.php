<?php

namespace BloomDesign\Bud\Http\Controllers\Admin;

use BloomDesign\Bud\Http\Controllers\AdminController;
use Auth,
    BaseController,
    Form,
    Input,
    Redirect,
    Sentry,
    View,
    Notification,
    Option,
    DB;
use Language;


class OptionController extends AdminController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {

        $rows = new Option;
        $rows = $rows->all();

        return View::make('bud::admin.Option.list')
                        ->with('rows', $rows)
                        ->with('query_string', $this->query_string);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {


        // uso il modulo generico, passandogli i parametri che servono per la personalizzazione

        $form = Array(
            'breadcrumbs' => Array(
                'Dashboard' => route('admin.dashboard'),
                'Opzioni' => route('admin.Option.index'),
                'Modulo' => ''
            ),
            'action' => 'admin.Option.store',
            'model_name' => 'Option'
        );
        

        return View::make('bud::admin.Option.form')
                        ->with('form', $form);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {

        if (Option::validate()) {
            $options = Option::encodeOptions(Input::get('option_key'), Input::get('option_value'));

            $record = new Option;
            $record->name = Input::get('name');
            $record->description = Input::get('description');
            $record->type = Input::get('type');
            $record->options = $options;
            $record->value = Input::get('value');
            $record->mandatory = Input::get('mandatory') == 1 ? true : false;
            $record->validator = Input::get('validator');
            $record->save();

            Notification::success('Record salvato correttamente.');
            return Redirect::route('admin.Option.edit', $record->id);
        }

        return Redirect::back()->withInput()->withErrors(Article::$errors['validation']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $record = Option::find($id);



        // uso il modulo generico, passandogli i parametri che servono per la personalizzazione


        $form = Array(
            'title' => $record->label,
            'breadcrumbs' => Array(
                'Dashboard' => route('admin.dashboard'),
                'Opzioni' => route('admin.Option.index'),
                'Modulo' => ''
            ),
            'model_name' => 'Option'
        );

        return View::make('bud::admin.Option.form')
                        ->with('form', $form)
                        ->with('record', $record);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {

        if (Option::validate()) {

            $options = Option::encodeOptions(Input::get('option_key'), Input::get('option_value'));

            $record = Option::find($id);
            $record->name = Input::get('name');
            $record->description = Input::get('description');
            $record->type = Input::get('type');
            $record->options = $options;
            $record->value = Input::get('value');
            $record->mandatory = Input::get('mandatory') == 1 ? true : false;
            $record->validator = Input::get('validator');
            $record->save();

            Notification::success('Record salvato correttamente.');
            return Redirect::route('admin.Option.edit', $record->id);
        }

        return Redirect::back()->withInput()->withErrors(Option::$errors['validation']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $record = Option::find($id);
        $record->delete();
        Notification::success('Record cancellato correttamente.');
        return Redirect::route('admin.Option.index');
    }

    public function saveAll() {

        try {
            $options = Option::all();

            if (count($options) > 0) {
                foreach ($options as $option) {
                    $option->value = Input::get('option_' . $option->id);
                    $option->save();
                }
            }

            Notification::success('Record cancellato correttamente.');
        } catch (\Exception $e) {
            Notification::success($e->getMessage());
        }
        return Redirect::route('admin.Option.index');
    }

    public function remove($id) {
        return $this->destroy($id);
    }

}
