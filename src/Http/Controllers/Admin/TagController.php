<?php 
namespace BloomDesign\Bud\Http\Controllers\Admin;
use BloomDesign\Bud\Http\Controllers\AdminController;

use Auth, BaseController, Form, Input, Redirect, Sentry, View, Notification, Option, DB;

use Language;
use Tag;


class TagController extends AdminController {

        public function __construct(){
            parent::__construct();
            //Tag::translate();
            // Tag::setAvailableLanguages(Language::getLanguages());
            // Tag::setLanguage(Language::getCurrent());
        }
    
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
            
            
        $rows = new Tag;
        $rows = $rows->filter($this->search_string);
        
        $rows = $rows->paginate(Option::value('admin_items_per_page'));
                
		return View::make('bud::admin.Tag.list')
                        ->with('rows' , $rows)
                        ->with('query_string' , $this->query_string);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
            
                        
            // uso il modulo generico, passandogli i parametri che servono per la personalizzazione
            
            $form = Array(
                'breadcrumbs' =>  Array(
                    'Dashboard' => route('admin.dashboard') ,
                    'Categorie' => route('admin.Tag.index'),
                    'Modulo' => ''
                    ),
                'action' => 'admin.Tag.store',
                'model_name' => 'Tag'
                
            );
            
            
                        
            return View::make('bud::admin.Tag.form')
                    ->with('tags_favourites', Tag::favourites(10))  
                    ->with('form', $form);
                
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
  
            if (Tag::validate()){
                
		        $record = new Tag;
                $record->label = Input::get('label');
                $record->name = Tag::encodeTranslations(Input::get('name'));
                $record->save();
              
            
              
                Notification::success('Record salvato correttamente.');
                return Redirect::route('admin.Tag.edit', $record->id);
            }
            
            return Redirect::back()->withInput()->withErrors(Article::$errors['validation']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
            $record = Tag::find($id);
            
             // uso il modulo generico, passandogli i parametri che servono per la personalizzazione
            
            
            $form = Array(
                'title' => $record->label,
                'breadcrumbs' =>  Array(
                    'Dashboard' => route('admin.dashboard') ,
                    'Tags' => route('admin.Tag.index'),
                    'Modulo' => ''
                    ),
                'model_name' => 'Tag'
                
            );

            return View::make('bud::admin.Tag.form')
                        ->with('form', $form)                        
                        ->with('record', $record);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
 
            if (Tag::validate()){
		        $record = Tag::find($id);
                $record->label = Input::get('label');
                $record->name = Tag::encodeTranslations(Input::get('name'));
                $record->save();
                
             
                Notification::success('Record salvato correttamente.');
                return Redirect::route('admin.Tag.edit', $record->id);
            }
            
            return Redirect::back()->withInput()->withErrors(Tag::$errors['validation']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$record = Tag::find($id);
        $record->taggable()->detach($record);
        $record->delete();
        Notification::success('Record cancellato correttamente.');
        return Redirect::route('admin.Tag.index');
	}

}