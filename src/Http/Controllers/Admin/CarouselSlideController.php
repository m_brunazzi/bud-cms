<?php 

namespace BloomDesign\Bud\Http\Controllers\Admin;

use BloomDesign\Bud\Http\Controllers\AdminController;

use Auth, BaseController, Form, Input, Redirect, Sentry, View, Notification, Option;

use CarouselSlide, Carousel;
use Language;



class CarouselSlideController extends AdminController {
        
        public function __construct(){
            parent::__construct();
            
            CarouselSlide::setAvailableLanguages(Language::getLanguages());
            CarouselSlide::setLanguage(Language::getCurrent());
        }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
              
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
     
            $carousel_id = Input::get('carousel_id');
            if(!$carousel_id)
                throw new \Exception('ID Carousel non valido');

            $record = new CarouselSlide;
            $record->carousel_id = $carousel_id;

            // uso il modulo generico, passandogli i parametri che servono per la personalizzazione
            
            $form = Array(
                'breadcrumbs' =>  Array(
                    'Dashboard' => route('admin.dashboard') ,
                    'Carousel' => route('admin.Carousel.edit', $carousel_id),
                    'Slide' => ''
                    ),
                'action' => 'admin.CarouselSlide.store',
                'model_name' => 'CarouselSlide',
                'hide_attachments' => true,
                
            );
            
            return View::make('bud::admin.CarouselSlide.form')
                    ->with('options_carousel' , collection_options(Carousel::orderBy('label')->get(), 'label'))
                    ->with('form', $form)
                    ->with('record', $record);
                
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
 
            if (CarouselSlide::validate()){
		      $record = new CarouselSlide;
                $record->label = Input::get('label');
                $record->carousel_id = Input::get('carousel_id');
                $record->name = CarouselSlide::encodeTranslations(Input::get('name'));
                $record->summary = CarouselSlide::encodeTranslations(Input::get('summary'));
                $record->online = Input::get('online');

                $record->image = $record->handleUpload('image', Option::value('images_folder'));
                $record->background_video = Input::get('background_video');

                $record->link_text = Input::get('link_text');
                $record->link_url = Input::get('link_url');
                $record->link_blank = Input::get('link_blank');

                $record->caption_position = Input::get('caption_position');
                $record->save();
                
              
                Notification::success('Record salvato correttamente.');
                return Redirect::route('admin.CarouselSlide.edit', $record->id);
            }
            
            return Redirect::back()->withInput()->withErrors(CarouselSlide::$errors['validation']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
            $record = CarouselSlide::find($id);
            
             // uso il modulo generico, passandogli i parametri che servono per la personalizzazione
            
            $form = Array(
                'title' => $record->label,
                'breadcrumbs' =>  Array(
                    'Dashboard' => route('admin.dashboard') ,
                    'Carousel' => route('admin.Carousel.edit', $record->carousel_id),
                    'Slide' => ''
                    ),
                'model_name' => 'CarouselSlide',
                'hide_attachments' => true,
                
            );

            return View::make('bud::admin.CarouselSlide.form')
                        ->with('form', $form)                        
                        ->with('options_carousel' , collection_options(Carousel::orderBy('label')->get(), 'label'))                  
                        ->with('record', $record);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
 
            if (CarouselSlide::validate()){
		        $record = CarouselSlide::find($id);
                $record->label = Input::get('label');
                $record->carousel_id = Input::get('carousel_id');
                $record->name = CarouselSlide::encodeTranslations(Input::get('name'));
                $record->summary = CarouselSlide::encodeTranslations(Input::get('summary'));
                
                $record->online = Input::get('online');

                $record->image = $record->handleUpload('image', Option::value('images_folder'));
                $record->background_video = Input::get('background_video');

                $record->link_text = Input::get('link_text');
                $record->link_url = Input::get('link_url');
                $record->link_blank = Input::get('link_blank');

                $record->caption_position = Input::get('caption_position');
                $record->save();
                            
             
                Notification::success('Record salvato correttamente.');
                return Redirect::route('admin.CarouselSlide.edit', $record->id);
            }
            
            return Redirect::back()->withInput()->withErrors(CarouselSlide::$errors['validation']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$record = CarouselSlide::find($id);
                $record->delete();
                
                // TODO: cancellare l'immagine
                
                Notification::success('Record cancellato correttamente.');
                return Redirect::route('admin.CarouselSlide.index');
	}

}