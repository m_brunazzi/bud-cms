<?php 

namespace BloomDesign\Bud\Http\Controllers\Admin;


use BloomDesign\Bud\Http\Controllers\AdminController;
 
use Auth, BaseController, Form, Input, Redirect, Sentry, View, Notification, Validator;
use Language,Option;

class LanguageController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
                
               $rows = new Language;
                
                if($this->search_string){
                    $rows = $rows->search($this->search_string);
                }
                
               
                
                $rows = $rows->paginate(Option::value('admin_items_per_page'));
                
                
                
		return View::make('bud::admin.Language.list')
                        ->with('search' , $this->search_string)
                        ->with('rows' , $rows)
                        ->with('query_string' , $this->query_string);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

		$form = Array(
                'breadcrumbs' =>  Array(
                    'Dashboard' => route('admin.dashboard') ,
                    'Lingue' => route('admin.Language.index'),
                    'Modulo' => ''
                    ),
                'action' => 'admin.Language.store',
                'model_name' => 'Language'
                
            );

		return View::make('bud::admin.Language.form')
					->with('form', $form);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            $validation = Validator::make(Input::all(), [
                'language' => 'required',
                'code' => 'required',
            ]);
 
            if ($validation->passes()){
				$record = new Language;
                $record->language = Input::get('language');
                $record->code = Input::get('code');
                $record->online = Input::get('online');
                $record->icon = $record->handleUpload('icon', Option::value('images_folder'));
                $record->save();
                
                
                Notification::success('Record salvato correttamente.');
                return Redirect::route('admin.Language.edit', $record->id);
            }
            
            return Redirect::back()->withInput()->withErrors($validation->errors);
            
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

		$form = Array(
                'breadcrumbs' =>  Array(
                    'Dashboard' => route('admin.dashboard') ,
                    'Lingue' => route('admin.Language.index'),
                    'Modulo' => ''
                    ),
                'action' => 'admin.Language.store',
                'model_name' => 'Language'
                
            );

		$record = Language::find($id);
        return View::make('bud::admin.Language.form')
        			->with('record', $record)
        			->with('title', $record->language )
        			->with('form', $form);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$validation = Validator::make(Input::all(), [
                    'language' => 'required',
                    'code' => 'required',
                ]);
 
                if ($validation->passes()){
                    $record = Language::find($id);
                    $record->language = Input::get('language');
                    $record->code = Input::get('code');
                    $record->online = Input::get('online');
                    $record->icon = $record->handleUpload('icon', Option::value('images_folder'));
                    $record->save();
                    Notification::success('Record salvato correttamente.');
                    return Redirect::route('admin.Language.edit', $record->id);
                }
            
                return Redirect::back()->withInput()->withErrors($validation->errors);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$record = language::find($id);
                $record->delete();
                Notification::success('Record cancellato correttamente.');
                return Redirect::route('admin.Language.index');
	}

}