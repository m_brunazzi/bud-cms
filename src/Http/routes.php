<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// PUBLIC
//Route::get('/', array('as' => 'public.homepage',       'uses' => 'BloomDesign\Bud\Http\Controllers\Www\MainController@homepage'));
//Route::get('/article/{article_id}/{article_slug?}', array('as' => 'public.article.show',       'uses' => 'BloomDesign\Bud\Http\Controllers\Www\ArticleController@show'));
//Route::get('/category/{category_id}/{category_slug?}', array('as' => 'public.category.show',       'uses' => 'BloomDesign\Bud\Http\Controllers\Www\ArticleController@category'));


// ADMINISTRATION LOGIN 
Route::get('admin/login',   array('as' => 'admin.login', 'middleware' => ['web'],      'uses' => 'BloomDesign\Bud\Http\Controllers\Admin\DashboardController@login'));

// ADMINISTRATION
Route::group(array('prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['web', 'bloom.auth']), function(){
    
    //Route::get('dashboard',   array('as' => 'admin.dashboard',       'uses' => 'BloomDesign\Bud\Http\Controllers\Admin\DashboardController@index'));
    Route::get('dashboard',   array('as' => 'dashboard',       'uses' => 'BloomDesign\Bud\Http\Controllers\Admin\DashboardController@index'));
    Route::resource('Article', 'BloomDesign\Bud\Http\Controllers\Admin\ArticleController');
    
    Route::resource('User', 'BloomDesign\Bud\Http\Controllers\Admin\UserController');
    Route::resource('Group', 'BloomDesign\Bud\Http\Controllers\Admin\GroupController');
    Route::resource('Language', 'BloomDesign\Bud\Http\Controllers\Admin\LanguageController');
    Route::resource('Category', 'BloomDesign\Bud\Http\Controllers\Admin\CategoryController');
   
    Route::resource('Carousel', 'BloomDesign\Bud\Http\Controllers\Admin\CarouselController');
    Route::resource('CarouselSlide', 'BloomDesign\Bud\Http\Controllers\Admin\CarouselSlideController');
    Route::resource('Menu', 'BloomDesign\Bud\Http\Controllers\Admin\MenuController');
    
    Route::resource('Option', 'BloomDesign\Bud\Http\Controllers\Admin\OptionController');
    
    Route::resource('Attachment', 'BloomDesign\Bud\Http\Controllers\Admin\AttachmentController');
    
    Route::resource('AttachmentType', 'BloomDesign\Bud\Http\Controllers\Admin\AttachmentTypeController');

    Route::resource('Tag', 'BloomDesign\Bud\Http\Controllers\Admin\TagController');
    
    Route::post('Option/saveAll',   array('as' => 'Option.saveAll',       'uses' => 'BloomDesign\Bud\Http\Controllers\Admin\OptionController@saveAll')); 
    Route::get('Option/remove/{id}',   array('as' => 'Option.remove',       'uses' => 'BloomDesign\Bud\Http\Controllers\Admin\OptionController@remove')); 

    Route::resource('Translation', 'BloomDesign\Bud\Http\Controllers\Admin\TranslationController');
    
    /*
    Route::get('settings',   array('as' => 'admin.settings',       'uses' => 'BloomDesign\Bud\Http\Controllers\Admin\SettingsController@index')); 
    Route::post('settings/save',   array('as' => 'admin.settings.save',       'uses' => 'BloomDesign\Bud\Http\Controllers\Admin\SettingsController@save')); 
    Route::get('settings/delete/{group}/{key}',   array('as' => 'admin.settings.delete',       'uses' => 'BloomDesign\Bud\Http\Controllers\Admin\SettingsController@delete')); 
     * 
     */
});


// RPC
Route::group(array('prefix' => 'rpc', 'middleware' => ['web']), function(){
    
    // Autenticazione
    Route::post('login',   array('as' => 'rpc.login', 'uses' => 'BloomDesign\Bud\Http\Controllers\Rpc\RpcAuthController@postLogin'));
   
});

Route::group(array('prefix' => 'rpc', 'middleware' => ['web', 'bloom.auth']), function(){
    
    // Autenticazione
    
    Route::get('logout',   array('as' => 'rpc.logout', 'uses' => 'BloomDesign\Bud\Http\Controllers\Rpc\RpcAuthController@getLogout'));
    
  

    // Attachmentable - Single controller
    Route::any('attachmentable/manager',   array('as' => 'rpc.attachmentable.manager', 'uses' => 'BloomDesign\Bud\Http\Controllers\Rpc\RpcAttachmentableController@manager'));

    // Passwordable - Single controller
    Route::any('passwordable/manager',   array('as' => 'rpc.passwordable.manager', 'uses' => 'BloomDesign\Bud\Http\Controllers\Rpc\RpcPasswordableController@manager'));

    // Taggable - Single controller
    Route::any('taggable/manager',   array('as' => 'rpc.taggable.manager', 'uses' => 'BloomDesign\Bud\Http\Controllers\Rpc\RpcTaggableController@manager'));


    // Menu - Single controller
    Route::any('menu/manager',   array('as' => 'rpc.menu.manager', 'uses' => 'BloomDesign\Bud\Http\Controllers\Rpc\RpcMenuController@manager'));

    // Translation - Single controller
    Route::any('translation/manager',   array('as' => 'rpc.translation.manager', 'uses' => 'BloomDesign\Bud\Http\Controllers\Rpc\RpcTranslationController@manager'));

    // Carousel - Single controller
    Route::any('carousel/manager',   array('as' => 'rpc.carousel.manager', 'uses' => 'BloomDesign\Bud\Http\Controllers\Rpc\RpcCarouselController@manager'));

    // Link - Single controller
    Route::any('link/manager',   array('as' => 'rpc.link.manager', 'uses' => 'BloomDesign\Bud\Http\Controllers\Rpc\RpcLinkController@manager'));

    // Language - Single controller
    Route::any('language/manager',   array('as' => 'rpc.language.manager', 'uses' => 'BloomDesign\Bud\Http\Controllers\Rpc\RpcLanguageController@manager'));

    // Version - Single controller
    Route::any('version/manager',   array('as' => 'rpc.version.manager', 'uses' => 'BloomDesign\Bud\Http\Controllers\Rpc\RpcVersionController@manager'));

    // Metadata - Single controller
    Route::any('metadata/manager',   array('as' => 'rpc.metadata.manager', 'uses' => 'BloomDesign\Bud\Http\Controllers\Rpc\RpcMetadataController@manager'));


    Route::get('typeahead/query/{type}',   array('as' => 'rpc.typeahead.query', 'uses' => 'BloomDesign\Bud\Http\Controllers\Rpc\RpcTypeaheadController@query'));


    // Summernote upload
    Route::post('html_editor/upload_image',   array('as' => 'rpc.html_editor.upload_image', 'uses' => 'BloomDesign\Bud\Http\Controllers\Rpc\RpcAjaxController@html_editor_upload_image'));
    
    // Rpc Test Command
    Route::any('rpc/test',   array('as' => 'rpc.test', 'uses' => 'BloomDesign\Bud\Http\Controllers\Rpc\RpcAjaxController@rpc_test'));


});




// Catchall route for dynamic routing
// Route::get('{slug}', [
//     'uses' => 'BloomDesign\Bud\Http\Controllers\Www\RoutableController@handle' 
// ])->where('slug', '([A-Za-z0-9\-\/]+)');