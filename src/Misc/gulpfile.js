const elixir = require('laravel-elixir');

var webpack = require('webpack');


require('laravel-elixir-vue-2');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */



elixir(mix => {
    mix.sass('bud/bloom-admin.scss', 'public/assets/css')
    	.sass('bud/public.scss', 'public/assets/css')
      
      .copy('resources/assets/js/bud/bud-service.js','public/assets/js')
      
      .webpack('resources/assets/js/bud/bud-admin.js','public/assets/js', null, {
         resolve: {
             alias: {
                 'vue$': 'vue/dist/vue.js'
             }
         }
     });
	
});

