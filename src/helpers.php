<?php

function ll($url){
    
    if($url == ''){
        return '#';
    }
    else if(substr($url,0,7) == 'http://' || substr($url,0,8) == 'https://'){ 
        return $url;
    }
    else
        return URL::to('/').'/'.$url;
    
    
}

function collection_options($array, $value = null, $key = 'id', $empty = true){
    
    $result = Array();
    if($empty)
        $result[] = '---';
    
    if( count($array) > 0){
        foreach($array as $item){
            if($value != null)
                $result[$item->$key] = $item->$value;
            else
                $result[$item->$key] = $item;
        }
    }
    return $result;
}

function filesize_human($size){
    
    if($size < 1000)
        return $size.' byte';
    else if($size >= 1000 && $size < 10000000)
        return number_format($size/1024, 1).' Kb';
    else if($size >= 10000000)
        return number_format($size/1024/1024, 1).' Mb';
    return $size;
}


?>
