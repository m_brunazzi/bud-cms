<?php


namespace BloomDesign\Bud\App\Interfaces;

interface iLinkable{
	/**
	 * Crea il link per il modello corrente facendo ar
	 * @return string          url della pagina di dettagli del modello
	 */
	public function createLink($language_id = null, $prefix_segments = null); 

	/**
	 * Gestisce la url e visualizza il contenuti relativo
	 * @param  integer $model_id ID del modello corrente
	 * @return null           
	 */
	//public static function handleLink($model_id); // lo tolgo perche tanto lo gestisco lato applicazione

	
}

?>