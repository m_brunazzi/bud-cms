<?php

namespace BloomDesign\Bud\App\Traits;

trait isSearchable {

    //public $searchable_fields = Array(); // Contiene i nomi dei campi in cui effettuare la ricerca semplice
    
    public function filter($search_string = null, $params = Array()) {

        $searchable_fields = $this->searchable_fields;

        $instance = $this;

        

        if (isset($search_string) && $search_string != '') {

            if (is_array($searchable_fields) && count($searchable_fields) > 0) {

                $instance = $instance->where(function($query) use($instance, $searchable_fields, $search_string) {
                    $counter = 0;

                    foreach ($searchable_fields as $item) {

                        if ($counter == 0) {
                            $query->where($item, 'LIKE', '%' . $search_string . '%');
                        } else {
                            $query->orWhere($item, 'LIKE', '%' . $search_string . '%');
                        }
                        $counter++;
                    }
                });
            }
        }

        if (is_array($params) && count($params) > 0) {
            foreach ($params as $key => $value) {
                $instance = $instance->where($key, '=', $value);
            }
        }

        return $instance;
    }

}
