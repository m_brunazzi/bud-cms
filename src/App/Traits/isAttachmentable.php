<?php
namespace BloomDesign\Bud\App\Traits;

trait isAttachmentable {

    public function pictureMain()
    {
        //return $this->morphToMany('Attachment', 'attachmentable', 'blm_attachmentable', 'attachmentable_id', 'attachment_id')->where('is_image', '=', true)->where('is_image_main', '=', true)->withPivot(['is_image_main', 'order']);

        return $this->morphToMany('Attachment', 'attachmentable', 'blm_attachmentable', 'attachmentable_id', 'attachment_id')->where('is_image', '=', true)->orderBy('is_image_main', 'DESC')->take(1)->withPivot(['is_image_main', 'order']);

        
    }
    
    public function picture()
    {
        return $this->morphToMany('Attachment', 'attachmentable', 'blm_attachmentable', 'attachmentable_id', 'attachment_id')->where('is_image', '=', true)->orderBy('order', 'DESC')->withPivot(['is_image_main', 'order']);
    }
    
    public function notPicture()
    {
        return $this->morphToMany('Attachment', 'attachmentable', 'blm_attachmentable', 'attachmentable_id', 'attachment_id')->where('is_image', '<>', true)->withPivot(['is_image_main', 'order']);
    }
    
    
    public function attachment()
    {
        return $this->morphToMany('Attachment', 'attachmentable', 'blm_attachmentable', 'attachmentable_id', 'attachment_id')->withPivot(['is_image_main', 'order']);
    }

    public function notPictureByType(){
        $result = [];
        $attachments = $this->notPicture->sortBy(function($att){
                            return $att->name;
                        });;


        if(!empty($attachments)){
            foreach($attachments as $attachment){

                if(!isset($result[$attachment->type_id]))
                    $result[$attachment->type_id] = collect([]);

                $result[$attachment->type_id]->push($attachment);
            }
        }
        return $result;
    }
    

    
}
