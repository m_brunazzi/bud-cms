<?php
namespace BloomDesign\Bud\App\Traits;

use DB;
use Log;
use Version;
use Auth;
use Request;

trait isVersionable {

	public static $versioning_active = true;
	public static $versioning_copies = 10;
	public static $query_string_var = 'version_id';

	
	public static function bootIsVersionable(){
		
		if(static::$versioning_active){


			static::created(function ($model) {
				static::_saveVersion($model, true);
	        });	

			// updating works only when some data is changed, otherwise use saving
	        static::updating(function ($model) {
				static::_saveVersion($model, false);
	        });	


	        static::deleting(function ($model) {
				$model->version()->delete();
	        });

		}

	}

	public function version(){
		return $this->morphMany('Version', 'versionable')->orderBy('created_at', 'DESC');
	}


	private static function _saveVersion($model, $new = false){
		$versionable_type = get_class($model);
        $data = $model->toJson();

        $versions_to_keep = Version::where('versionable_id', '=', $model->id)
        					->where('versionable_type', '=', $versionable_type)
        					->orderBy('version_number', 'DESC')
        					->take(self::$versioning_copies - 1)
        					->get();
        					
        if($versions_to_keep->isEmpty()){
			$version_number = 1;
        }
        else{
        	$version_number = $versions_to_keep->first()->version_number + 1;

        	// CLEANUP
	        $last_to_keep = $versions_to_keep->last();
	        Version::where('versionable_id', '=', $model->id)
					->where('versionable_type', '=', $versionable_type)
					->where('created_at', '<', $last_to_keep->created_at)
					->where('id', '<>',$last_to_keep->id)
					->delete();
        }
		
        $version = new Version;
        $version->version_number = $version_number;
        $version->versionable_id = $model->id;
        $version->versionable_type = $versionable_type;
        $version->data = $data;
        $version->user_id = @Auth::user()->id?:1;
        $version->save();

        //$model->version()->save($version);
        
	}

	public static function setPreviewRoute($route){
		static::$preview_route = $route;
	}

	public function getPreviewUrl($version_id = null){
		if(!empty(static::$preview_route))
			return route(static::$preview_route, $this->id).($version_id?'?'.self::$query_string_var.'='.$version_id:'');
		else return false;
	}

	public static function findVersioned($id){

		$current = self::find($id);


		if(Auth::check() && Auth::user()->admin) {

			$version_id = Request::get(self::$query_string_var);
			$version = Version::find($version_id);

			if($version && $current->is($version->versionable)){
				return ($version->getVersioned());
			}
		}

		return $current;
	}
}
