<?php
namespace BloomDesign\Bud\App\Traits;

use DB;
use Log;
use Version;
use Auth;
use Request;

trait hasMeta {

	public function meta(){
		return $this->morphMany('Metadata', 'hasmeta');
	}


	
}
