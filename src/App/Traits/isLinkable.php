<?php
namespace BloomDesign\Bud\App\Traits;

trait isLinkable {
    
    public function link()
    {
        return $this->morphMany('Link', 'linkable');
    }
    
    
    // Generazione link pubblico
    
    public function writeLink($link_parameters = null){
        
        if(!$this->exists){
            return false;
        }
        
        $params = $this->link_parameters;
        
        if(isset($link_parameters) && is_array($link_parameters)){
            $link_parameters = array_merge($params, $link_parameters);
        }
       
        $route_params = array_map("Content::prepareLinkParam", $params['route_params']);
        $string = '<a href="'.route($params['route_name'], $route_params).'" class="'.$params['link_class'].'" title="'.($this->$params['link_title']?$this->$params['link_title']:'').'">'.($this->$params['link_text']?$this->$params['link_text']:'').'</a>';
        //$string = '<a href="'.route($params['route_name'], $route_params).'" >'.$this->$params['link_text'].'</a>';
        
        return $string;
    }
    
    public function getLink($link_parameters = null){
        if(!$this->exists){
            return false;
        }
        
        $params = $this->link_parameters;
        
        if(isset($link_parameters) && is_array($link_parameters)){
            $link_parameters = array_merge($params, $link_parameters);
        }
        
        $route_params = array_map("Content::prepareLinkParam", $params['route_params']);
        $string =  route($params['route_name'], $route_params);
        return $string;
    }
    
    private function prepareLinkParam($p){
        if(!is_numeric($this->$p))
            return str_slug($this->$p);
        else
            return $this->$p;
    }
    
    
}
