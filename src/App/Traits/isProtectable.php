<?php
namespace BloomDesign\Bud\App\Traits;

use Auth;

trait isProtectable {
    
    public function authorizedGroup()
    {
        return $this->morphToMany('Group', 'protectable', 'blm_protectable', 'protectable_id', 'group_id');
    }


    public function isGroupAuthorized($user_group_id ){

    	$authorized_groups_id = [];
    	foreach($this->authorizedGroup as $group){
    		$authorized_groups_id[] = $group->id;
    	}

    	

    	if(in_array($user_group_id, $authorized_groups_id) || empty($authorized_groups_id) || @Auth::user()->admin){
    		return true;
    	}
    	else{
    		return false;
    	}
    }
    
    
}
