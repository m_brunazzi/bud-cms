<?php
namespace BloomDesign\Bud\App\Traits;

trait isTaggable {
    
    public function tag()
    {
        return $this->morphToMany('Tag', 'taggable', 'blm_taggable', 'taggable_id', 'tag_id');
    }
    
}
