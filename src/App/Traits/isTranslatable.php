<?php
namespace BloomDesign\Bud\App\Traits;

use View;
use Language;

trait isTranslatable {

    //public $translatable_fields = Array();
    protected static $current_language = null;
    protected static $available_languages = null;
    protected static $auto_translate = false; // definisce se i testi traducibili vengono o no tradotti in automatico

    public static $no_translation = '';


    public function __construct(){


        parent::__construct();
        self::initLanguage();

    }

    public static function initLanguage(){
        if(!self::$available_languages){
            self::setAvailableLanguages(Language::getLanguages());
        }
        if(!self::$current_language){
            self::setLanguage(Language::getCurrent());
        }
    }
    
    public static function setLanguage($language){
        self::$current_language = $language;
    }
    
    public static function setAvailableLanguages($languages){
        self::$available_languages = $languages;
    }
      
    public static function translate($status = true, $language_id = null){

        if(!$language_id)
            $language_id = Language::getCurrent();
        
        $instance = new static;

        $instance->setAvailableLanguages(Language::getLanguages());
        $instance->setLanguage($language_id);

        $instance::$auto_translate = $status;
        
        return $instance;
        

    }


    
    
    // Accessor custom per i campi traducibili. Ovverride della funzione originale, che invece di ritornare il valore grezzo, effettua prima il decode json
    public function getAttribute($key) {
        
        if(in_array($key, $this->translatable_fields ) && self::$auto_translate){
            
            
            $inAttributes = array_key_exists($key, $this->attributes);

            // If the key references an attribute, we can just go ahead and return the
            // plain attribute value from the model. This allows every attribute to
            // be dynamically accessed through the _get method without accessors.
            if ($inAttributes || $this->hasGetMutator($key))
            {
                    $value = $this->getAttributeValue($key);
            }
            
            
            // If the key already exists in the relationships array, it just means the
            // relationship has already been loaded, so we'll just return it out of
            // here because there is no need to query within the relations twice.
            if (array_key_exists($key, $this->relations))
            {
                    $value = $this->relations[$key];
            }

            // If the "attribute" exists as a method on the model, we will just assume
            // it is a relationship and will load and return results from the query
            // and hydrate the relationship's value on the "relationships" array.
            $camelKey = camel_case($key);

            if (method_exists($this, $camelKey))
            {
                    $value =  $this->getRelationshipFromMethod($key, $camelKey);
            }
            else 
                $value = parent::getAttribute($key);
            
            $translation = self::_translate($value);

            return $translation;
        }
        else{
            return parent::getAttribute($key);
        }
    }
    
    
    private function _decodeJson($field){
        $json = $field;
        $translated = self::_translate($json);
        return $translated;
    }
    
    //
    // Converte un array contenente le traduzioni (in cui gli indici sono gli id delle lingue) in una stringa json
    //
    static public function encodeTranslations($translations, $check = true, $function = null){
        
        
        if(!is_array($translations) || count($translations) == 0)
            return false;
        
        // controllo che gli indici corrispondano a lingue disponibili nel database
        // 
        
               
        $validTranslations = Array();
        if(is_array(self::$available_languages) && count(self::$available_languages) > 0 && $check){
            foreach(self::$available_languages as $key => $value){
                if((isset(self::$available_languages[$key]) && isset($translations[$key])) || !$check){

                    if(!$function){
                        $t = $translations[$key];
                    }
                    else{
                        $t = $function($translations[$key]);
                    }


                    $validTranslations[$key] = $t;
                }
            }   
    
        }
        else{
            $validTranslations = $translations;
        }
        
        return json_encode($validTranslations);
        
    }
    
    //
    //  Converte una stringa json in un array di traduzione, verificandone indici e contenuto
    //
    static public function decodeTranslations($string, $check = true){
        
        // Potrebbe servire un TRY CATCH per le eccezioni
        
        
        // controllo che l'array decodificato sia corretto
        $validArray = Array();

        $decoded = json_decode($string, true);
        
        if($decoded === null){
            return false;   // ritorna falso se non riesco a decodificare la stringa
        }
        
        if(isset(self::$available_languages) && count(self::$available_languages) > 0){
            foreach(self::$available_languages as $key => $value){
                if((isset($decoded[$key]) && is_string($decoded[$key])) || !$check){
                    $validArray[$key] = $decoded[$key];
                }
            }
        }
        return $validArray;
        
    }
    
    //
    //  Converte una stringa json in un array di traduzione, verificandone indici e contenuto
    //
    public static function _translate($json, $language_id = null){
        
        
        
        try{
            $decoded = json_decode($json, true);
        
            if($decoded === null){
                return $json;    // ritorna la stringa originale se non riesco a decodificare il json
            }


            if(!$language_id){
                //$language_id = $this->current_language; // se non passo la lingua in cui voglio la traduzione, me lo traduce nella lingua corrente
                $language_id = self::$current_language; // se non passo la lingua in cui voglio la traduzione, me lo traduce nella lingua corrente
            }
            
            if(!isset($decoded[$language_id])){
                throw new \Exception('Traduzione non disponibile');   
            }
            else{
                return $decoded[$language_id];
            }
            
        }
        catch(\Exception $e){
            return isTranslatable::$no_translation;
        }
                
    }
    
    
    public static function multilanguageWidget($widget_name, $translations = Array(), $active_language_code = null, $html_editor = true){
        
        $view_file = 'admin._partials.multilanguage_widget';
        $data = Array();
        $view = View::make($view_file, $data)->render();
        
        return $view;
    }
    

    public static function translateCollection($collection, $translatable_fields = []){



        if(empty($translatable_fields) ){
            $model = new static;
            $translatable_fields = $model->translatable_fields;
        }

        foreach($collection as $item){

            foreach($translatable_fields as $field){
                if(isset($item->$field)){

                    $item->$field = self::_translate($item->$field);
                }
            }
        }

        return $collection;

    }

    public static function isTranslationEmpty($string){
        $cleaned = trim(strip_tags($string));
        return ($cleaned == '' || $cleaned == self::$no_translation);
    }

    
}




// OVERKILL: uso un helper
// Decorator
/*
class CollectionTranslated
{
    protected $_instance;

    public function __construct(\Illuminate\Database\Eloquent\Collection $instance) {
        $this->_instance = $instance;
    }

    public function __call($method, $args) {
        return call_user_func_array(array($this->_instance, $method), $args);
    }

    public function __get($key) {
        return $this->_instance->$key;
    }

    public function __set($key, $val) {
        return $this->_instance->$key = $val;
    }

    public function translate() {

        foreach( $this->_instance as $item){
            dd($item->translatable_fields);
            foreach($item->translatable_fields as $field){
                echo $item->field.'<br/>';
            }
        }
    }
}
*/
