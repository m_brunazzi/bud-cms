<?php
namespace BloomDesign\Bud\App\Traits;

trait isPasswordable {
    
    public function password()
    {
        return $this->morphOne('Password', 'passwordable');
    }
}
