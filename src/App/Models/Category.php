<?php namespace BloomDesign\Bud\App\Models;

use BloomDesign\Bud\App\Traits\isAttachmentable;
use BloomDesign\Bud\App\Traits\isTranslatable;
use BloomDesign\Bud\App\Traits\isSearchable;
use BloomDesign\Bud\App\Traits\isTaggable;
 
class Category extends Content {
    
    use isAttachmentable;
    use isTranslatable;
    use isSearchable;
    use isTaggable;
    
    protected $table = 'blm_category';
    
    protected $validation_rules = Array(
                                     'label' => 'required'
                                     );
    
    public $searchable_fields = Array('name', 'description');      
    public $translatable_fields = Array('name', 'description');      
    public $link_parameters =  Array(
                            'route_name' => 'public.article.show',
                            'route_params' => ['id', 'label'],
                            'link_text' => 'label',
                            'link_title' => 'label',
                            'link_class' => ''
                            ); 
    
    public function article()
    {
        return $this->belongsToMany('BloomDesign\Bud\App\Models\Article', 'blm_article_category', 'category_id', 'article_id');
    }
    
    public function father()
    {
        return $this->hasOne('BloomDesign\Bud\App\Models\Category', 'id', 'parent_id');
    }
    
    public function children()
    {
        return $this->hasMany('BloomDesign\Bud\App\Models\Category',  'parent_id', 'id');
    }
  
}

?>