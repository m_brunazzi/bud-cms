<?php namespace BloomDesign\Bud\App\Models;

use  DB;

use BloomDesign\Bud\App\Traits\isTranslatable;
use BloomDesign\Bud\App\Traits\isSearchable;

class AttachmentType extends Content {

    use isTranslatable;
    use isSearchable;

    protected $table = 'blm_attachment_type';
    protected $validation_rules = Array('label' => 'required'
);
    
    
    public $searchable_fields = Array('name', 'description');      
    public $translatable_fields = Array('name', 'description'); 
    
    public function attachment()
    {
        return $this->hasMany('Attachment', 'type_id');
    }


    public static function getAll(){
        $attachment_types = self::where('online', '=', 1)->orderBy('id')->get();
        $noType = new AttachmentType;
        $noType->id = 0; 
        $noType->label = 'Allegati'; 
        $noType->name = trans('attachments.attachments'); 
        $attachment_types->push($noType);
        return $attachment_types;
    }
    
    
    
    
}

?>