<?php namespace BloomDesign\Bud\App\Models;

use Illuminate\Database\Eloquent\Model;

// Classe astratta da estendere per contenuti generici.
// Gestisce i seguenti aspetti:
// - Immagini associate al contenuto (Picture)
// - Allegati associati al contenuto (Attachment)
// - Tag associati al contenuto (Tag)

use Input;
 
abstract class Content extends Model {
    
    protected $validation_rules;
    
    public static $errors; 
    
    
    public function __construct(){
        parent::__construct();
        
    }
    
    
    // VALIDAZIONE
    
    public static function validate($data = null)
    {
         $instance = new static;
        
        $data = $data ?: \Input::all();
   
        $validation = \Validator::make($data, $instance->validation_rules);
 
        if ($validation->passes()) 
            return true;
        else{
            self::$errors['validation'] = $validation->messages();
            return false;   
        }
    }
    
    
    public function handleUpload($input_name, $destination_folder){
        
        $action = Input::get($input_name.'_action');

        // cancello il file esistente nel caso di richiesta di rimozione o di un nuovo file
        if($action == 'add' || $action == 'remove'){
            if($this->$input_name && file_exists($destination_folder.$this->$input_name))
                unlink($destination_folder.$this->$input_name);
        }

        if($action == 'remove')
            return '';

        if($action == 'add'){
            $file = Input::file($input_name);
            if(!$file)
                return false;

            if(!$file->isValid())
                return false;

            $extension = $file->getClientOriginalExtension(); //if you need extension of the file

            $filename = uniqid().'.'.$extension;
            $uploadSuccess = $file->move($destination_folder, $filename);

            return $destination_folder.$filename;
        }
        
        return $this->$input_name;
    }
    
    // VARIE
    
    public function getOptions($value_field, $id_field = 'id', $empty = true){
        $data = self::get();
        //$data = new static;
        $data->get();
        $result = Array();
        if($empty)
            $result[] = '---';
        foreach($data as $item){
            $result[$item->$id_field] = $item->$value_field;
        }
        return $result;
    }
    
}

?>