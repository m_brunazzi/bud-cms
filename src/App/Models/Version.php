<?php namespace BloomDesign\Bud\App\Models;

use DB;

 
class Version extends \Eloquent {
 
    protected $table = 'blm_version';



    public function user(){
    	return $this->belongsTo('User', 'user_id');
    }
    
    
    public function versionable()
    {
        return $this->morphTo();
    }

    public function getVersioned(){
    	$data = json_decode($this->data, true);
    	$versionable = $this->versionable;

        //get_class($versionable)::unguard(); // errore in produzione
        
        if($versionable){
            $model_name = get_class($versionable);
            call_user_func(array($model_name, 'unguard'));
        	$versionable->fill($data);
        }

    	return $versionable;
    }

    public function restore(){
    	
	    $versionable = $this->getVersioned();	

		$versionable->save();
    	
    }
}

?>