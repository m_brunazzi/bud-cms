<?php namespace BloomDesign\Bud\App\Models;

//use User;

class Group extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    
    protected $table = 'groups';
        
    public function user(){
        return $this->belongsToMany('BloomDesign\Bud\App\Models\User', 'users_groups', 'group_id', 'user_id');
    }
  
}