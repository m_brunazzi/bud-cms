<?php namespace BloomDesign\Bud\App\Models;

use DB;
use BloomDesign\Bud\App\Traits\isTranslatable;
 
class Metadata extends Content {

    use isTranslatable;
 
    protected $table = 'blm_metadata';
    protected $translatable_fields = ['data'];

    public static $default_meta = ['title', 'description'];

    
    
    public function hasmeta()
    {
        return $this->morphTo();
    }

    
    
    
}

?>