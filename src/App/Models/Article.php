<?php namespace BloomDesign\Bud\App\Models;

use BloomDesign\Bud\App\Traits\isAttachmentable;
use BloomDesign\Bud\App\Traits\isTranslatable;
use BloomDesign\Bud\App\Traits\isSearchable;
use BloomDesign\Bud\App\Traits\isTaggable;
use BloomDesign\Bud\App\Traits\isPasswordable;
use BloomDesign\Bud\App\Traits\isProtectable;
use BloomDesign\Bud\App\Traits\hasMeta;
use BloomDesign\Bud\App\Traits\isLinkable;
use BloomDesign\Bud\App\Traits\isVersionable;

use BloomDesign\Bud\App\Models\Language;
use BloomDesign\Bud\App\Models\Category;

//use Category;
 
class Article extends Content {
    use isAttachmentable;
    use isTranslatable;
    use isSearchable;
    use isTaggable;
    use isPasswordable;
    use isProtectable;
    use hasMeta;
    use isLinkable;
    use isVersionable;
        
    protected $table = 'blm_article';
    protected $validation_rules = Array(
                                        'label' => 'required'
                                        );
    protected $dates = array('date');
    
    public $searchable_fields = Array('name', 'summary', 'description');      
    public $translatable_fields = Array('name', 'summary', 'description');      
    public $link_parameters =  Array(
                        'route_name' => 'public.article.show',
                        'route_params' => ['id', 'label'],
                        'link_text' => 'label',
                        'link_title' => 'label',
                        'link_class' => ''
                        ); 
    

    private static $preview_route = 'www.article.show';
    
    // ACCESSORS & MUTATORS
 
    public function getDateAttribute($value){
        $date_formatted = implode('/',array_reverse(explode('-',$value)));
        if($date_formatted == '00/00/0000')
            $date_formatted = '';
        return $date_formatted;
    }
    
    public function setDateAttribute($value){
        $date_formatted = implode('-',array_reverse(explode('/',$value)));
         $this->attributes['date'] = $date_formatted;
    }
    
    // RELATIONS

    public function category()
    {
        return $this->belongsToMany('BloomDesign\Bud\App\Models\Category', 'blm_article_category', 'article_id', 'category_id');
    }
    
    public function father()
    {
        return $this->hasOne('BloomDesign\Bud\App\Models\Article', 'id', 'parent_id');
    }
    
    public function children()
    {
        return $this->hasMany('BloomDesign\Bud\App\Models\Article',  'parent_id', 'id');
    }

    public function getAncestors(){

        if(!isset($this->id) || !$this->parent_id){
            return [];
        }

        $parent = Article::find($this->parent_id);
        
        if(!$parent){
            return [];
        }

        $result = array_merge([$parent], $parent->getAncestors());

        return $result;

    }


    public function createLink($language_id = null, $prefix_segments = []){
        
        Article::translate(true, $language_id);
        Category::translate(true, $language_id);

        if(!$this->name)
            return "";

        $segments = $prefix_segments;
        $link = '';

        $language = Language::find($language_id);
        if($language){
           $segments[] = $language->code; 
        }


        // recupero la prima categoria assegnata se esiste
        $category = $this->category->first();
        if($category && str_slug($category->name) != ''){
            $segments[] = str_slug($category->name);
        }

        $ancestors = array_reverse($this->getAncestors());

        if(!empty($ancestors)){
            foreach($ancestors as $ancestor){
                 $segments[] = str_slug($ancestor->name?:$ancestor->label);
            }
        }

        

        $segments[] = str_slug($this->name?:$this->label);

        $link = '/'.implode('/', $segments);



        if(!Link::check($link, $this)){
            $link = Link::findAlternative($link);
        }


        return $link;

    }
    

}

?>