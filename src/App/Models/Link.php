<?php namespace BloomDesign\Bud\App\Models;


use DB, Log;
 
class Link extends \Eloquent {
 
    protected $table = 'blm_link';
    protected static $recursion_level = 0;
    
    
    public function linkable()
    {
        return $this->morphTo();
    }

   
    public static function check($link, $linkable = null){

    	// se il link è vuoto non lo salvo, ma lo considero valido
    	if($link == '')
    		return true;


    	$count = DB::table('blm_link')
                            ->where('link','=',$link);
                            
        if($linkable){
        	
        	$count = $count->where('linkable_id', '<>', $linkable->id)->where('linkable_type', '<>', get_class($linkable));
        }

                            
        $count  = $count->count();


        

        return !$count > 0;

    }

    public static function findAlternative($link, $uniqid = null){

        
    	$new = '';

    	$suffix_index = 1;

        Log::info('');
    	
    	// controllo se finisce gia con un suffisso
    	if(preg_match("/(\-)(\d+)$/", $link, $matches)){

    		$suffix_index = $matches[2];

    		// se è cosi cerco il primo valore disponibile
    		while($new == ''){

                $new_suffix = $suffix_index++;
    			
    			$tmp = preg_replace("/\-\d+$/", "$1-".$new_suffix, $link);

    			if(!self::check($tmp)){
    				$new = '';
    			}
    			else $new = $tmp;
    		}

    	}
    	else{
            
    		$tmp = $link.'-'.$uniqid?:$suffix_index;
    		$new = self::findAlternative($tmp);
    	}

    	return $new;



    }


    
}

?>