<?php namespace BloomDesign\Bud\App\Models;

use BloomDesign\Bud\App\Traits\isSearchable;
 
class Carousel extends Content {
  
    use isSearchable;
    
    protected $table = 'blm_carousel';
    protected $validation_rules = Array(
                                        'label' => 'required'
                                        );
    
    public $searchable_fields = Array('label');      

    public function slide(){
        return $this->hasMany('BloomDesign\Bud\App\Models\CarouselSlide', 'carousel_id');
    }
    
}

?>