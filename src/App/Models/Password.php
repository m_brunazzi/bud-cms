<?php namespace BloomDesign\Bud\App\Models;

use DB;

 
class Password extends \Eloquent {
 
    protected $table = 'blm_password';
    
    
    public function passwordable()
    {
        return $this->morphTo();
    }
}

?>