<?php

namespace BloomDesign\Bud\App\Models;

use Session,
    Input;

class Language extends Content {

    protected $table = 'blm_language';

    public static $request = null;
    public static $available_languages = null;
    public static $current_language = null;
    public static $editor_class = 'html-editor';

    public static function initialize($request = null) {
        self::loadLanguages();
        self::$current_language = self::getCurrent();
        self::$request = $request;

             
        //return self::$current_language;
    }

    public static function getCurrent() {

        $session_var = Option::value('session_language_var');
        // $session_data = Session::get($session_var);
        // $session_data = session($session_var);
        // $session_data = self::$request->session()->get($session_var);
        $session_data = @$_SESSION[$session_var];

       

        if (isset($session_data['code']) && isset($session_data['id'])) {
            
            \App::setLocale($session_data['code']);
            return $session_data['id'];
        } else {
           
           
            $code = \App::getLocale();

            $language = self::where('code', $code)->first();

            if ($language) {
                self::setCurrent($language->id);
                // $session_data = Array('id' => $language->first()->id, 'code' => $language->first()->code);

                // //session($session_var, $session_data);
                // $_SESSION[$session_var] = $session_data;
                // //Session::put($session_var, $session_data);
                // //Session::save();

                // \App::setLocale($language->code);
                
                // return $language->id;
            }
        }
    }

    public static function setCurrent($language_id) {

        $language = self::find($language_id);
        if (!$language)
            return false;

        $session_var = Option::value('session_language_var');


        if ($language && $session_var) {

            $session_data = Array('id' => $language->id, 'code' => $language->code);

            //session($session_var, $session_data);
            //self::$request->session()->put($session_var, $session_data);
            ////Session::put($session_var, $session_data);
            //Session::save();
            $_SESSION[$session_var] = $session_data;
            

            \App::setLocale($language->code);
            self::$current_language = $language->id;
            return $language->id;
        } else
            return false;
    }

    public static function getLanguages() {
        return self::$available_languages;
    }

    public static function getLanguagesOptions($key = 'id') {
        $result = Array();

        foreach (self::$available_languages as $item) {
            $result[$item->$key] = $item->language;
        }

        return $result;
    }

    private static function loadLanguages() {

        $languages = self::where('online', 1)->get();
        if ($languages) {
            foreach ($languages as $item) {
                self::$available_languages[$item->id] = $item;
            }
            return true;
        }
        return false;
    }

//
// ritorna una stringa contenente un widget per inserire le traduzioni
// 
// @param $codes: Array che contiene elementi in cui l'indice è l'id della lingua e il valore è il suo nome (es. 1 => Italiano)
// @param $translations: Array che contiene elementi in cui l'indice è l'id della lingua e il valore è la traduzione

    static public function multilanguageWidget($widget_name, $translations = Array(), $active_code = null, $editor = true) {



        if (!is_array(self::$available_languages) || count(self::$available_languages) == 0)
            return false;

        if (count(self::$available_languages) > 1)
            $is_multilanguage = true;
        else
            $is_multilanguage = false;

        if (!isset($active_code)) {
            reset(self::$available_languages);
            $active_code = key(self::$available_languages);
        }

        $html = '<div class="multilanguage-widget">' . "\n";

        if ($is_multilanguage) {
            // Linguette
            $html .= '<ul class="nav nav-tabs" role="tablist">' . "\n";
            foreach (self::$available_languages as $id => $language) {
                $html .= '<li role="presentation" class="nav-item"><a class="nav-link ' . ($id == $active_code ? ' active' : '') . '" href="#' . $widget_name . '-' . $language->language . '" aria-controls="' . $language->language . '" role="tab" data-toggle="tab">' . $language->language . '</a></li>' . "\n";
            }
            $html .= '</ul>' . "\n";

            // Pannelli
            $html .= '<div class="tab-content">' . "\n";
        }

        foreach (self::$available_languages as $id => $language) {

            //$html .= '<li role="presentation" '.($code == $active_code?'class="active"':'').'><a href="#home" aria-controls="'.$language.'" role="tab" data-toggle="tab">'.$language.'</a></li>'."\n";
            if ($is_multilanguage) {
                $html .= '<div role="tabpanel" class="tab-pane' . ($id == $active_code ? ' show active' : '') . '" id="' . $widget_name . '-' . $language->language . '">' . "\n";
            }


            $html .= '<div class="html-editor-container">' . "\n";
            if ($editor)
                $html .= '<textarea name="' . $widget_name . '[' . $id . ']" id="'.$widget_name.'_'.$id.'" class="'.Language::$editor_class.'">' . @$translations[$id] . '</textarea>' . "\n";
            else
                $html .= '<input name="' . $widget_name . '[' . $id . ']" id="'.$widget_name.'_'.$id.'"  class="form-control" value="' . strip_tags(@$translations[$id]) . '"/>' . "\n";
            
            /*
            if ($is_multilanguage) {
                $html .= '<div class="text-center"><small>' . $language->language . '</small></div>' . "\n";
            }
            */

            $html .= '</div>' . "\n";

            /*
            
            // RIMOSSO IL TASTO PER COPIARE DA ALTRE LINGUE
            $html .= '<div class="text-right">';
            foreach (self::$available_languages as $id2 => $language2){
                if($id2 != $id){
                    $html .= '<a class="btn btn-sm copy-translation-btn" data-widget-name="'.$widget_name.'" data-source-id="'.$id2.'" data-destination-id="'.$id.'">Copia da '.$language2->language.'</a>';
                }
            }
            $html .= '</div>';
            */

            if ($is_multilanguage) {
                $html .= '</div>' . "\n";
            }
        }

        if ($is_multilanguage) {
            $html .= '</div>' . "\n";
        }

        // tasti per la copia dei contenuti



        $html .= '</div>' . "\n";


        return $html;
    }

}

?>