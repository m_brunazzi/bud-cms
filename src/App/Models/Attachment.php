<?php namespace BloomDesign\Bud\App\Models;

use  DB, Input;

use BloomDesign\Bud\App\Models\Content;
use BloomDesign\Bud\App\Traits\isSearchable;
use BloomDesign\Bud\App\Traits\isTranslatable;
use BloomDesign\Bud\App\Traits\isProtectable;
use BloomDesign\Bud\App\Traits\isLinkable;
 
 
class Attachment extends Content {

    use isTranslatable;
    use isSearchable;
    use isProtectable;
    use isLinkable;

    protected $validation_rules = Array(
                                        'name' => 'required'
                                        );

    public $searchable_fields = Array('label', 'name', 'description');      
    public $translatable_fields = Array('name', 'description'); 
 
    protected $table = 'blm_attachment';
    private $directory_separator = '/';
    protected $image_extensions = Array(
                                    'jpg',
                                    'jpeg',
                                    'gif',
                                    'png',
                                    'bmp'
                                        );
    public function attachmentable()
    {
        // TODO: questa è da verificare perche mi sa che non funziona
        return $this->morphTo();
    }
    
    public function type()
    {
        return $this->BelongsTo('AttachmentType', 'type_id');
    }

    public function language()
    {
        return $this->belongsToMany('BloomDesign\Bud\App\Models\Language', 'blm_attachment_language', 'attachment_id', 'language_id');
    }




    public function getMetaAttribute($value){
        return json_decode($value);
    }




        
    public function setMain(){
        
        if(isset($this->id)){
            $this->is_image_main = true;
            $this->save();
        }
        return $this;
    }
    
    public function save(array $options = array()){
        
        if($this->is_image_main == true){
            DB::update('update blm_attachment set is_image_main = 0 where is_image = 1 AND  id <> ? AND attachmentable_id = ? AND attachmentable_type = ?', array($this->id, $this->attachmentable_id, $this->attachmentable_type));
            
        }
        return parent::save();
    }
    
     public function delete(){
        
         if(isset(self::$filename) && file_exists(self::$filename))
             unlink(self::$filename);
        
        return parent::delete();
    }




    public function createLink($language_id = null,  $prefix_segments = []){

        

        Attachment::translate(true, $language_id);


        
        if(!$this->name)
            return "";


        $segments = $prefix_segments;

        $link = '';


        $language = Language::find($language_id);
        if($language){
           $segments[] = $language->code; 
        }


        $segments[] = 'files';
        $segments[] = str_slug($this->name?:$this->label);
        
        $link = '/'.implode('/', $segments);



        if(!Link::check($link, $this)){
            $link = Link::findAlternative($link);
        }


        $link .= '.'.$this->extension;

        if(!Link::check($link, $this)){

            $link = Link::findAlternative($link, $this->id);

        }

        return $link;
       

    }

    public function setNameTranslated($name){
        Language::initialize();
        $languages = Language::getLanguages();
        $name_translated = [];
        if(!empty($languages)){
            foreach($languages as $language){
                $name_translated[$language->id] = $name;
            }
        }
        $this->name = json_encode($name_translated);
    }
    
    
    public function addFile($f, $destination_folder, $slug = ""){
        
        // aggingo la barra in fondo se non c'è
        if(substr($destination_folder, -1) != $this->directory_separator){
            $destination_folder .= $this->directory_separator;
        }
        
        try{
              
            $file = $f;
            $class = get_class($f);

            

            
            switch($class){
                
                case 'Illuminate\Http\UploadedFile':

                    if(!$this->label){
                        $label = $file->getClientOriginalName();
                        $this->label = $label;
                        
                    }

                    $this->original_filename = $file->getClientOriginalName();
                    $this->extension = $file->getClientOriginalExtension();
                    $this->mime_type = $file->getClientMimeType();
                    $this->size = $file->getClientSize();
                    break;
                default:

                    if(!$this->name){
                        $label = $file->getFilename();
                        $this->label = $label;
                        $name_translated = [];
                        if(!empty($languages)){
                            foreach($languages as $language){
                                $name_translated[$language->id] = $name;
                            }
                        }
                        $this->name = json_encode($name_translated);
                    }
                                        
                    $this->original_filename = $file->getFilename();
                    $this->extension = $file->getExtension();
                    $finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
                    $this->mime_type = finfo_file($finfo, $f->getRealPath() );
                    finfo_close($finfo);
                    $this->size = $file->getSize();
                    break;
            }


            if(!$this->name){
                $this->setNameTranslated($this->label);
            }
           
           
           $filename_new = ($slug!=''?$slug.'-':'').uniqid().($this->extension != '' ? '.'.$this->extension : '');
           
           $this->filename = $destination_folder.$filename_new;
           
           $is_image = $this->isImage();
           
           $this->is_image = $is_image;
           
           
           if($class == 'Illuminate\Http\UploadedFile'){
            $file->move($destination_folder, $this->filename);    
           }
           else{
               $destination = public_path().'/'.$this->filename;
               rename($file->getRealPath(), $destination);
           }
           
           
           
           return $this;
            
        }
        catch(\Exception $e){
            return false;
        }
    }
    
    public function removeFile(){
        
        if(file_exists($this->filename)){
            unlink($this->filename);
        }
        
        $this->filename = '';
        $this->original_filename = '';
        $this->extension = '';
        $this->mime_type = '';
        $this->size = 0;
        
        return $this;
        
    }
    
    public function isImage(){
        return (isset($this->extension) && in_array(strtolower($this->extension), $this->image_extensions));
    }
    
    public function thumbnail(){
        return ($this->filename !=  "" && $this->is_image)?Image::resize($this->filename, Option::value('admin_thumbnail_width'), Option::value('admin_thumbnail_height'), true)
                                    :
                                    '';
    }
    
    public function setMainImage(){
        if($this->isImage()){
           
            $this->main_image = true;
            $this->save();
        }
    }


    public function handleUpload($input_name, $destination_folder){
        
        $action = Input::get($input_name.'_action');



        // cancello il file esistente nel caso di richiesta di rimozione o di un nuovo file
        if($action == 'add' || $action == 'remove'){
            /*if($this->$input_name && file_exists($destination_folder.$this->$input_name))
                unlink($destination_folder.$this->$input_name);
                */
            $this->removeFile();
        }

        if($action == 'remove')
            return '';

        if($action == 'add'){
            $file = Input::file($input_name);
            if(!$file)
                return false;

            if(!$file->isValid())
                return false;


            $this->addFile($file, $destination_folder, '', $this->name);

            return $this->filename;
        }
        
        return $this->$input_name;
    }
    

    public static function filterByLanguage($attachments, $language_id = null){



        if(!$language_id){
            $language_id = Language::getCurrent();
        }

        $filtered = [];
        if(!empty($attachments)){
            foreach($attachments as $attachment){
                $language_ids = $attachment->language->pluck('id');


                if($language_ids->isEmpty() || $language_ids->contains($language_id))
                    $filtered[] = $attachment;
            }
        }   

        
        return collect($filtered);

    }
    
}

?>