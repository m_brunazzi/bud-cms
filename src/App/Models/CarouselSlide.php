<?php namespace BloomDesign\Bud\App\Models;


use BloomDesign\Bud\App\Traits\isTranslatable;
use BloomDesign\Bud\App\Traits\isSearchable;


 
class CarouselSlide extends Content {
    
    use isTranslatable;
    use isSearchable;
    
    protected $table = 'blm_carousel_slide';
    protected $validation_rules = Array(
                                        'label' => 'required',
                                        'carousel_id' => 'required'
                                        );
    
    
    public $searchable_fields = Array('name', 'summary');      
    public $translatable_fields = Array('name', 'summary');   


    public function carousel(){
        return $this->belongsTo('BloomDesign\Bud\App\Models\Carousel', 'carousel_id');
    }
}

?>
