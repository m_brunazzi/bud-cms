<?php
namespace BloomDesign\Bud\App\Models;

//use Option;

class Option extends \Eloquent {

    protected $table = 'blm_option';
    protected $validation_rules = Array(
        'name' => 'required'
    );
    private static $types = [
        'boolean',
        'text',
        'number',
        'select',
        'radio',
    ];
    private $errors;

    // VALIDAZIONE

    public static function validate($data = null) {
        $instance = new static;

        $data = $data ?: \Input::all();

        $validation = \Validator::make($data, $instance->validation_rules);

        return $validation->passes();
    }

    public static function value($name) {

        $result = self::where('name', '=', $name)->first();

        if (!$result) {
            return false;
        }

        return $result->value;
    }

    public static function getTypes() {

        $values = [];
        foreach (self::$types as $item) {
            $values[$item] = $item;
        }
        return $values;
    }

    public static function encodeOptions($keys, $values) {
        $options = [];
        if (is_array($keys) && count($keys) > 0) {
            foreach ($keys as $index => $key) {
                $options[$key] = @$values[$index];
            }
        }
        return json_encode($options);
    }

    public static function decodeOptions($json) {
        $options = json_decode($json);
        if (!$options)
            $options = [];

        return $options;
    }

    public function getOptions() {
        if (!$this->id)
            return false;
        $options = self::decodeOptions($this->options);
        return $options;
    }

}

?>