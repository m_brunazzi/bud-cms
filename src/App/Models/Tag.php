<?php namespace BloomDesign\Bud\App\Models;

use DB;
use BloomDesign\Bud\App\Traits\isTranslatable;
use BloomDesign\Bud\App\Traits\isSearchable;

 
class Tag extends Content {

    use isTranslatable;
    use isSearchable;
 
    protected $table = 'blm_tag';

    protected $validation_rules = Array(
                                     'label' => 'required'
                                     );
    
    public $searchable_fields = Array('label', 'name');      
    public $translatable_fields = Array('name'); 
 
    
    
    public function taggable()
    {
        return $this->morphTo();
    }

    
    
    public static function find_or_create(Array $tags){
        $result = Array();
        
        
        for($i = 0; $i < count($tags); $i++){
            $tags[$i] = strtolower($tags[$i]);
        }
        
        $actual = Tag::whereIn('label', $tags)->get();
        
        
        foreach($actual as $item){
            $result[] = $item->id;
            $tags = array_diff($tags, Array($item->label));
        }
        foreach($tags as $item){
            if(trim($item) != ''){
                $tag = new Tag;
                $tag->label = $item;
                $tag->save();
                $result[] = $tag->id;
            }
        }
        
        return $result;
    }
    
    
    
    public static function favourites($count = 0, $taggable_type = null){
        $query = 'SELECT COUNT(blm_taggable.tag_id) AS cnt, blm_tag.label FROM blm_tag INNER JOIN blm_taggable ON  blm_taggable.tag_id = blm_tag.id ';
        if($taggable_type){
            $query .= 'WHERE blm_taggable.taggable_type = "'.addslashes($taggable_type).'"';
        }
        $query .= ' GROUP BY blm_tag.label ORDER BY cnt DESC ';



        if($count > 0)
            $query .= 'LIMIT 0,'.$count;
        
        $result = collect(DB::select($query));
        
        return $result;
        
    }
    
    
}

?>