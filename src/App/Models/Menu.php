<?php namespace BloomDesign\Bud\App\Models;

use BloomDesign\Bud\App\Traits\isTranslatable;
use BloomDesign\Bud\App\Traits\isSearchable;

use MenuNode;
use URL;
 
class Menu extends Content {
    use isTranslatable;
    use isSearchable;
 
    protected $table = 'blm_menu';
    protected $translatable_fields = Array('name','description');
    protected $searchable_fields = Array('name','description');
    
    protected $validation_rules = Array(
                                     'label' => 'required'
                                     );
  

    private $nodes = Array();
    private $nodes_hierarchy_plain = false;
    private $nodes_hierarchy = Array();

    private $level = 0;

    public function getNodesPlain($language_id){
        $this->nodes_hierarchy_plain = Array();
        $nodes_tmp = MenuNode::where('menu_id', '=', $this->id)->where('language_id', '=', $language_id)->orderBy('order', 'asc')->get();


        foreach($nodes_tmp as $item){
            $parent_id = $item->parent_id?$item->parent_id:0;
            $this->nodes_hierarchy_plain[$parent_id][$item->id] = $item;    
        }
        return $this->nodes_hierarchy_plain;
    }


    public function getNodes($language_id, $parent_id = 0,  $force_reload = false){

        $nodes = Array();       

        if(!isset($this->id))
            return false;
        
        // preparo l'array con la gerarchia appiattita
        if($this->nodes_hierarchy_plain === false || $force_reload == true){
            $this->getNodesPlain($language_id);
        }
            

        // elaboro la struttura
        if(isset($this->nodes_hierarchy_plain[$parent_id]) && count($this->nodes_hierarchy_plain[$parent_id]) > 0){
            foreach($this->nodes_hierarchy_plain[$parent_id] as $item){
                $item->children = $this->getNodes($language_id, $item->id);
                $nodes[] = $item;
            }   
        }
        

        return $nodes;

    }






    /**
     * Restituisce l'html di base del menu
     * @param  integer  $language_id    lingua
     * @param  integer $parent_id     nodo di partenza
     * @param  function  $node_formatter funzione che formatta i nodi
     * @return string                  html del menu
     */
    public function getHtml($language_id, $parent_id = 0, $node_formatter = null){

        $html = '';

        if(!isset($this->id))
            return false;
        
        // preparo l'array con la gerarchia appiattita
        if($this->nodes_hierarchy_plain === false){
            $this->getNodesPlain($language_id);
        }

        // elaboro la struttura
        if(isset($this->nodes_hierarchy_plain[$parent_id]) && count($this->nodes_hierarchy_plain[$parent_id]) > 0){
            $html .= '<ul>';
            foreach($this->nodes_hierarchy_plain[$parent_id] as $item){
                $html .= '<li rel="'.$item->id.'">';
                if(!$node_formatter){
                    $html .= '<span class="item">'.$item->title.'</span>';
                }
                else
                    $html .= $node_formatter($item);
                $html .= $this->getHtml($language_id, $item->id, $node_formatter);
                $html .= '</li>';
            }
            $html .= '</ul>';   
        }
        
        return $html;

    }


    /**
     * Restituisce l'html  del menu per Bootstrap 3
     * @param  integer  $language_id    lingua
     * @param  integer $parent_id     nodo di partenza
     * @param  function  $node_formatter funzione che formatta i nodi
     * @return string                  html del menu
     */
    public function getBs3Html($language_id, $parent_id = 0){

        $html = '';

        if(!isset($this->id))
            return false;
        
        // preparo l'array con la gerarchia appiattita
        if($this->nodes_hierarchy_plain === false){
            $this->getNodesPlain($language_id);
        }

        // elaboro la struttura
        if(isset($this->nodes_hierarchy_plain[$parent_id]) && count($this->nodes_hierarchy_plain[$parent_id]) > 0){
            $html .= '<ul class="'.(!$parent_id?'nav navbar-nav':'dropdown-menu').'">';
            foreach($this->nodes_hierarchy_plain[$parent_id] as $item){
                if(isset($this->nodes_hierarchy_plain[$item->id]) && count($this->nodes_hierarchy_plain[$item->id]) > 0){
                    $html .= '<li class="dropdown" '.$item->extra.' rel="'.$item->id.'">';
                    $html .= '<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
                    $html .= $item->title;
                    $html .= '<span class="caret"></span>';
                    $html .= '</a>';
                    $html .= $this->getBs3Html($language_id, $item->id);
                    $html .= '</li>';
                }
                else{
                    $html .= '<li rel="'.$item->id.'" '.$item->extra.'>';
                    $html .= '<a href="'.$this->handleLink($item->url).'"'.($item->url_blank_page?' target="_blank"':'').'>';
                    $html .= $item->title;
                    $html .= '</a>';
                    $html .= '</li>';
                }
            }
            $html .= '</ul>';   
        }
        
        return $html;

    }



    
    public function getSortableHtml($language_id, $parent_id = 0, $node_formatter = null, $skip_first = false){

        $this->level++;

        $html = '';

        if(!isset($this->id))
            return false;

        // preparo l'array con la gerarchia appiattita
        if($this->nodes_hierarchy_plain === false){

            $this->nodes_hierarchy_plain = Array();
            $nodes_tmp = MenuNode::where('menu_id', '=', $this->id)->where('language_id', '=', $language_id)->orderBy('order', 'asc')->get();

            foreach($nodes_tmp as $item){
                $parent_id = $item->parent_id?$item->parent_id:0;
                $this->nodes_hierarchy_plain[$parent_id][] = $item;    
            }
      
        }

        // elaboro la struttura
        if(isset($this->nodes_hierarchy_plain[$parent_id]) && count($this->nodes_hierarchy_plain[$parent_id]) > 0){

            if(!$skip_first || $this->level > 1)
                $html .= '<ol>';
            
            foreach($this->nodes_hierarchy_plain[$parent_id] as $item){
                
                $html .= '<li id="node_'.$item->id.'">';
                $html .= '<div>';
                
                if(!$node_formatter){
                    $html .= $item->id.') '.$item->title;
                }
                else
                    $html .= $node_formatter($item);

                $html .= '</div>';

                $html .= $this->getSortableHtml($language_id, $item->id, $node_formatter, '');
                $html .= '</li>';
            }
            if(!$skip_first || $this->level > 1)
                $html .= '</ol>';   
        }
        
        return $html;

    }

    public static function handleLink($url){
        if($url == ''){
            return '#';
        }
        else if(substr($url,0,7) == 'http://' || substr($url,0,8) == 'https://'){ 
            return $url;
        }
        else{
            if(substr($url,0,1) != '/')
                    $url = '/'.$url;
            return URL::to('/').$url;
        }
    }
}

?>