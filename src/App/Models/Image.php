<?php namespace BloomDesign\Bud\App\Models;
 
use Log;
 
use File;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Orchestra\Imagine\Facade as Imagine;

class Image {
   
    public static function resize($url, $width = 100, $height = null, $crop = false, $quality = null)
    {

        
        $info = pathinfo($url);
        
        if ( ! $height) $height = $width;
        $fileName       = $info['basename'];
        $sourceDirPath  = public_path() .'/'. $info['dirname'];
        $sourceFilePath = $sourceDirPath . '/' . $fileName;
        $targetDirName  = $width . 'x' . $height . ($crop ? '_crop' : '');
        $targetDirPath  = public_path() . '/'. Option::value('cache_folder') . $targetDirName . '/';
        $targetFilePath = $targetDirPath . $fileName;
        $targetUrl      = asset(Option::value('cache_folder') . $targetDirName . '/' . $fileName);
        
        try
        {
            
            if ( ! File::isDirectory($targetDirPath) && $targetDirPath != '') {
                File::makeDirectory($targetDirPath, 0777, true);
                
            }

            $size   = new Box($width, $height);

            $mode = $crop ? ImageInterface::THUMBNAIL_OUTBOUND : ImageInterface::THUMBNAIL_INSET;
            
            if ( !File::exists($targetFilePath) or (File::lastModified($targetFilePath) < File::lastModified($sourceFilePath)))
            {
                Imagine::open($sourceFilePath)
                              ->thumbnail($size, $mode)
                              ->save($targetFilePath, array('quality' => $quality));
            }
        }
        catch (\Exception $e)
        {
            
            Log::error('[IMAGE SERVICE] Failed to resize image ' . $url . ' [' . $e->getMessage() . ']');
        }

        return $targetUrl;
        
        
         
    }
 
    public function thumb($url, $width, $height = null)
    {
        return $this->resize($url, $width, $height, true);
    }
    
    public static function extend($url, $width = 100, $height = null, $quality = null)
    {

        $i = new \Imagine\Gd\Imagine();
        //$i = new \Imagine\Imagick\Imagine();

        $info = pathinfo($url);
        
        if ( ! $height) $height = $width;
        $fileName       = $info['basename'];
        $sourceDirPath  = public_path() .'/'. $info['dirname'];
        $sourceFilePath = $sourceDirPath . '/' . $fileName;
        $targetDirName  = $width . 'x' . $height . '_extend';
        $targetDirPath  = public_path() . '/'. Option::value('cache_folder') . $targetDirName . '/';
        $targetFilePath = $targetDirPath . $fileName;
        $targetUrl      = asset(Option::value('cache_folder') . $targetDirName . '/' . $fileName);
        
        try
        {
            if ( ! File::isDirectory($targetDirPath) && $targetDirPath != '') {
                File::makeDirectory($targetDirPath, 0777, true);
            }

            $size   = new Box($width, $height);

            $mode = ImageInterface::THUMBNAIL_INSET;
            
            if ( !File::exists($targetFilePath) or (File::lastModified($targetFilePath) < File::lastModified($sourceFilePath)))
            {

                $palette = new \Imagine\Image\Palette\RGB();

                $color = $palette->color('#000', 0);
                $container = $i->create(new \Imagine\Image\Box($width, $height), $color);

                $image = $i->open($sourceFilePath)
                              ->thumbnail($size, $mode);

                $thumb_size = $image->getSize();
                $thumb_width = $thumb_size->getWidth();
                $thumb_height = $thumb_size->getHeight();

                if($thumb_width >= $thumb_height){
                    $x = 0;
                    $y = ($thumb_width - $thumb_height) / 2;
                }
                else{
                    $y = 0;
                    $x = ($thumb_height - $thumb_width) / 2;
                }

                $container->paste($image, new \Imagine\Image\Point($x, $y));

                $container->save($targetFilePath, array('quality' => $quality));

                
            }
        }
        catch (\Exception $e)
        {
            
            Log::error('[IMAGE SERVICE] Failed to resize image ' . $url . ' [' . $e->getMessage() . ']');
        }

        return $targetUrl;
        
        
         
    }
    
    
    
    public static function resizeLocal($url, $width = 100, $height = null, $crop = false, $quality = null)
    {
        if ($url)
        {
            $info = pathinfo($url);
 
            if ( ! $height) $height = $width;
 
            $quality = ($quality) ? $quality : 70;
 
            $fileName       = $info['basename'];
            $sourceDirPath  = $info['dirname'];
            //$sourceDirPath  = public_path() . $info['dirname'];
            $sourceFilePath = $sourceDirPath . '/' . $fileName;
            $targetDirName  = $width . 'x' . $height . ($crop ? '_crop' : '');
            $targetDirPath  = $sourceDirPath . '/' . $targetDirName . '/';
            $targetFilePath = $targetDirPath . $fileName;
            
            
 
            try
            {
                if ( ! File::isDirectory($targetDirPath) and $targetDirPath) @File::makeDirectory($targetDirPath);
 
                //$size = new \Imagine\Image\Box($width, $height);
 
                //$mode = $crop ? \Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND : \Imagine\Image\ImageInterface::THUMBNAIL_INSET;
                $size   = new Box($width, $height);

                $mode = $crop ? ImageInterface::THUMBNAIL_OUTBOUND : ImageInterface::THUMBNAIL_INSET;
            
 
                if (! File::exists($targetFilePath) or (File::lastModified($targetFilePath) < File::lastModified($sourceFilePath)))
                {
                    Imagine::open($sourceFilePath)
                                  ->thumbnail($size, $mode)
                                  ->save($targetFilePath, array('quality' => $quality));
                }
            }
            catch (\Exception $e)
            {
                
                Log::error('[IMAGE SERVICE] Failed to resize image &quot;' . $url . '&quot; [' . $e->getMessage() . ']');
            }
 
            return $targetFilePath;
        }
    }


    public static function pdfPreview($pdf, $width, $quality = 70){

        if(!class_exists('Imagick'))
            return false;

        $info = pathinfo($pdf);
 
        $quality = ($quality) ? $quality : 70;

        $fileName       = $info['basename'];
        $sourceDirPath  = $info['dirname'];
        $sourceFilePath = $sourceDirPath . '/' . $fileName;
        $targetDirName  = 'pdf_preview';
        $targetDirPath  = $sourceDirPath . '/' . $targetDirName . '/';
        $targetFilePath = $targetDirPath . $fileName.'.jpg';


        try
            {
                if ( ! File::isDirectory($targetDirPath) and $targetDirPath) @File::makeDirectory($targetDirPath);
 
                // //$size = new \Imagine\Image\Box($width, $height);
 
                // //$mode = $crop ? \Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND : \Imagine\Image\ImageInterface::THUMBNAIL_INSET;
                // $size   = new Box($width, $height);

                // $mode = $crop ? ImageInterface::THUMBNAIL_OUTBOUND : ImageInterface::THUMBNAIL_INSET;
            
 
                if (! File::exists($targetFilePath) or (File::lastModified($targetFilePath) < File::lastModified($sourceFilePath)))
                {
                    //$fp_pdf = fopen($pdf, 'rb');

                    $img = new \Imagick(); // [0] can be used to set page number
                    
                    $img->setResolution(300,300);
                    $img->readImage($pdf.'[0]');

                    $img->setImageFormat( "jpg" );
                    $img->setImageCompression(\Imagick::COMPRESSION_JPEG); 
                    $img->setImageCompressionQuality(95); 

                    $img->setImageUnits(\Imagick::RESOLUTION_PIXELSPERINCH);
                    file_put_contents ($targetFilePath, $img);

                    // // calcolo l'altezza
                    $w = $img->getImageWidth();
                    $h = $img->getImageHeight();

                    $height = $width * ($h / $w);

                    //$img->resizeImage($w, $h, \Imagick::FILTER_LANCZOS, 0.9, TRUE);
                    
                    $size   = new Box($width, $height);

                    Imagine::open($targetFilePath)
                                  ->thumbnail($size, ImageInterface::THUMBNAIL_INSET)
                                  ->save($targetFilePath, array('quality' => $quality));



                    //$data = $img->getImageBlob(); 
                    
                    
                }
            }
            catch (\Exception $e)
            {
                throw($e);
                Log::error('[IMAGE SERVICE] Failed to create PDF preview &quot;' . $filename . '&quot; [' . $e->getMessage() . ']');
            }
 
            return $targetFilePath;


    }
}