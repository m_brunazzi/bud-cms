<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blm_attachment', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('label');
            $table->text('name')->nullable();
            $table->text('description')->nullable();
            $table->boolean('online')->nullable()->default(true);
            $table->integer('type_id')->nullable();
            $table->string('filename')->nullable();
            $table->string('original_filename')->nullable();
            $table->string('extension')->nullable();
            $table->string('mime_type',32)->nullable();
            $table->integer('size')->nullable();
            $table->boolean('is_image')->nullable()->default(false);
            
            $table->text('meta')->nullable();
            $table->integer('download_count')->nullable()->default(0);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blm_attachment');
    }
}
