<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarouselSlideTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('blm_carousel_slide', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('carousel_id');
			$table->string('label', 128);
			$table->text('name')->nullable();
			$table->text('summary')->nullable();
			$table->string('image')->nullable();
			$table->string('background_video')->nullable();
			$table->string('link_text')->nullable();
			$table->string('link_url')->nullable();
			$table->boolean('link_blank')->nullable()->default(false);
			$table->integerr('order')->nullable()->default(0);
			$table->boolean('online')->nullable()->default(true);
			$table->text('extra')->nullable();
			$table->integer('caption_position')->nullable()->default(5);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('blm_carousel_slide');
	}

}
