<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategory extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('blm_category', function(Blueprint $table)
		{
                    $table->increments('id');
                    $table->string('label', 128);
                    $table->integer('parent_id')->nullable();
                    $table->text('name')->nullable();
                    $table->text('description')->nullable();
                    $table->boolean('online')->nullable()->default(true);
                    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('blm_category');
	}

}
