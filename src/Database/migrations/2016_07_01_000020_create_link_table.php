<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blm_link', function (Blueprint $table) {
            $table->increments('id');
            $table->string('link');
            $table->integer('language_id')->nullable();
            $table->integer('linkable_id');
            $table->string('linkable_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blm_link');
    }
}
