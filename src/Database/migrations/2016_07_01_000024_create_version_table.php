<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVersionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blm_version', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('version_number');
            $table->integer('versionable_id');
            $table->string('versionable_type');
            $table->text('data')->nullable();
            $table->integer('user_id')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blm_version');
    }
}
