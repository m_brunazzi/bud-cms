<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachmentableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blm_attachmentable', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('attachment_id');
            $table->integer('attachmentable_id');
            $table->string('attachmentable_type');
            $table->boolean('is_image_main')->nullable();
            $table->integer('order')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blm_attachmentable');
    }
}
