<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class MenuSeeder extends Seeder {

	public function run()
        {
            Menu::unguard();
            DB::table('blm_menu')->truncate();
            DB::table('blm_menu_node')->truncate();
            
            $name_json = json_encode(Array('1' => 'Menu Principale'));
            $description_json = json_encode(Array('1' => ''));
            
            $menu = new Menu;
            $menu->label = 'Menu principale';
            $menu->name = $name_json;
            $menu->description = $description_json;
            
            $menu->save();
            
        }

}
