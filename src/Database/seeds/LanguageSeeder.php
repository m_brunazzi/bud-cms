<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class LanguageSeeder extends Seeder {

	public function run()
        {
            Language::unguard();
            DB::table('blm_language')->truncate();
            
            $language = new Language;
            $language->id = 1;
            $language->language = 'Italiano';
            $language->code = 'it';
            $language->online = true;
            $language->save();
        }

}
