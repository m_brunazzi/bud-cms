<?php

use Illuminate\Database\Seeder;

class OptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        $data = 
            [
                [
                'name' => 'admin_email',
                'type' => 'text',
                'value' => 'dev@bl8m.it'
                ],
                [
                'name' => 'admin_items_per_page',
                'type' => 'number',
                'value' => 20
                ],
                [
                'name' => 'admin_thumbnail_height',
                'type' => 'number',
                'value' => 75
                ],
                [
                'name' => 'admin_thumbnail_width',
                'type' => 'number',
                'value' => 75
                ],
                [
                'name' => 'images_folder',
                'type' => 'text',
                'value' => 'upload/images/'
                ],
                [
                'name' => 'files_folder',
                'type' => 'text',
                'value' => 'upload/files/'
                ],
                [
                'name' => 'cache_folder',
                'type' => 'text',
                'value' => 'upload/cache/'
                ],
                [
                'name' => 'session_language_var',
                'type' => 'text',
                'value' => 'BLOOM_VAR_LANGUAGE'
                ],
                [
                'name' => 'site_name',
                'type' => 'text',
                'value' => 'Bloom CMS Base Site'
                ],
                [
                'name' => 'contact_email',
                'type' => 'text',
                'value' => 'dev@bl8m.it'
                ],
            ];
        
        Option::insert($data);
    }
}
