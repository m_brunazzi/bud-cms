<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class BudSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();


        $this->call(LanguageSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(MenuSeeder::class);
        $this->call(OptionSeeder::class);

        Model::reguard();
    }
}
