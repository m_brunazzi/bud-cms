<?php

namespace BloomDesign\Bud;

use Illuminate\Support\ServiceProvider;


class BudServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        
        $this->app['router']->aliasMiddleware('bloom.auth', 'BloomDesign\Bud\Http\Middleware\BloomAuth');
        $this->app['router']->aliasMiddleware('bloom.routing', 'BloomDesign\Bud\Http\Middleware\BloomRouting');
        
        
        // Publishing SASS
        $this->publishes([
            __DIR__.'/Resources/assets/sass' => resource_path('assets/sass'),
        ], 'bud');

        // Publishing JS
        $this->publishes([
            __DIR__.'/Resources/assets/js' => resource_path('assets/js'),
        ], 'bud');


     
        // MIGRATIONS & SEEDS
        $this->publishes([
            __DIR__.'/Database' => database_path(),
        ], 'bud');
        
		$this->loadMigrationsFrom(__DIR__.'/Database/migrations');

        // PER procedere con la pubblicazione degli assets:
        // php artisan vendor:publish --tag=bud --force
        
        
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        
        //require __DIR__ . '/../vendor/autoload.php';
        
        
        // SERVICE PROVIDERS
        
        $this->app->register('Collective\Html\HtmlServiceProvider');
        $this->app->register(\Krucas\Notification\NotificationServiceProvider::class);
        $this->app->register(\Orchestra\Imagine\ImagineServiceProvider::class);
        $this->app->register(\Torann\LaravelMetaTags\MetaTagsServiceProvider::class);
        //$this->app->register('Laravel\Socialite');
        
        // // VIEWS
        // to override package views, put the new ones in resources/views/vendor/bud
        $this->loadViewsFrom(__DIR__.'/Resources/Views', 'bud');
        
        // ROUTES
        include __DIR__.'/Http/routes.php';
                
        // ALIASES
        
        $alias_loader = \Illuminate\Foundation\AliasLoader::getInstance();
        
        // Bud CMS Models
        $alias_loader->alias('Language', \BloomDesign\Bud\App\Models\Language::class);
        $alias_loader->alias('User',  \BloomDesign\Bud\App\Models\User::class);
        $alias_loader->alias('Group',  \BloomDesign\Bud\App\Models\Group::class);
        $alias_loader->alias('Content',  \BloomDesign\Bud\App\Models\Content::class);
        $alias_loader->alias('Image',  \BloomDesign\Bud\App\Models\Image::class);
        $alias_loader->alias('Attachment', \BloomDesign\Bud\App\Models\Attachment::class);
        $alias_loader->alias('Article', \BloomDesign\Bud\App\Models\Article::class);
        $alias_loader->alias('Category', \BloomDesign\Bud\App\Models\Category::class);
        $alias_loader->alias('Carousel', \BloomDesign\Bud\App\Models\Carousel::class);
        $alias_loader->alias('CarouselSlide', \BloomDesign\Bud\App\Models\CarouselSlide::class);
        // $alias_loader->alias('Country', \BloomDesign\Bud\App\Models\Country::class);
        $alias_loader->alias('Menu', \BloomDesign\Bud\App\Models\Menu::class);
        $alias_loader->alias('MenuNode', \BloomDesign\Bud\App\Models\MenuNode::class);
        $alias_loader->alias('Picture', \BloomDesign\Bud\App\Models\Picture::class);
        $alias_loader->alias('Tag', \BloomDesign\Bud\App\Models\Tag::class);
        $alias_loader->alias('Attachment', \BloomDesign\Bud\App\Models\Attachment::class);
        $alias_loader->alias('AttachmentType', \BloomDesign\Bud\App\Models\AttachmentType::class);
        $alias_loader->alias('Password', \BloomDesign\Bud\App\Models\Password::class);
        $alias_loader->alias('Option', \BloomDesign\Bud\App\Models\Option::class);
        $alias_loader->alias('Link', \BloomDesign\Bud\App\Models\Link::class);
        $alias_loader->alias('Version', \BloomDesign\Bud\App\Models\Version::class);
        $alias_loader->alias('Metadata', \BloomDesign\Bud\App\Models\Metadata::class);
        
        $alias_loader->alias('Input', \Illuminate\Support\Facades\Input::class);
        $alias_loader->alias('Form', \Collective\Html\FormFacade::class);
        $alias_loader->alias('Html', \Collective\Html\HtmlFacade::class);
        
        $alias_loader->alias('Notification', \Krucas\Notification\Facades\Notification::class);
        $alias_loader->alias('Meta', \Torann\LaravelMetaTags\Facades\MetaTag::class);
        
               
        
        /*
        $this->app->alias('Imagine', 'Orchestra\Imagine\Facade');
        $this->app->alias('Socialize', 'Laravel\Socialite\Facades\Socialite');
         */
        
    }
}
