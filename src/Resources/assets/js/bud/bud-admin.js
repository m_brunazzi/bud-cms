require('../bootstrap');

require('summernote');



window.Vue = require('vue');


import AttachmentList from './vue/attachment-list.vue';
//import AuthAttachmentList from './vue/auth-attachment-list.vue';
import PasswordWidget from './vue/password-widget.vue';
import MenuEditor from './vue/menu-editor.vue';
import TranslationEditor from './vue/translation-editor.vue';
import FileSelector from './vue/file-selector.vue';
import TagsManager from './vue/tags-manager.vue';
import TypeaheadInput from './vue/typeahead-input.vue';
import CarouselEditor from './vue/carousel-editor.vue';
import LinkWidget from './vue/link-widget.vue';
import VersionWidget from './vue/version-widget.vue';
import MetadataWidget from './vue/metadata-widget.vue';
import Bud from './bud-service';


window.onload = function () {
	
	const app = new Vue({
	  el: '.main-container',
	  components: { AttachmentList, 
	  				//AuthAttachmentList, 
	  				PasswordWidget, 
	  				MenuEditor, 
	  				TranslationEditor, 
	  				FileSelector,
	  				TagsManager, 
	  				TypeaheadInput, 
	  				CarouselEditor, 
	  				LinkWidget,
	  				VersionWidget,
	  				MetadataWidget,
	  			},
	  mounted: function(){
  	
			  	

	  	// HTML EDITOR with image upload support
	  	this.activateEditor();

	  	// DATEPICKER
	    // $('.datepicker').datepicker({
	    //     format: 'dd/mm/yyyy',
	    //     language: 'it',
	    //     todayHighlight: true
	    // });

	    // $(document).on('click', '.copy-translation-btn', function(e){
    	// 	var widget_name = $(e.target).attr('data-widget-name');
    	// 	var source_id = $(e.target).attr('data-source-id');
    	// 	var destination_id = $(e.target).attr('data-destination-id');
    		
    	// 	var source = $('[name=' + ( widget_name + '\\[' + source_id + '\\]') + ']');
    	// 	var destination = $('[name=' + ( widget_name + '\\[' + destination_id + '\\]') + ']');
    		
    	// 	destination.val(source.val());
	    // });


	    // CONFIRM ACTION on danger buttons
	    //$('.btn-danger').click(function(e){
	    $(document).on('click', '.btn-danger', function(e){
	        e.preventDefault();

	        var target = $(e.target);
	        
	        if(confirm('Procedere con l\'operazione selezionata?')){
	            if(target.is('input[type=submit]') || target.is('button[type=submit]')){
	                var form = target.closest('form');
	                form.submit();
	            }
	            else{
	            	console.log(e);
	                var url = target.attr('href');
	                if(url != undefined)
		                window.location.href = url;
		            else
		            	console.log('Url di redirect non definita');
	            }
	        }
	        
	    });


	  },
	  methods: {
	  		activateEditor: function(){

	  			// SUMMERNOTE
	  			$('.html-editor').summernote({
			        height: 400,
			        toolbar: [
			            // [groupName, [list of button]]
			            ['style', [ 'style', 'bold', 'italic', 'clear']],
			            ['para', ['ul', 'ol', 'paragraph']],
			            ['insert', ['link', 'picture', 'video', 'hr', 'table']],
			             ['view', ['fullscreen', 'codeview']],
			             ['actions', ['undo', 'redo']],
			          ],
				    callbacks : {
				        onImageUpload: function(image) {

				            var data = new FormData();
						    data.append("image", image[0]);
						    var _this = $(this);

						    var ajax_params = {
						    	contentType: false,
			        			processData: false
						    }; 

					    	Bud.rpcCall(BASE_URL + '/rpc/html_editor/upload_image', data, function(result){
						  		var image = result.data.filename;
					           	_this.summernote("insertImage", image);
						  	}, ajax_params);
				        }
				    }
			    });
			    // SUMMERNOTE END
	  		}
	  }
	});

	
}
