import _ from 'jquery';
import axios from 'axios'; 

//require('toastr');

export default {

	init : function(){

		// LARAVEL TOKEN 

		$.ajaxPrefilter(function(options, originalOptions, xhr) { // this will run before each request

	        var token = $('meta[name="csrf-token"]').attr('content'); // or _token, whichever you are using

	        if (token) {
	            return xhr.setRequestHeader('X-CSRF-TOKEN', token); // adds directly to the XmlHttpRequest Object
	        }

	    });

	    this.loading(false);

	},

	

	rpcCall : function(url, data = [], callback = null, ajax_params = [], toastr = true, cache = false){

		this.init();

		var _this = this;


		var url_normalized = url + this.serialize(data);

		var storage_supported = typeof(Storage) !== "undefined";

		if(storage_supported && cache && localStorage.getItem(url_normalized) != undefined){
			
			console.log('Response cached for ' + url_normalized);
			var response = JSON.parse(localStorage.getItem(url_normalized));
			
			
			_this.handleResponse(response, callback);
			
		}
		else{
			
			var ajax_defaults = {
		        data: data,
		        method: 'post',
		        url: url,
		        cache: false,
		        dataType: 'json',
		        success: function(response) {


		        	if(storage_supported && cache){
		        		localStorage.setItem(url_normalized, JSON.stringify(response));
		        	}

		        	_this.handleResponse(response, callback);
		        }

		    };

		    $.extend(ajax_defaults, ajax_params);
			$.ajax(ajax_defaults);
			
		}


	},

	handleResponse: function(response, callback = null){

		if(response.status == '1'){
    		callback(response);
    	}
    	
    	this.notifyResponse(response);
	},


	notifyResponse: function (response) {

		if(response == undefined)
			return;
		

        if (response.status != undefined && response.status == '1') {

            if (response.message != undefined)
                this.notification.success(response.message);

        } else {

            if (response.message != undefined) {

                this.notification.error(response.message);

            } else {

                this.notification.error('Errore nella richiesta');

            }

        }

    },

    serialize: function(obj) {
	  var str = [];
	  for(var p in obj)
	    if (obj.hasOwnProperty(p)) {
	      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
	    }
	  return str.join("&");
	},

	notification: require('toastr'),


	loading: function(is_loading){
		if(is_loading == undefined || is_loading == true)
			$('.bud-loading').show();
		else
			$('.bud-loading').hide();

	}




};



