import axios from 'axios';

const EventEmitter = require( 'events' );

export default {

	params: {
		url: '',
		method: 'POST',
		data: {}
        
	},

    rpc_calls: [],
	enable_notification: true,

	rpc: function(params, thenCallback, catchCallback){


        var signature = String(Date.now()) + '-' + String(Math.floor((Math.random() * 1000000000) + 1));
        this.pushRpcCall(signature);


		this.params = Object.assign(this.params, params);

		var self = this;

		axios(this.params)
        .then(function(response){

            self.popRpcCall(signature);

        	self.notifyResponse(response.data);
        	if(typeof thenCallback === "function")
        		thenCallback(response);
        })
        .catch(function(error){

            self.popRpcCall(signature);

            self.notification.error(error);
            if(typeof catchCallback === "function")
        		catchCallback(error);
        })

		

	},

    pushRpcCall(signature){
        
        if(this.rpc_calls.length == 0){
            console.log('Caricamento iniziato');            
            //this.emit('loading-start');
        }

        this.rpc_calls.push(signature);
    },

    popRpcCall(signature){

        var index = this.rpc_calls.indexOf(signature);
        if (index > -1) {
          this.rpc_calls.splice(index, 1);
        } 

        if(this.rpc_calls.length == 0){
            console.log('Caricamento terminato');         
            //this.emit('loading-end');   
        }
    },

	notifyResponse: function (response) {

		if(!this.enable_notification)
			return;

		if(response == undefined)
			return;
		

        if (response.status != undefined && response.status == '1') {

            if (response.message != undefined)
                this.notification.success(response.message);

        } else {

            if (response.message != undefined) {

                this.notification.error(response.message);

            } else {

                this.notification.error('Errore nella richiesta');

            }

        }

    },

	notification: require('toastr'),


}