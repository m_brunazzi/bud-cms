@extends('bud::admin._layouts.default')
 
@section('main')

@php
    $breadcrumbs = [
        'Dashboard' => route('admin.dashboard'),
        'Carousel' => ''
    ];
@endphp

@include('bud::admin._partials.breadcrumbs', ['breadcrumbs' => $breadcrumbs])


 
<h1>Carousel - Elenco</h1>

<div class="well">
    
    <div class="row">
        <div class="col-sm-9">
            Record trovati: <strong>{!! $rows->total() !!}</strong> - <a href="{!!URL::route('admin.Carousel.create')!!}" class="btn btn-primary">Nuovo Record</a>
        </div>
        <div class="col-sm-3">
            <div class="pull-right">
                {!! Form::open(array('role' => 'form', 'route' => 'admin.Carousel.index', 'method' => 'GET', 'class' => 'form-inline')) !!}
                <div class="form-group">
                    <div class="input-group">
                    <input type="text" name="search" class="form-control m-1" value="{!! $search !!}">
                        <span class="input-group-btn">
                            <button class="btn btn-default m-1" type="submit">Cerca</button>
                        </span>
                    </div>
                </div>
                {!! Form::close() !!}
           </div>
        </div>
    </div>

</div>

{!! Notification::showAll() !!}

@if (count($rows) > 0)
    <table class="table table-hover ">
        <thead>
            <tr>
                <th>&nbsp;</th>
                <th>ID</th>
                <th>Etichetta</th>
                <th>Slug</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        
    @foreach ($rows as $row)
        <tr>
            
            <td>
                @if(!empty($row->image))
                    <img src="{!! Image::resize($row->image, Option::value('admin_thumbnail_width'), Option::value('admin_thumbnail_height'), true) !!}" class="img-thumbnail"></td>
                @endif
            <td><span class="badge badge-primary">{!! $row->id !!}</span></td>
            
            <td><strong>{!! $row->label !!}</strong></td>
            <td><em>{!! $row->slug !!}</em></td>
            <td class="text-right">
                <a href="{!!URL::route('admin.Carousel.edit', $row->id)!!}" class="btn btn-primary">Modifica</a>
                {!! Form::open(array('route' => array('admin.Carousel.destroy', $row->id), 'method' => 'delete', 'class' => 'form-delete')) !!}
                        <button type="submit" href="{!! URL::route('admin.Carousel.destroy', $row->id) !!}" class="btn btn-danger btn-mini">Cancella</button>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </table>
    {!! $rows->appends($query_string)->render() !!}
@else
    <div class="alert alert-info">Nessun record trovato</div>
@endif


@stop