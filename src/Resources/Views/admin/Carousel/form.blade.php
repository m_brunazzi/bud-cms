@extends('bud::admin._layouts.content_form')
 
@section('nav_pills')
<li class="nav-item"><a href="#slides" class="nav-link" data-toggle="tab">Slides</a></li>
@stop

@section('nav_panels')
<div class="tab-pane fade in" id="slides">
                
        @if(isset($record) && !empty($record->id))
            <div class="well">
                <carousel-editor carousel-id="{{ $record->id }}" rpc-url="{{ route('rpc.carousel.manager') }}"></carousel-editor>
            </div>
        @else
            <div class="alert alert-warning">
                Prima di poter gestire le slides è necessario salvare il record
            </div>
        @endif
            
        
    </div>
@stop

@section('form_content')
 



@if (isset($record))
    {!! Form::model($record, array('route' => Array('admin.Carousel.update', $record->id), 'role' => 'form', 'method' => 'PUT', 'files' => true)) !!}
@else
    {!! Form::open(array('role' => 'form', 'route' => 'admin.Carousel.store', 'method' => 'POST', 'files' => true)) !!}
@endif
            @if ($errors->has('login'))
                <div class="alert alert-error">{!! $errors->first('login', ':message') !!}</div>
            @endif
            
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        {!! Form::label('label', 'Etichetta') !!}
                        {!! Form::text('label', null, Array('class'=> 'form-control' ,'placeholder' => 'label')) !!}
                    </div>
                </div>
                <div class="col-sm-6">
                     <div class="form-group">
                        {!! Form::label('slug', 'Slug') !!}
                        {!! Form::text('slug', null, Array('class'=> 'form-control' ,'placeholder' => 'slug')) !!}
                    </div>
                </div>
            </div>
            
            
            <div class="form-actions">
                {!! Form::submit('Salva', array('class' => 'btn btn-block btn-primary')) !!}
            </div>
 
        {!! Form::close() !!}

        


@stop
