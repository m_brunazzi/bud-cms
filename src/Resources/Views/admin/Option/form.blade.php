@extends('bud::admin._layouts.default')

@section('main')






@php
    $breadcrumbs = [
        'Dashboard' => route('admin.dashboard'),
        'Opzioni' => route('admin.Option.index'),
        'Modulo' => ''
    ];
@endphp

@include('bud::admin._partials.breadcrumbs', ['breadcrumbs' => $breadcrumbs])

@include('bud::admin._partials.form_header', ['title' => @$record->name])

{!! Notification::showAll() !!}

@if ($errors->any())
<div class="alert alert-danger">
    {!! implode('<br>', $errors->all()) !!}
</div>
@endif
@if (isset($record))
{!! Form::model($record, array('route' => Array('admin.Option.update', $record->id), 'role' => 'form', 'method' => 'PUT', 'files' => true)) !!}
@else
{!! Form::open(array('role' => 'form', 'route' => 'admin.Option.store', 'method' => 'POST', 'files' => true)) !!}
@endif

<div class="row">
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::label('name', 'Nome') !!}
            {!! Form::text('name', null, Array('class'=> 'form-control' ,'placeholder' => 'name')) !!}
        </div>
    </div>
    <div class="col-sm-4">
        {{--
        <div class="form-group">
            <br/>
            <label for="mandatory">{!! Form::checkbox('mandatory', 1) !!} Obbligatorio</label>
        </div>
        --}}
        <div class="form-group">
            {!! Form::label('type', 'Tipo') !!}
            {!! Form::select('type', Option::getTypes(), null, Array('class'=> 'form-control')) !!}
        </div>
    </div>

</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('description', 'Descrizione') !!}
            {!! Form::textarea('description', null, Array('class'=> 'form-control' ,'placeholder' => 'description')) !!}
        </div>
    </div>
    <div class="col-sm-6">
        
        
        <div class="options-container">
            <div class="row">
                <div class="col-sm-10">
                    <h4>Valori disponibili</h4>
                </div>
                <hr>
                <div class="col-sm-2">
                    <button class="btn btn-block btn-sm btn-info btn-add"><i class="fa fa-plus"></i></button>
                </div>
            </div>
            
            <div class="options-values">

            </div>
        </div>
        
    </div>

</div>




<div class="form-actions">
    {!! Form::submit('Salva', array('class' => 'btn btn-block btn-primary')) !!}
</div>


{!! Form::close() !!}

    
@stop




@section('after_footer')

<script>
$(document).ready(function(){
    handle_options();
    
    @if(isset($record) && $options = Option::decodeOptions($record->options))
        @foreach($options as $key => $value)
            draw_row('{{ $key }}', '{{ $value }}');
        @endforeach
    @endif
});

$(document).on('change', 'select[name=type]', function(){
    handle_options();
});

$(document).on('click', '.btn-add', function(e){
    e.preventDefault();
    draw_row();
});

$(document).on('click', '.btn-delete', function(e){
//$('.btn-delete').click(function(e){
    e.preventDefault();
    //console.log($(e.target).closest('.option-row').attr('test'));
    $(e.target).closest('.option-row').remove();
});

function handle_options(){
    var type = $('select[name=type]').val();
    if(type == 'select' || type == 'radio'){
        $('.options-container').show();
    }
    else{
        $('.options-container').hide();
    }
}



function draw_row(key, value){
    
    if(key == undefined)
        key = '';
    
    if(value == undefined)
        value = '';
    
    html = '<div class="row option-row">';
    html += '<div class="col-sm-5">';
    html += '<div class="form-group"><input type="text" class="form-control input-sm" placeholder="key" name="option_key[]" value="' + key + '"></div>';
    html += '</div>';
    html += '<div class="col-sm-5">';
    html += '<div class="form-group"><input type="text" class="form-control input-sm" placeholder="value" name="option_value[]" value="' + value + '"></div>';
    html += '</div>';
    html += '<div class="col-sm-2">';
    html += '<button class="btn btn-sm btn-danger btn-delete btn-block"><i class="fa fa-remove"></i></button>';
    html += '</div>';
    html += '</div>';
    
    
    $('.options-values').append(html);
}



</script>
@stop

