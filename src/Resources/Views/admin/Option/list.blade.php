@extends('bud::admin._layouts.default')
 
@section('main')
 
@php
    $breadcrumbs = [
        'Dashboard' => route('admin.dashboard'),
        'Opzioni' => ''
    ];
@endphp

@include('bud::admin._partials.breadcrumbs', ['breadcrumbs' => $breadcrumbs])

<h1>Opzioni</h1>


@if(count($rows) > 0)

    {!! Form::open(['route' => 'admin.Option.saveAll']) !!}
    @foreach($rows as $row)
    <hr/>
    <div class="row" id="option_row_{{ $row->id }}">
    <div class="col-sm-3 text-right">
        <h4>{{ $row->name }}
        @if($row->mandatory)
            *
        @endif
        </h4>
        <p><small>{{ $row->description }}</small></p>
    </div>
    <div class="col-sm-6">
        @if($row->type == "boolean")
        <div class="radio">
        <input name="option_{{ $row->id }}" type="checkbox" value="1" {{ $row->value == '1'?'checked="checked"':'' }}> 
        </div>
        @elseif($row->type == "text")
        <textarea name="option_{{ $row->id }}" class="form-control" rows="4">{{ $row->value }}</textarea>
        @elseif($row->type == "number")
        <input name="option_{{ $row->id }}" type="number" value="{{ $row->value }}" class="form-control">
        @elseif($row->type == "select")
        <select  name="option_{{ $row->id }}" class="form-control">
            <option value="">---</option>
            @foreach($row->getOptions() as $key => $value)
            <option value="{{ $key }}" {{ $key == $row->value?'selected="selected"':'' }}>{{ $value }}</option>
            @endforeach
        </select>
        @elseif($row->type == "radio")
            @foreach($row->getOptions() as $key => $value)
            <div class="radio">
            <label><input type="radio" name="option_{{ $row->id }}" value="{{ $key }}" {{ $key == $row->value?'checked="checked"':'' }}>{{ $value }}</label>
            </div>
            @endforeach 
            <small><a href="#option_row_{{ $row->id }}" onClick="resetRadio('option_{{ $row->id }}')">Reset radiobox</a></small>
        @endif
    </div>
    <div class="col-sm-3">
        <div class="edit-toggle">
            <a href="{!!URL::route('admin.Option.edit', $row->id)!!}" class="btn btn-primary btn-sm">Modifica</a>
            <a href="{!!URL::route('admin.Option.remove', $row->id)!!}" class="btn btn-danger btn-sm">Cancella</a>
            
        </div>
    </div>
    </div>
    
    @endforeach
    <div class="form-actions">
        {!! Form::submit('Salva', ['class' => 'btn btn-block btn-primary']) !!}
    </div>
{!! Form::close() !!}
@endif
<hr/>
<div class="text-center">
    
    <small><a href="#" class="btn-edit">Attiva Modifica</a>&nbsp;
    <span class="edit-toggle"> - <a href="{{ route('admin.Option.create') }}" class="">Nuova opzione</a></span>
    </small>
</div>


@stop


@section('after_footer')
    <script>
    $(document).ready(function(){
        $('.edit-toggle').hide();
    });

    $(document).on('click', '.btn-edit', function(e){
        e.preventDefault();
        $('.edit-toggle').toggle();
    });


    function resetRadio(radio_name){
        $('input[name=' + radio_name+ ']:checked').removeAttr('checked');
    }

    </script>
@stop