@extends('bud::admin._layouts.basic_form')
 
@section('form_content')




 
@if (isset($record))
    {!! Form::model($record, array('route' => Array('admin.Tag.update', $record->id), 'role' => 'form', 'method' => 'PUT')) !!}
@else
    {!! Form::open(array('role' => 'form', 'route' => 'admin.Tag.store', 'method' => 'POST')) !!}
@endif
          

            <div class="">
                <div class="col-sm-12">
                    
                    <div class="form-group">
                        {!! Form::label('label', 'Etichetta') !!}
                        {!! Form::text('label', null, Array('class'=> 'form-control input-lg ' ,'placeholder' => 'label')) !!}
                    </div>

                    <div class="form-group">
                    {!! Form::label('name', 'Nome') !!}
                    {!! Language::multilanguageWidget('name',  Tag::decodeTranslations(@$record->name), null, false) !!}
                    </div>


                </div>
                
                <!-- SIDEBAR -->
               
            </div>
            
            <div class="form-actions">
                {!! Form::submit('Salva', array('class' => 'btn btn-block btn-primary')) !!}
            </div>
 
        {!! Form::close() !!}
@stop


