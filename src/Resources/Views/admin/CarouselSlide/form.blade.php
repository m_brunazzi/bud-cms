@extends('bud::admin._layouts.content_form')
 
@section('form_content')
 



@if (isset($record->id))
    {!! Form::model($record, array('route' => Array('admin.CarouselSlide.update', $record->id), 'role' => 'form', 'method' => 'PUT', 'files' => true)) !!}
@else
    {!! Form::model($record, array('route' => Array('admin.CarouselSlide.store', $record->id), 'role' => 'form', 'method' => 'POST', 'files' => true)) !!}
    
@endif
            @if ($errors->has('login'))
                <div class="alert alert-error">{!! $errors->first('login', ':message') !!}</div>
            @endif
            
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        {!! Form::label('label', 'Etichetta') !!}
                        {!! Form::text('label', null, Array('class'=> 'form-control' ,'placeholder' => 'label')) !!}
                    </div>
                </div>
                <div class="col-sm-3">
                     <div class="form-group">
                        {!! Form::label('carousel_id', 'Carousel') !!}
                        {!! Form::select('carousel_id', $options_carousel, null, Array('class'=> 'form-control')) !!}
                        @if($record->carousel_id)
                            <div class="text-right"><small><a href="{{ route('admin.Carousel.edit', $record->carousel_id) }}">Vai allo slider</a></small></div>
                        @endif
                    </div>
                </div>
                <div class="col-sm-3">
                     <div class="checkbox">
                         <br><br>
                        <label for="online">{!! Form::checkbox('online', 1) !!} Online</label>
                    </div>
                </div>
            </div>
            
            
            <div class="form-group">
            {!! Form::label('name', 'Nome') !!}
            {!! Language::multilanguageWidget('name',  CarouselSlide::decodeTranslations(@$record->name), null, false) !!}
            </div>
            
            <div class="form-group">
            {!! Form::label('summary', 'Sommario') !!}
            {!! Language::multilanguageWidget('summary',  CarouselSlide::decodeTranslations(@$record->summary)) !!}
            </div>
            
            <div class="row">
                 <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('caption_position', 'Posizione Didascalia') !!}
                        <table class="table">
                            <tr>
                                <td class="text-center">{!! Form::radio('caption_position', 1) !!}<br/><small>Alto SX</small></td>
                                <td class="text-center">{!! Form::radio('caption_position', 2) !!}<br/><small>Alto Centro</small></td>
                                <td class="text-center">{!! Form::radio('caption_position', 3) !!}<br/><small>Alto DX</small></td>
                            </tr>
                            <tr>
                                <td class="text-center">{!! Form::radio('caption_position', 4) !!}<br/><small>Centro SX</small></td>
                                <td class="text-center">{!! Form::radio('caption_position', 5) !!}<br/><small>Centro Centro</small></td>
                                <td class="text-center">{!! Form::radio('caption_position', 6) !!}<br/><small>Centro DX</small></td>
                            </tr>
                            <tr>
                                <td class="text-center">{!! Form::radio('caption_position', 7) !!}<br/><small>Basso SX</small></td>
                                <td class="text-center">{!! Form::radio('caption_position', 8) !!}<br/><small>Basso Centro</small></td>
                                <td class="text-center">{!! Form::radio('caption_position', 9) !!}<br/><small>Basso DX</small></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="form-group">
                        {!! Form::label('link_text', 'Link / Testo') !!}
                        {!! Form::text('link_text', null, Array('class'=> 'form-control' ,'placeholder' => 'link_text')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('link_url', 'Link / Url') !!}
                        {!! Form::text('link_url', null, Array('class'=> 'form-control' ,'placeholder' => 'link_url')) !!}
                    </div>
                    <div class="checkbox">
                         <br/>
                        <label for="link_blank">{!! Form::checkbox('link_blank', 1) !!} Apri il link in una nuova finestra</label>
                    </div>
                </div>
                
            </div>
            
            <hr/>
            <label>Immagine di sfondo</label>
            @include('bud::admin._partials.vue.file_selector', Array('filename' => @$record->image, 'input_name' => 'image'))
            <hr/>
            <label>Video di sfondo</label>
            <small>Indicare il codice del video su Youtube. Nel caso questo campo sia valorizzato, avrà priorità sull'immagine di sfondo</small>
             {!! Form::text('background_video', null, Array('class'=> 'form-control' ,'placeholder' => 'background_video')) !!}

             <hr/>

            <div class="form-actions">
                {!! Form::submit('Salva', array('class' => 'btn btn-block btn-primary')) !!}
            </div>
 
        {!! Form::close() !!}
@stop
