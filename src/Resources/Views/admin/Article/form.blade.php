@extends('bud::admin._layouts.content_form')
 
@section('form_content')
 
@if (isset($record))
    {!! Form::model($record, array('route' => Array('admin.Article.update', $record->id), 'role' => 'form', 'method' => 'PUT')) !!}
@else
    {!! Form::open(array('role' => 'form', 'route' => 'admin.Article.store', 'method' => 'POST')) !!}
@endif
          
            

            <div class="row">
                <div class="col-sm-9">
                    
                    <div class="form-group">
                        {!! Form::label('label', 'Etichetta') !!}
                        {!! Form::text('label', null, Array('class'=> 'form-control input-lg ' ,'placeholder' => 'label')) !!}
                    </div>

                    <div class="form-group">
                    {!! Form::label('name', 'Nome') !!}
                    {!! Language::multilanguageWidget('name',  Article::decodeTranslations(@$record->name), null, false) !!}
                    </div>

                    <div class="form-group">
                    {!! Form::label('summary', 'Sottotitolo') !!}
                    {!! Language::multilanguageWidget('summary',  Article::decodeTranslations(@$record->summary), null, false) !!}
                    </div>

                    <div class="form-group">
                    {!! Form::label('name', 'Descrizione') !!}
                    {!! Language::multilanguageWidget('description',  Article::decodeTranslations(@$record->description)) !!}
                    </div>

            
                </div>
                
                <!-- SIDEBAR -->
                <div class="col-sm-3">
                    <aside class="well">
                        <div class="form-check">
                            {!! Form::checkbox('online', 1, null, ['class' => 'form-check-input']) !!}
                            <label class="form-check-label" for="online">
                            Online
                            </label>
                        </div>
                        <hr/>

                        @include('bud::admin._partials.protectable_widget')
                        <hr/>
                        <div class="form-group">
                        <h4>Data</h4>
                        {!! Form::text('date', null, Array('class'=> 'form-control datepicker' ,'placeholder' => 'Date')) !!}    
                        </div>
                        
                        <hr/>
                        
                        <h4>Categorie</h4>

                            <?php
                            $category_checked = Array();
                            if(null !== (@$record->category)){
                                foreach(@$record->category as $item){
                                    $category_checked[] = $item->id;    
                                }
                            }

                            ?>
                            @if(count($categories)> 0)

                                <?php $i = 0 ?>
                                @foreach($categories as $row)
                                   
                                        <div class="checkbox">
                                            <div class="checkbox">
                                                <label >
                                                    {!! Form::checkbox('category['.$row->id.']', $row->id, in_array($row->id,$category_checked)) !!}
                                                    {!! $row->label !!}
                                                </label>
                                            </div>
                                        </div>
                                    
                                    <?php $i++; ?>
                                @endforeach
                            @else
                                <div class="alert alert-info">
                                    Non sono presenti categorie nel database. <a href="{{ route('admin.Category.index') }}" target="_blank">Cliccare qui per crearle</a>.
                                </div>

                            @endif
                            
                            <hr>
                            <h4>Genitore</h4>
                            @if(isset($record))
                                <typeahead-input rpc-url="{{  route('rpc.typeahead.query', ['article'])   }}" input-name="parent_id"  value-id="{{ @$record->father->id  }}" value-label="{{  @$record->father->label  }}"></typeahead-input>
                            @else

                                <div class="alert alert-info">&Egrave; necessario salvare il record prima di specificare il genitore</div>

                            @endif
                               
                        
                            <hr>
                            <h4>Tags</h4>
                            @include('bud::admin._partials.vue.tags-widget')
                        

                            <hr>
                            <h4>Password</h4>
                        
                            @include('bud::admin._partials.vue.password-widget')
                            <hr/>


                            <div class="form-group">
                            <h4>Template custom</h4>
                            {!! Form::text('template', null, Array('class'=> 'form-control input-sm ' ,'placeholder' => 'template')) !!}
                    </div>
                    </aside>
                </div>
            </div>
 
            
            
            <div class="form-actions">
                {!! Form::submit('Salva', array('class' => 'btn btn-block btn-primary')) !!}
            </div>
 
        {!! Form::close() !!}
@stop
