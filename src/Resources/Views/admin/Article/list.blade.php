@extends('bud::admin._layouts.default')
 
@section('main')


@php
    $breadcrumbs = [
        'Dashboard' => route('admin.dashboard'),
        'Articoli' => ''
    ];
@endphp

@include('bud::admin._partials.breadcrumbs', ['breadcrumbs' => $breadcrumbs])

 
<h1>Articoli - Elenco</h1>

<div class="well">
    
    <div class="row">
        <div class="col-sm-4">
            Record trovati: <strong>{!! $rows->total() !!}</strong> - <a href="{!!URL::route('admin.Article.create')!!}" class="btn btn-primary">Nuovo Record</a>
        </div>
        <div class="col-sm-8">
            <div class="pull-right">
                {!! Form::open(array('role' => 'form', 'route' => 'admin.Article.index', 'method' => 'GET', 'class' => 'form-inline'))!!}
                <div class="form-group">
                    {!! Form::select('category_id', $options_category, Input::get('category_id'), Array('class' => 'form-control m-1')) !!}
                </div>
                <div class="form-group">
                    <div class="input-group">
                    <input type="text" name="search" class="form-control m-1" value="{!! $search !!}">
                        <span class="input-group-btn">
                            <button class="btn btn-default m-1" type="submit">Cerca</button>
                        </span>
                    </div>
                </div>
                {!! Form::close() !!}
           </div>
        </div>
    </div>

</div>

{!! Notification::showAll() !!}

@if (count($rows) > 0)
    <table class="table table-hover ">
        <thead>
            <tr>
                <th>ID</th>
                <th>Etichetta</th>
                <th>Categorie</th>
                <th>Links</th>
                
                <th>Online</th>
                
                <th>&nbsp;</th>
            </tr>
        </thead>
        
    @foreach ($rows as $row)
        <tr>
            <td><span class="badge">{!! $row->id !!}</span></td>
            
            <td><strong>{!! $row->label !!}</strong></td>
            <td>
                @foreach($row->category as $item)
                    <a href="{!! route('admin.Article.index') !!}?category_id={!! $item->id !!}"><span class="label label-primary">{!! $item->label !!}</span></a>
                @endforeach
                
            </td>
            <td>
                @foreach($row->link as $item)
                    <small><a href="{{ URL::to($item->link) }}" target="_blank">{{ URL::to($item->link) }}</a></small><br/>
                @endforeach
            </td>

            <td class="text-center"><i class="fa {!! $row->online?'fa-check-square':'fa-remove' !!}"></i></td>
            <td class="text-right">
                <a href="{!!URL::route('admin.Article.edit', $row->id)!!}" class="btn btn-primary">Modifica</a>
                {!! Form::open(array('route' => array('admin.Article.destroy', $row->id), 'method' => 'delete', 'class' => 'form-delete')) !!}
                        <button type="submit" href="{!! URL::route('admin.Article.destroy', $row->id) !!}" class="btn btn-danger btn-mini">Cancella</button>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </table>
    {!! $rows->appends(Input::except('page'))->render() !!}
@else
    <div class="alert alert-info">Nessun record trovato</div>
@endif


@stop