<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="csrf-token" content="{{ csrf_token() }}"/>
	<title>{!! $page_title or '' !!} {!! Option::value('site_name') !!} - Control Panel</title>

	@if(isset($head_assets['css']) && is_array($head_assets['css']) && count($head_assets['css']) > 0)
	@foreach(@$head_assets['css'] as $item)
	<link href="{!! URL::asset($item) !!}" rel="stylesheet">
	@endforeach
	@endif

	@if(isset($head_assets['js']) && is_array($head_assets['js']) && count($head_assets['js']) > 0)
	@foreach(@$head_assets['js'] as $item)
	<script src="{!! URL::asset($item) !!}"></script>
	@endforeach
	@endif
	
	
	
	@yield('head')

	<script type="text/javascript">
		var BASE_URL =  '{!! URL::to('/') !!}';
	</script>
</head>
<body>
	
	
	<nav class="navbar navbar-default no-margin navbar-bud">
    
	    <div class="navbar-header  fixed-brand">
	        
	        <a class="navbar-brand" href="#">{{ Option::value('site_name') }}</a>
	    </div><!-- navbar-header-->

	    <div class="bud-loading">
	    	<i class="fa fa-circle-o-notch  fa-spin"></i><small>Operazione in corso</small>
	    </div>
	</nav>
	<div id="wrapper">
		<div id="sidebar-wrapper">
			@include('bud::admin._partials.sidemenu')
		</div>
		<div id="page-content-wrapper">
			<div class="container-fluid xyz">
				<div class="main-container">
		
					<div class="content-wrapper container-fluid">
						<div class="row">
							<!-- content area -->
							<div class="col-sm-12">
							 @yield('main')
						 </div>
					 </div>
				 </div>
			</div>
			<footer class="footer">
				@include('bud::admin._partials.footer')
			</footer>
		</div>
		<!-- /#page-content-wrapper -->
	</div>
	<!-- /#wrapper -->
</div>





	

	 

	 


@yield('after_footer')



</body>
</html>