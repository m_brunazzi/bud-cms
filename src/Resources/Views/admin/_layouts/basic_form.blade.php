@extends('bud::admin._layouts.default')
 
@section('head')
    <!--<script src="{{ asset('bower_components/jquery-ui/jquery-ui.min.js') }}"></script>-->
@stop

@section('main')

    @if(!empty($form['breadcrumbs']))
       @include('bud::admin._partials.breadcrumbs', ['breadcrumbs' => $form['breadcrumbs']])
    @endif

    @include('bud::admin._partials.form_header', ['title' => @$form["title"]])

    <ul class="nav nav-pills admin-pills" role="tablist">
        
        <li class="nav-item"><a href="#main" class="nav-link active" data-toggle="tab">Informazioni generali</a></li>
        
        
        @yield('nav_pills')

    </ul>

    {!! Notification::showAll() !!}

    @if ($errors->any())
            <div class="alert alert-danger">
                    {!! implode('<br>', $errors->all()) !!}
            </div>
    @endif


        <div class="tab-content">
            <div class="tab-pane fade show active" id="main">
                @yield('form_content')
            </div>

           
            
            @yield('nav_panels')
        </div>
    </div>
    
    
    
<!-- END EDIT -->
@overwrite  
        
