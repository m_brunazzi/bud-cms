@extends('bud::admin._layouts.default')
 
@section('head')
    
@stop

@section('main')

    @if(!empty($form['breadcrumbs']))
       @include('bud::admin._partials.breadcrumbs', ['breadcrumbs' => $form['breadcrumbs']])
    @endif

    @include('bud::admin._partials.form_header', @$form)

    <ul class="nav nav-pills admin-pills" role="tablist">
        
        <li class="nav-item"><a href="#main" class="nav-link active" data-toggle="tab">Informazioni generali</a></li>
        

        @if(isset($record->id))

        @php( $traits = array_map (function($trait){ $reflection = new \ReflectionClass($trait); return $reflection->getShortName(); }, class_uses($record)) )


            {{-- ALLEGATI --}}
            @if(in_array('isAttachmentable', $traits) && !@$form['hide_attachments'])
                <li class="nav-item"><a href="#attachments" class="nav-link" data-toggle="tab">Files allegati</a></li>
            @endif

            {{-- META --}}
            @if(in_array('hasMeta', $traits) && !@$form['hide_attachments'])
                <li class="nav-item"><a href="#meta" class="nav-link" data-toggle="tab">Metadati</a></li>
            @endif

            {{-- LINKS --}}
            @if(in_array('isLinkable', $traits))
                <li class="nav-item"><a href="#links" class="nav-link" data-toggle="tab">Links</a></li>
            @endif

            {{-- REVISIONI --}}
            @if(in_array('isVersionable', $traits))
                <li class="nav-item"><a href="#versions" class="nav-link" data-toggle="tab">Revisioni</a></li>
            @endif

        @endif
        
        @yield('nav_pills')

    </ul>

    {!! Notification::showAll() !!}

    @if ($errors->any())
            <div class="alert alert-danger">
                    {!! implode('<br>', $errors->all()) !!}
            </div>
    @endif

    
        <div class="tab-content">
            <div class="tab-pane fade show active" id="main">
                @yield('form_content')
            </div>

            @if(isset($record->id))
            {{-- ALLEGATI --}}
                @if(in_array('isAttachmentable', $traits) && !@$form['hide_attachments'])
                    <div class="tab-pane fade" id="attachments">
                        @include('bud::admin._partials.vue.attachment_list', ['record' => @$record])
                    </div>
                @endif

                {{-- META --}}
                @if(in_array('hasMeta', $traits))
                    <div class="tab-pane fade" id="meta">
                       @include('bud::admin._partials.vue.metadata_widget', ['record' => @$record])
                    </div>
                @endif

                {{-- LINKS --}}
                @if(in_array('isLinkable', $traits))
                    <div class="tab-pane fade" id="links">
                        @include('bud::admin._partials.vue.link_widget', ['record' => @$record])
                    </div>
                @endif
                
                {{-- REVISIONI --}}
                @if(in_array('isVersionable', $traits))
                    <div class="tab-pane fade" id="versions">
                        @include('bud::admin._partials.vue.version_widget', ['record' => @$record])
                       
                    </div>
                @endif

            

            @endif

            
            
            @yield('nav_panels')
        </div>
    </div>
    
    
    
<!-- END EDIT -->
@overwrite  
        
