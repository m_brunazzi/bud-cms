<div class="multilanguage-widget">
    <!--
    $widget_name, $translations = Array(), $active_language_code = null, $html_editor = true
    -->
@if(@$is_multilanguage)
        <ul class="nav nav-tabs nav-tabs-translation" role="tablist">
        @foreach(self::$available_languages as $id => $language)
            <li role="presentation"  {{ ($id == $active_code?'class="active"':'') }}><a href="#'.$widget_name.'-'.$language->language.'" aria-controls="'.$language->language.'" role="tab" data-toggle="tab">'.$language->language.'</a></li>
        @endforeach
        </ul>
        <div class="tab-content">
@endif
    
   
    
@foreach(self::$available_languages as $id => $language){
        
        @if(@$is_multilanguage)
            <div role="tabpanel" class="tab-pane tab-pane-translation '.($id == $active_code?'active':'').'" id="'.$widget_name.'-'.$language->language.'">
        @endif
        
        
       <div class="html-editor-container">
        @if($html_editor)
            <textarea name="'.$widget_name.'['.$id.']" class="html-editor">'.@$translations[$id].'</textarea>
        @else
        <input name="'.$widget_name.'['.$id.']" class="form-control" value="'.strip_tags(@$translations[$id]).'"/>
        @endif
            
            
        @if(@$is_multilanguage){
            <div class="text-center"><small>'.$language->language.'</small></div>
        @endif
        
        </div>
        
        @if(@$is_multilanguage){
            </div>
        @endif
        
@endforeach
    
    @if(@$is_multilanguage){
        </div>
    @endif
    
</div>
    
   
 