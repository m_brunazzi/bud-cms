<h4>Gruppi autorizzati</h4>

<?php
$groups_selected = [];
if(isset($record)){
    foreach($record->authorizedGroup as $group){
        $groups_selected[] = $group->id;

    }
}
?>

@if(isset($options_group) && !empty($options_group))
   @foreach($options_group as $id => $name)
        @if($id)
        <div class="checkbox">
            <label>
                {!! Form::checkbox('authorized_group['.$id.']', $id, in_array($id, $groups_selected)) !!}
            {{ $name }}
            </label>
        </div>
        @endif
   @endforeach

   <p><small>
       Non specificando nessun gruppo, il file sarà disponibile per tutti i visitatori, anche non autenticati.
   </small></p>

@endif