<div class="container text-center">
    
    @if(isset($current_user))
        {{ $current_user->name }} -  
    @endif
    &copy; {!! date('Y') !!} Bloom Design
    
</div>