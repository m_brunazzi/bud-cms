<div class="row form-header">
    <div class="col-sm-12">

        
        @if(isset($record->id))
        <div class="record-info pull-right">
            <small>Creazione: <strong>{!! $record->created_at !!}</strong></small><br/>
            <small>Modifica: <strong>{!! $record->updated_at !!}</strong></small>
        </div>
        @endif

        <h1>
            @if(@isset($title))
                {!! $title !!}
            @else
                Nuovo Record
            @endif

            @if(@isset($link))
                <a href="{{ $link }}" target="_blank"><span class="glyphicon glyphicon-link"></span></a>
            @endif
        </h1>

    
        
    </div>    
</div>