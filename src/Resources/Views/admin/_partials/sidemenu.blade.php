
      <ul class="sidebar-nav nav-stacked" id="menu">
        
          
        
          
        <li class="dropdown">
          <span class="fa-stack fa-lg pull-left"><i class="fa fa-database fa-stack-1x "></i></span>
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Contenuti <span class="caret"></span></a>
          <ul class="nav-stacked" role="menu">
            <li><a href="{!! URL::route('admin.Article.index') !!}">Articoli</a></li>
            <li><a href="{!! URL::route('admin.Category.index') !!}">Categorie</a></li>
            <li><a href="{!! URL::route('admin.Carousel.index') !!}">Carousel</a></li>
            <li><a href="{!! URL::route('admin.Tag.index') !!}">Tags</a></li>
            <li class="divider"></li>
            
            <li><a href="{!! URL::route('admin.Menu.index') !!}">Menu</a></li>
          </ul>
        </li>
          
        <li class="dropdown">	
        	<span class="fa-stack fa-lg pull-left"><i class="fa fa-users fa-stack-1x "></i></span>
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Autenticazione <span class="caret"></span></a>
          <ul class="nav-stacked" role="menu">
            <li><a href="{!! URL::route('admin.User.index') !!}">Utenti</a></li>
            <li><a href="{!! URL::route('admin.Group.index') !!}">Gruppi</a></li>
          </ul>
        </li>

        <li class="dropdown">
          <span class="fa-stack fa-lg pull-left"><i class="fa fa-files-o fa-stack-1x "></i></span>
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Allegati <span class="caret"></span></a>
          <ul class="nav-stacked" role="menu">            
            
            <li><a href="{!! URL::route('admin.Attachment.index') !!}">Archivio</a></li>
            <li><a href="{!! URL::route('admin.AttachmentType.index') !!}">Tipi</a></li>
          </ul>
        </li>
        
        <li class="dropdown">
        	<span class="fa-stack fa-lg pull-left"><i class="fa fa-gear fa-stack-1x "></i></span>
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Configurazione <span class="caret"></span></a>
          <ul class="nav-stacked" role="menu">            
            <li><a href="{!! URL::route('admin.Language.index') !!}">Lingue</a></li>
            <li><a href="{!! URL::route('admin.Option.index') !!}">Opzioni</a></li>
            <li><a href="{!! URL::route('admin.Translation.index') !!}">Traduzioni</a></li>
            
          </ul>
        </li>
        
        
        <li>
        	<span class="fa-stack fa-lg pull-left"><i class="fa fa-power-off fa-stack-1x "></i></span>
        	<a href="{!! route('rpc.logout') !!}">Disconnessione</a>
    	</li>
      </ul>
     

   <script> 

     // SIDEBAR
    // 
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

    $("#menu-toggle-2").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled-2");
        $('#menu ul').hide();
    });


    function initMenu() {
      $('#menu ul').hide();
      $('#menu ul').children('.current').parent().show();
      //$('#menu ul:first').show();
      $('#menu li a').click(
        function() {
          var checkElement = $(this).next();
          if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
            return false;
        }
        if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
            $('#menu ul:visible').slideUp('normal');
            checkElement.slideDown('normal');
            return false;
        }
    }
    );
  }

    



     $(document).ready(function(){

      //initMenu();  

      // Adjust sidebar wrapper height
      var viewport_d = $(document).height();
      var viewport_h = $('#wrapper').height();
      $('#sidebar-wrapper').height(viewport_d > viewport_h?viewport_d:viewport_h);

      // Setting active li 
      var active_menu_item = $('.sidebar-nav li a[href*="' + window.location.href + '"]');
      if(active_menu_item.length > 0){
        active_menu_item.parent('li').addClass('active');
        var par = active_menu_item.closest('ul.nav-stacked');
        par.show();

      }
      else{
        $('.sidebar-nav li a').each(function(){
        var link = $(this).attr('href');
        
        if((window.location.href).match('^' + link)){
          $(this).parent('li').addClass('active');
            var par = $(this).closest('ul.nav-stacked');
            par.show();
            return false;
        }

        });

      }
     });

     </script>