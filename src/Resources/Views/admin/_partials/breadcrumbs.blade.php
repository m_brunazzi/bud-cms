@if(isset($breadcrumbs) && count($breadcrumbs) > 0)
@php($i = 0)
<ol class="breadcrumb">

  @foreach($breadcrumbs as $key => $value)
  @if(++$i == count($breadcrumbs))
  <li class="breadcrumb-item active">{!! $key !!}</li>
  @else
  <li class="breadcrumb-item"><a href="{!! $value !!}">{!! $key !!}</a></li>

  @endif
  @endforeach
</ol>
@endif
