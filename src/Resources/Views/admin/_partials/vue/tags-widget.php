
<?php
if(isset($record)):
	?>

	<tags-manager  taggable-id="<?php echo $record->id; ?>" taggable-type="<?php echo (get_class($record)); ?>" rpc-url="<?php echo route('rpc.taggable.manager') ?>"></tags-manager>

	<?php
else:
	?>

	<div class="alert alert-info">&Egrave; necessario salvare il record prima di aggiungere i tags</div>

	<?php
endif;
?>