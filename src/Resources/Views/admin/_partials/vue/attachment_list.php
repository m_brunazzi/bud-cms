<div id="attachments-container" class="vue-element">
    <attachment-list attachmentable-id="<?php echo $record->id; ?>" attachmentable-type="<?php echo (get_class($record)); ?>" rpc-url="<?php echo route('rpc.attachmentable.manager') ?>" edit-url="<?php echo route('admin.Attachment.edit', '%id') ?>" destination-folder="<?php echo Option::value('files_folder'); ?>"></attachment-list>
</div>