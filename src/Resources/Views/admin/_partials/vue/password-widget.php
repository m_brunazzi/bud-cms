<div class="vue-element">

<?php
if(isset($record)):

?>


<password-widget  passwordable-id="<?php echo $record->id; ?>" passwordable-type="<?php echo (get_class($record)); ?>" rpc-url="<?php echo route('rpc.passwordable.manager') ?>" :checked="<?php echo (!empty($record->password)?'true':'false') ?>"></password-widget>

<?php
else:
?>
<div class="alert alert-info">&Egrave; necessario salvare il record prima di creare una password</div>
<?php
endif;
?>
</div>
