<div id="metadata-container" class="vue-element">
    
<?php
if(isset($record)):
	?>

	<metadata-widget  hasmeta-id="<?php echo $record->id; ?>" hasmeta-type="<?php echo (get_class($record)); ?>" rpc-url="<?php echo route('rpc.metadata.manager') ?>"></tags-manager>

	<?php
else:
	?>

	<div class="alert alert-info">&Egrave; necessario salvare il record prima di gestire i metadati</div>

	<?php
endif;
?>
</div>