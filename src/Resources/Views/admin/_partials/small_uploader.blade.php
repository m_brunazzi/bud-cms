
@if(isset($record) && !empty($record->id) && isset($form['model_name']) && !empty($form['model_name']))
<div id="small-attachment-uploader" class="dm-uploader text-center">
    <p><span class="glyphicon glyphicon-upload"></span></p>
    <p class="hidden-sm"><small>Trascinare qui i file</small></p>
    <label class="btn-browse btn btn-default btn-sm">
        <span>Seleziona</span>
        <input type="file" name="files[]" class="upload" multiple="multiple"/>
    </label>
    
    
</div>


<div class="form-upload-progress-container">
    <div id="small-attachment-upload-progress" >
        <div class="progress">
            <div id="small-attachment-upload-progress-bar" class="progress-bar progress-bar-striped" style="width: 100%">
                 In attesa
            </div>
        </div>
    </div>
</div>


<!--
<input type="file" name="files[]"  title="Seleziona">
-->
<br/>


<script>
       // attivo il widget per l'upload
       $("#small-attachment-uploader").dmUploader({
                                        url : '{{ route('rpc.attachment.upload') }}',
                                        allowedTypes : '*',
                                        dataType : 'json',
                                        fileName : 'file',
                                        extraData : {'attachmentable_type' : '{{ addslashes (get_class($record)) }}', 'attachmentable_id' : '{{ $record->id }}', 'destination_path' : '{{ Option::value('files_folder') }}'},
                                        onNewFile: function(){
                                            toastr.info('Caricamento del file in corso')
                                        },
                                        onComplete: function(){
                                            updateAttachments()
                                        },
                                        onUploadSuccess : function(id, data){
                                            
                                            if(data.status == '1')
                                                toastr.success( data.file.name + ' caricato correttamnte')
                                            else
                                                toastr.error( data.message)
                                            
                                        },
                                        onUploadError : function(id, message){
                                            toastr.error( message)
                                        },
                                        onFileTypeError: function(file){
                                            toastr.error( 'Formato non valido')
                                        }
        });
</script>
@endif