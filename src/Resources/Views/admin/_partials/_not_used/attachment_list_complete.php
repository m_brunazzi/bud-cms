<div id="attachments-container">
    <attachment-list attachmentable-id="<?php echo $record->id; ?>" attachmentable-type="<?php echo (get_class($record)); ?>"></attachment-list>
</div>


<template id="attachment-list">
    <div class="attachment-list">
        <div class="row">
            <div class="col-sm-12">
                <select v-model="filter" class="form-control pull-right input-sm">
                    <option value="all">Tutti</option>
                    <option value="images">Solo immagini</option>
                    <option value="no_images">Tranne immagini</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-9">

                <div v-if="loading" class="pull-right">Caricamento in corso</div>
                <table class="table table-hover table-condensed attachment-table" id="attachments-table">
                    <tbody>
                        <tr v-for="item in rows" v-if="(filter == 'all') || (filter == 'images' && item.is_image == '1') || (filter == 'no_images' && item.is_image != '1')">
                            <td><span class="badge">{{ item.id }}</span></td>
                            <td>
                                <div v-if="item.is_image == 1">
                                    <a :href="item.filename" target="_blank"><img class="img-thumbnail" :src="item.thumbnail"></a>
                                </div>
                            </td>
                            <td>
                                <strong>
                                    <span class="editable" id="name" data-type="text" :data-pk="item.id" data-url="<?php echo route('rpc.attachment.rename') ?>" data-title="Inserisci il nome del file">
                                        {{ item.name }}
                                    </span>
                                </strong>
                                <br/>
                                
                                <small><strong>{{ Math.floor(item.size / 1024) }}  Kb</strong>  ({{ item.mime_type }})</small>
                                &nbsp;<a :href="item.filename" target="_blank"><span class="glyphicon glyphicon-share-alt"></span>
                            </td>
                            <td>
                                <div v-if="item.is_image == 1" class="checkbox">
                                    <label><input type="checkbox" name="main" value="1" @click="setMainPicture(item.id)" class="cb_attachment_main" :checked="item.pivot.is_image_main == 1?'checked':''"/><small>Principale</small></label>
                                </div>
                            </td>
                            <td class="text-right">
                                <button class="btn btn-warning btn-sm btn-detach-attachment" @click="detach(item.id)">Separa</button>&nbsp;
                                <button class="btn btn-danger btn-sm btn-delete-attachment" @click="remove(item.id)">Cancella</button>
                            </td>

                        </tr>
                    </tbody>
                </table>

            </div>
            <div class="col-sm-3">
                <attachment-uploader v-on:update="loadAttachments" :attachmentable-id="attachmentableId" :attachmentable-type="attachmentableType"></attachment-uploader>
                <hr/>
                <attachment-browser v-on:update="loadAttachments" :attachmentable-id="attachmentableId" :attachmentable-type="attachmentableType"></attachment-browser>
            </div>
        </div>
</template>




<template id="attachment-uploader">
    <div class="attachment-uploader">
        <div id="small-attachment-uploader" class="dm-uploader text-center">
            <p><span class="glyphicon glyphicon-upload"></span></p>
            <p class="hidden-sm"><small>Trascinare qui i file</small></p>
            <label class="btn-browse btn btn-default btn-sm">
                <span>Seleziona</span>
                <input type="file" name="files[]" class="upload" multiple="multiple"/>
            </label>
        </div>
        <div class="form-upload-progress-container">
            <div id="small-attachment-upload-progress" >
                <div class="progress">
                    <div id="small-attachment-upload-progress-bar" class="progress-bar progress-bar-striped" style="width: 100%">
                        In attesa
                    </div>
                </div>
            </div>
        </div>
    </div>
</template>








<template id="attachment-browser">
    <div class="attachment-browser">
        <button class="btn btn-primary btn-block btn-lg " @click="showModal">Archivio allegati</button>

        <div class="modal fade" id="attachments_browser" tabindex="-1" role="dialog" aria-labelledby="Sfoglia allegati">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" ><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Archivio allegati</h4>
                    </div>
                    <div class="modal-body">
                        <div class="loading-overlay" v-if="loading"></div>
                        <div class="row">
                            <div  class="col-sm-3 attachment-browser-cell-container" v-for="item in files">
                                    <div  class="attachment-browser-cell">
                
                                    <div class="img-background" v-if="item.is_image" :style=" {backgroundImage: 'url('+item.thumbnail+')'} "></div>
                                    
                                    <div class="caption">
                                        <strong><a href="" class="btn-attach-attachment" :rel="item.id">{{ item.name }}</a></strong><br/>
                                        <a href="#" data-toggle="tooltip" data-placement="right" :title="item.filename"><span class="glyphicon glyphicon-info-sign"></span></a>&nbsp;
                                        <small>{{ Math.floor(item.size / 1024) }} Kb | {{ item.mime_type }}</small>
                                        &nbsp;<a :href="item.filename" target="_blank"><span class="glyphicon glyphicon-share-alt"></span></a>
                                    </div>
               
                                    <button v-if="item.is_attached" class="btn btn-action btn-danger btn-sm "  @click="detach(item.id)"><span class="glyphicon glyphicon-minus"></span></button>
                                    <button v-else class="btn btn-action btn-primary btn-sm " @click="attach(item.id)"><span class="glyphicon glyphicon-plus"></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="">
                            <label>Ricerca</label>
                            <input type="text" class="form-control input-sm" v-model="search"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</template>




<script>

// register

    Vue.component('attachment-browser', {
        template: '#attachment-browser',
        props: ['attachmentableId', 'attachmentableType'],
        methods: {
            loadAttachments : function(){
                self = this;
                self.loading = true;
                $.ajax({
                    url: '/rpc/attachment/browse',
                    data: {
                        'attachmentable_type': self.attachmentableType,
                        'attachmentable_id': self.attachmentableId,
                        'search': self.search
                    },
                    success: function(response){
                        self.files = response.data;
                        self.loading = false;
                        
                    },
                    dataType: 'json'
                });
                self.$emit('update');
            },
            showModal: function () {
                this.loadAttachments();
                $('#attachments_browser').modal();  
            },
            attach: function(id){
                this.loading = true;
                var self = this;
                $.ajax({
                    url: '/rpc/attachment/attach',
                    data: {
                        'attachmentable_type': self.attachmentableType,
                        'attachmentable_id': self.attachmentableId,
                        'attachment_id': id
                    },
                    success: function (response) {
                        self.loading = false;
                        self.loadAttachments();
                    },
                    dataType: 'json'
                });
            },
            detach: function(id){
                this.loading = true;
                var self = this;
                $.ajax({
                    url: '/rpc/attachment/detach',
                    data: {
                        'attachmentable_type': self.attachmentableType,
                        'attachmentable_id': self.attachmentableId,
                        'attachment_id': id
                    },
                    success: function (response) {
                        self.loading = false;
                        self.loadAttachments();
                    },
                    dataType: 'json'
                });
            }
            
        },
        watch:{
            search: _.debounce(function(){
                
                this.loadAttachments();
            }, 500)
        },
        data: function () {
            data = {
                search: '',
                loading: false,
                files: []
            };
            return data;
        }
    });

    Vue.component('attachment-uploader', {
        template: '#attachment-uploader',
        props: ['attachmentableId', 'attachmentableType'],
        mounted: function () {
            this.setupUploader();
        },
        methods: {
            setupUploader: function () {
                self = this;

                $("#small-attachment-uploader").dmUploader({
                    url: '<?php echo route('rpc.attachment.upload') ?>',
                    allowedTypes: '*',
                    dataType: 'json',
                    fileName: 'file',
                    extraData: {
                        'attachmentable_type': self.attachmentableType,
                        'attachmentable_id': self.attachmentableId,
                        'destination_path': '<?php echo Option::value('files_folder') ?>'
                    },
                    onNewFile: function () {
                        toastr.info('Caricamento del file in corso')
                    },
                    onComplete: function () {
                        self.$emit('update');
                    },
                    onUploadSuccess: function (id, data) {

                        if (data.status == '1')
                            toastr.success(data.file.name + ' caricato correttamnte')
                        else
                            toastr.error(data.message)

                    },
                    onUploadError: function (id, message) {
                        toastr.error(message)
                    },
                    onFileTypeError: function (file) {
                        toastr.error('Formato non valido')
                    }
                }
                );
            }
        }
    });

    Vue.component('attachment-list', {
        template: '#attachment-list',
        props: ['attachmentableId', 'attachmentableType'],
        mounted: function(){
            $.fn.editable.defaults.mode = 'inline';
            $('.editable').editable();
        },
        created: function () {
            this.loadAttachments();
        },
        methods: {
            toastResponse: function (response) {

                if (response.status == '1') {
                    if (response.message != undefined)
                        toastr.success(response.message);
                } else {
                    if (response.message != undefined) {
                        toastr.error(response.message);
                    } else {
                        toastr.error('Errore nella richiesta');
                    }
                }
            },
            loadAttachments: function () {
                this.loading = true;
                var self = this;
                $.ajax({
                    url: '/rpc/attachment/collection',
                    data: {
                        'attachmentable_type': self.attachmentableType,
                        'attachmentable_id': self.attachmentableId
                    },
                    success: function (response) {
                        self.loading = false;
                        self.toastResponse(response);
                        self.rows = response.data;
                    },
                    dataType: 'json'
                });
            },
            setMainPicture(id) {
                this.loading = true;
                var self = this;
                is_image_main = true;
                $.ajax({
                    url: '/rpc/attachment/setMainImage',
                    data: {
                        'attachment_id': id,
                        'attachmentable_id': self.attachmentableId,
                        'attachmentable_type': params.attachmentableType,
                        'is_image_main': is_image_main
                    },
                    success: function (response) {
                        self.toastResponse(response);
                        this.loading = false;

                    },
                    dataType: 'json'
                });
            },
            detach(id) {
                if (confirm('Separare l\'immagine dal record?')) {
                    this.loading = true;
                    var self = this;
                    $.ajax({
                        url: '/rpc/attachment/detach',
                        data: {
                            'attachmentable_type': self.attachmentableType,
                            'attachmentable_id': self.attachmentableId,
                            'attachment_id': id
                        },
                        success: function (response) {
                            this.loading = false;
                            self.toastResponse(response);
                            self.loadAttachments();
                        },
                        dataType: 'json'
                    });
                }
            },
            remove(id) {
                if (confirm('Procedere con la cancellazione dell\'immagine?')) {
                    var self = this;
                    self.loading = true;
                    $.ajax({
                        url: '/rpc/attachment/delete',
                        data: {'id': id},
                        success: function (response) {
                            self.loading = false;
                            self.toastResponse(response);
                            self.loadAttachments();
                        },
                        dataType: 'json'
                    });
                }
            }
        },
        data: function () {

            data = {
                loading: false,
                filter: 'all',
                rows: []
            };

            return data;
        }
    })


    new Vue({
        el: '#attachments-container'
    })

</script>
