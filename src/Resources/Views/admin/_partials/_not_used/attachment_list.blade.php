@if(isset($record->id))
        

<div class="row">
    <div class="col-sm-9"> 
        
        <div class="pull-right">
        {!! Form::select('attachment_type', ['' => 'Mostra TUTTO', 'images' => 'Mostra solo le IMMAGINI', 'other' => 'Mostra gli ALLEGATI GENERICI'], null, ['class' => 'form-control']) !!}
        <br/>
        </div>
        
        
         <table class="table table-hover table-condensed attachment-table" id="attachments-table">
            <tbody>

            </tbody>
        </table>
    </div>
    <div class="col-sm-3">
        <div class="well">
            <p>&Egrave;  possibile caricare un nuovo file oppure associare al record un file gi&agrave; caricato</p>
            
            @include('bud::admin._partials.small_uploader')
            <hr/>
            
            <button id="attachments_browser_btn" class="btn btn-primary btn-block btn-lg ">Browser di allegati</button>
        </div>
        

    </div>
</div>
                    

@include('bud::admin._partials.modal', ['id' => 'attachments_browser', 'title' => 'Browser di allegati'])


<script>
    $(document).ready(function(){

            // inizializzo il servizio
            BiCi.init({
                baseUrl: '{!! url('/') !!}'
            });

            // mostro il browser di allegati
            $('#attachments_browser_btn').click(function(){
                $('#attachments_browser').modal();

                BiCi.attachmentBrowse({
                    onSuccess : function(response){
                        $('#attachments_browser .modal-body').html(drawAttachmentsBrowser(response.data));
                    }
                });

            });



            // le funzioni associate agli allegati sono disponibili solo se esiste un record associato   

            updateAttachments(); // aggiorno la tabella degli allegati




            // INTERAZIONI UTENTE
            // gestisco la cancellazione delle immagini
            $(document).on('click', '.btn-delete-attachment', function(e){
                BiCi.attachmentDelete({
                    id : $(e.target).attr('rel'),
                    onSuccess : function(response){                        
                            if(response.status == '1'){
                                toastr.success('File cancellato correttamente');
                                updateAttachments();
                            }
                            else{
                                toastr.error('Errore nella cancellazione del file');
                            }
                    }
                });
            });  

            // gestisco la scelta dell'immagine principale
            $(document).on('click', '.cb_attachment_main', function(e){
                var is_checked = $(e.target).is(':checked');
                
                // deseleziono gli altri
                if(is_checked == true){
                    var all = $('.cb_attachment_main').not($(e.target));
                    all.each(function(){
                        $(this).attr('checked', false);
                    });
                }
                
                // chiamo la funzione per aggiornare il record
                BiCi.setMainImage({
                    attachmentable_id : '<?= $record->id ?>',
                    attachmentable_type : '<?= addslashes(get_class($record)) ?>',
                    attachment_id : $(e.target).attr('data-attachment-id'),
                    is_image_main : is_checked,
                    onSuccess : function(response){
                        if(response.status == 1){
                            $('cb_attachment_' + response.attachment_id).attr('checked');
                            toastr.success(response.message);
                        }
                        else
                            toastr.error(response.message);
                        $('.cb_attachment_main').each(function(){
                            if( $(this).attr('data-attachment-id') == response.attachment_id){
                                $(this).attr('checked', 'checked');
                            }
                            else
                                $(this).removeAttr('disabled');
                        });
                    }
                });
            });

            // filtro per tipologia di allegato
            $(document).on('change', 'select[name=attachment_type]', function(e){
                var sel = $(e.target).val();
                switch(sel){
                    case 'images':
                        $('#attachments-table tr[data-is-image=0]').hide();
                        $('#attachments-table tr[data-is-image=1]').show();
                        break;
                    case 'other':
                        $('#attachments-table tr[data-is-image=1]').hide();
                        $('#attachments-table tr[data-is-image=0]').show();
                        break;
                    default:
                        $('#attachments-table tr').show();
                        break;
                }
            });


            // associo un file gia caricato
            $(document).on('click', '.btn-attach-attachment', function(e){
                var attachment_id = $(e.target).attr('rel');
                
                BiCi.attachmentAttach({
                    attachmentable_id : '<?= $record->id ?>',
                    attachmentable_type : '<?= addslashes(get_class($record)) ?>',
                    attachment_id : attachment_id,
                    onSuccess : function(response){
                         if(response.status == '1'){
                            updateAttachments();
                            toastr.success('File associato correttamente');
                        }
                        else{
                            updateAttachments();
                            toastr.error(response.message);
                        }
                    }
                });
                
            });
            
            // Separo un file gia caricato
            $(document).on('click', '.btn-detach-attachment', function(e){
                var attachment_id = $(e.target).attr('rel');
                
                BiCi.attachmentDetach({
                    attachmentable_id : '<?= $record->id ?>',
                    attachmentable_type : '<?= addslashes(get_class($record)) ?>',
                    attachment_id : attachment_id,
                    onSuccess : function(response){
                        if(response.status == '1'){
                            updateAttachments();
                            toastr.success('File separato correttamente');
                        }
                        else{
                            updateAttachments();
                            toastr.error(response.message);
                        }
                        
                    }
                });
                
            });
            
        
    });

    // FUNZIONI GENERALI DELLA PAGINA

        // aggiornamento delle immagini
        function updateAttachments(){
            BiCi.attachmentCollection({
                attachmentable_id : '<?= $record->id ?>',
                attachmentable_type : '<?= addslashes(get_class($record)) ?>',
                onSuccess : function(response){
                    drawAttachmentsTable(response.data)
                }
            });
        }

        // funzione per disegnare la tabella 
        function drawAttachmentsTable(model_attachments){
            var tableContent = '';

            for(i = 0; i < model_attachments.length; i++){
                
                //console.log(model_attachments); return;

                tableContent += '<tr rel="' + model_attachments[i].id + '" data-is-image="' + model_attachments[i].is_image + '">';
                tableContent += '<td class="text-center"><span class="glyphicon glyphicon-sort"></span></td>';
                if(model_attachments[i].is_image == '1'){
                    tableContent += '<td><a href="{!! asset('/') !!}' + model_attachments[i].filename + '"><img class="img-thumbnail" src="' + model_attachments[i].thumbnail + '"></a></td>';
                }
                else{
                    tableContent += '<td>&nbsp;</td>';
                }
                
                tableContent += '<td><span class="badge">' + model_attachments[i].id + '</span></td>';
                tableContent += '<td>'
                tableContent += '<strong><a href="#" class="editable" id="name" data-type="text" data-pk="' + model_attachments[i].id + '" data-url="{!! route('rpc.attachment.rename') !!}" data-title="Inserisci il nome del file">' + model_attachments[i].name + '</a></strong><br/>';
                // tableContent += '<small>' + model_attachments[i].mime_type + ' | ' + Math.floor(model_attachments[i].size / 1024) + 'Kb</small>';
                tableContent += '<small><strong>' + Math.floor(model_attachments[i].size / 1024) + 'Kb</strong> | ' + model_attachments[i].mime_type + '</small>';
                tableContent += '&nbsp;<a href="'+ BASE_URL + '/' + model_attachments[i].filename + '" target="_blank"><span class="glyphicon glyphicon-share-alt"></span>'
                tableContent += '</td>;'
                if(model_attachments[i].is_image == '1'){
                    tableContent += '<td><label class="radio"><input type="checkbox" name="main" value="1" data-attachment-id="' + model_attachments[i].id + '" id="cb_attachment_' + model_attachments[i].id + '" class="cb_attachment_main"'+(model_attachments[i].pivot.is_image_main == '1'?' checked="checked"':'')+'/><small>Principale</small></label></td>';
                }
                else{
                    tableContent += '<td>&nbsp;</td>';
                }
                tableContent += '<td class="text-right">';
                tableContent += '<button class="btn btn-warning btn-sm btn-detach-attachment" rel="' + model_attachments[i].id + '">Separa</button>&nbsp;';
                tableContent += '<button class="btn btn-danger btn-sm btn-delete-attachment" rel="' + model_attachments[i].id + '">Cancella</button>';
                
                tableContent += '</td>';
                tableContent += '</tr>';
            }

            $('#attachments-table tbody').html(tableContent);
            $.fn.editable.defaults.mode = 'inline';
            $('.editable').editable();
            $('#attachments-table tbody').sortable({
                update : function(){ 
                                var attachment_ids = [];
                                $('#attachments-table tbody tr').each(function(index){
                                    attachment_ids.push($(this).attr('rel'));
                                });
                                BiCi.setAttachmentsOrder({
                                    attachmentable_id : '<?= $record->id ?>',
                                    attachmentable_type : '<?= addslashes(get_class($record)) ?>',
                                    attachment_ids : attachment_ids,
                                    onSuccess : function(response){
                                        if(response.status == 1){
                                            toastr.success(response.message);
                                        }
                                        else
                                            toastr.error(response.message);
                                    }
                                });
                            }
            });
        }



function drawAttachmentsBrowser(model_attachments){
/*
            var tableContent = '<table class="table table-hovertable-condensed">';

            for(i = 0; i < model_attachments.length; i++){

                tableContent += '<tr rel="' + model_attachments[i].id + '" data-is-image="' + model_attachments[i].is_image + '">';
                
                tableContent += '<td><span class="badge">' + model_attachments[i].id + '</span></td>';
                tableContent += '<td>';
                tableContent += '<strong>' + model_attachments[i].name + '</strong><br/>';
                tableContent += '<small><strong>' + Math.floor(model_attachments[i].size / 1024) + 'Kb</strong> | ' + model_attachments[i].mime_type + '</small>';
                tableContent += '</td>';
               
                tableContent += '<td class="text-right">';
                tableContent += '<button class="btn btn-info btn-sm btn-attach-attachment" rel="' + model_attachments[i].id + '">Associa</button>';
                tableContent += '</td>';
                tableContent += '</tr>';
            }

            tableContent += '</table>';
            
            console.log(tableContent);
            
            return tableContent;
           */
              
            var browserContent = '<div class="row">';

            for(i = 0; i < model_attachments.length; i++){

                browserContent += '<div  class="col-sm-4 attachment-browser-cell-container">';
                browserContent += '<div  class="attachment-browser-cell">';
                
                browserContent += '<strong><a href="" class="btn-attach-attachment" rel="' + model_attachments[i].id + '">' + model_attachments[i].name + '</a></strong><br/>';
                browserContent += '<a href="#" data-toggle="tooltip" data-placement="right" title="' + model_attachments[i].filename + '"><span class="glyphicon glyphicon-info-sign"></span></a>&nbsp;';
                browserContent += '<small>' + Math.floor(model_attachments[i].size / 1024) + 'Kb | ' + model_attachments[i].mime_type + '</small>';
                browserContent += '&nbsp;<a href="'+ BASE_URL + '/' + model_attachments[i].filename + '" target="_blank"><span class="glyphicon glyphicon-share-alt"></span></a>';
                
               
                browserContent += '<a href="#" class="btn btn-primary btn-attach btn-sm btn-attach-attachment" rel="' + model_attachments[i].id + '"><span rel="' + model_attachments[i].id + '" class="glyphicon glyphicon-plus"></span></a>';
                browserContent += '</div>';
                browserContent += '</div>';
            }

            browserContent += '</div>';
            
            return browserContent;
        }


</script>        

@else
<div class="alert alert-info">Per gestire gli allegati è prima necessario salvare il record</div>
@endif 