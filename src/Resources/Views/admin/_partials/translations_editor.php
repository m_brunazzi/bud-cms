<div id="translation-container">

    <table class="table table-hover table-compressed">
        <tr v-for="item in keys">
            <td>{{ item }}</td>
        </tr>

    </table>

</div>


<script>

var tr = new Vue({
    el: '#translation-container',
    mounted: function(){
        this.loadTranslations();
    },
    methods: {
        loadTranslations: function(){

            self = this;

            Bud.rpcCall(self.rpcUrl, data, function(response){
                    self.loading = false;
                    self.keys = response.data.keys;
                    self.translations = response.data.translations;
                });
        }
    },
    data: {
        rpcUrl: '<?php echo route('rpc.translation.manager') ?>',
        keys: [],
        translations: [],
        loading: false


    }
});

</script>