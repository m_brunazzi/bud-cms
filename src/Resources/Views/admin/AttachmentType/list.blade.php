@extends('bud::admin._layouts.default')
 
@section('main')


@php
    $breadcrumbs = [
        'Dashboard' => route('admin.dashboard'),
        'Tipi di allegato' => ''
    ];
@endphp

@include('bud::admin._partials.breadcrumbs', ['breadcrumbs' => $breadcrumbs])


 
<h1>Tipi di allegato - Elenco</h1>

<div class="well">
    
    <div class="row">
        <div class="col-sm-4">
            Record trovati: <strong>{!! $rows->total() !!}</strong> - <a href="{!!URL::route('admin.AttachmentType.create')!!}" class="btn btn-primary m-1">Nuovo Record</a>
        </div>
        <div class="col-sm-8">
            <div class="pull-right">
                {!! Form::open(array('role' => 'form', 'route' => 'admin.AttachmentType.index', 'method' => 'GET', 'class' => 'form-inline'))!!}
                
                <div class="form-group">
                    <div class="input-group">
                    <input type="text" name="search" class="form-control m-1" value="{!! $search !!}">
                        <span class="input-group-btn">
                            <button class="btn btn-default m-1" type="submit">Cerca</button>
                        </span>
                    </div>
                </div>
                {!! Form::close() !!}
           </div>
        </div>
    </div>

</div>

{!! Notification::showAll() !!}

@if (count($rows) > 0)
    <table class="table table-hover ">
        <thead>
            <tr>
                <th>ID</th>
                <th>Online</th>
                <th>Etichetta</th>
                
                
                <th>&nbsp;</th>
            </tr>
        </thead>
        
    @foreach ($rows as $row)
        <tr>
            <td><span class="badge">{!! $row->id !!}</span></td>
            <td>{!! $row->online?'<span class="label label-icon label-success"><span class=" glyphicon glyphicon-ok-circle"></span></span>':'<span class="label label-danger label-icon"><span class="glyphicon glyphicon-remove-circle"></span></span>'!!}</td>
            <td><strong>{!! $row->label !!}</strong></td>
           
            <td class="text-right">
                <a href="{!!URL::route('admin.AttachmentType.edit', $row->id)!!}" class="btn btn-primary">Modifica</a>
                {!! Form::open(array('route' => array('admin.AttachmentType.destroy', $row->id), 'method' => 'delete', 'class' => 'form-delete')) !!}
                        <button type="submit" href="{!! URL::route('admin.AttachmentType.destroy', $row->id) !!}" class="btn btn-danger btn-mini">Cancella</button>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </table>
    {!! $rows->appends($query_string)->render() !!}
@else
    <div class="alert alert-info">Nessun record trovato</div>
@endif


@stop