@extends('bud::admin._layouts.basic_form')
 
@section('form_content')
 
@if (isset($record))
    {!! Form::model($record, array('route' => Array('admin.AttachmentType.update', $record->id), 'role' => 'form', 'method' => 'PUT')) !!}
@else
    {!! Form::open(array('role' => 'form', 'route' => 'admin.AttachmentType.store', 'method' => 'POST')) !!}
@endif
          

            <div class="row">
                <div class="col-sm-9">
                    
                    <div class="form-group">
                        {!! Form::label('label', 'Etichetta') !!}
                        {!! Form::text('label', null, Array('class'=> 'form-control input-lg ' ,'placeholder' => 'label')) !!}
                    </div>

                    <div class="form-group">
                    {!! Form::label('name', 'Nome') !!}
                    {!! Language::multilanguageWidget('name',  AttachmentType::decodeTranslations(@$record->name), null, false) !!}
                    </div>

                    <div class="form-group">
                    {!! Form::label('name', 'Descrizione') !!}
                    {!! Language::multilanguageWidget('description',  AttachmentType::decodeTranslations(@$record->description)) !!}
                    </div>

            
                </div>
                
                <!-- SIDEBAR -->
                <div class="col-sm-3">
                    <aside>
                       <br>
                        <div class="form-check">
                            {!! Form::checkbox('online', 1, null, ['class' => 'form-check-input']) !!}
                            <label class="form-check-label" for="online">
                            Online
                            </label>
                        </div>
                        
                        
                    </aside>
                </div>
            </div>
 
            
            
            <div class="form-actions">
                {!! Form::submit('Salva', array('class' => 'btn btn-block btn-primary')) !!}
            </div>
 
        {!! Form::close() !!}
@stop
