@extends('bud::admin._layouts.default')
 
@section('main')
 
<ol class="breadcrumb">
    <li><a href="{!! route('admin.dashboard') !!}">Dashboard</a></li>
    <li class="active">Traduzioni</li>
</ol>
 

<h1>Traduzioni - Elenco</h1>
<translation-editor rpc-url="{{ route('rpc.translation.manager') }}"></translation-editor>


@stop