@extends('bud::admin._layouts.content_form')
 
@section('form_content')
 
@if (isset($record))
    {!! Form::model($record, array('route' => Array('admin.Attachment.update', $record->id), 'role' => 'form', 'method' => 'PUT', 'files' => true)) !!}
@else
    {!! Form::open(array('role' => 'form', 'route' => 'admin.Attachment.store', 'method' => 'POST', 'files' => true)) !!}
@endif
          

            <div class="row">
                <div class="col-sm-9">
                    
                       <div class="form-group">
                        {!! Form::label('label', 'Etichetta') !!}
                        {!! Form::text('label', null, Array('class'=> 'form-control input-lg ' ,'placeholder' => 'label')) !!}
                    </div>


                    <label>File associato</label>
                    @include('bud::admin._partials.vue.file_selector', Array('filename' => @$record->filename, 'input_name' => 'filename'))
                    <hr/>
                    
                    <div class="form-group">
                    {!! Form::label('name', 'Nome') !!}
                    {!! Language::multilanguageWidget('name',  Attachment::decodeTranslations(@$record->name), null, false) !!}
                    </div>                   

                    <div class="form-group">
                    {!! Form::label('description', 'Descrizione') !!}
                    {!! Language::multilanguageWidget('description',  Attachment::decodeTranslations(@$record->description)) !!}
                    </div>
                    
            
                </div>
                
                <!-- SIDEBAR -->
                <div class="col-sm-3">
                    <aside >

                        <div class="form-check">
                            <div class="form-check">
                            {!! Form::checkbox('online', 1, null, ['class' => 'form-check-input']) !!}
                            <label class="form-check-label" for="online">
                            Online
                            </label>
                        </div>
                        </div>

                        <hr>

                        <div class="form-group">
                            <h4>Tipo</h4>
                            {!! Form::select('type_id', $options_type, null, ['class' => 'form-control']) !!}
                        </div>
                        
                        <hr>
                        

                        @include('bud::admin._partials.protectable_widget')

                        <hr>

                        <h4>Lingue</h4>
                        

                        <?php
                        $languages_selected = [];
                        if(isset($record)){
                            foreach($record->language as $language){
                                $languages_selected[] = $language->id;

                            }
                        }
                        ?>

                        @if(isset($options_language) && !empty($options_language))
                           @foreach($options_language as $id => $name)
                                @if($id)
                                <div class="checkbox">
                                    <label>
                                        {!! Form::checkbox('language_ids['.$id.']', $id, in_array($id, $languages_selected)) !!}
                                    {{ $name }}
                                    </label>
                                </div>
                                @endif
                           @endforeach

                           <p><small>
                               Non specificando nessuna lingua, il file sarà disponibile sempre
                           </small></p>

                        @endif



                        @if(isset($record->id) && $record->filename != '')
                        <hr>
                        <h4>Info File</h4>
                        <div class="attachment-details">
                            <div class="row">
                                <div class="col-sm-4">Filename originale</div>
                                <div class="col-sm-8 text-right"><strong>{{ $record->original_filename }}</strong></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">Estensione</div>
                                <div class="col-sm-8 text-right"><strong>{{ $record->extension }}</strong></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">Mime Type</div>
                                <div class="col-sm-8 text-right"><strong>{{ $record->mime_type }}</strong></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">Dimensione</div>
                                <div class="col-sm-8 text-right"><strong>{{ floor($record->size/1024) }} Kb</strong></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">Immagine</div>
                                <div class="col-sm-8 text-right"><strong>{{ $record->is_image?'SI':'NO' }}</strong></div>
                            </div>
                        </div>
                        @endif
                        
                        
                    </aside>
                </div>
            </div>
 
            
            
            <div class="form-actions">
                {!! Form::submit('Salva', array('class' => 'btn btn-block btn-primary')) !!}
            </div>
 
        {!! Form::close() !!}
@stop

