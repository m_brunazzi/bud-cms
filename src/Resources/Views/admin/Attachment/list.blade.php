@extends('bud::admin._layouts.default')
 
@section('main')


@php
    $breadcrumbs = [
        'Dashboard' => route('admin.dashboard'),
        'Allegati' => ''
    ];
@endphp

@include('bud::admin._partials.breadcrumbs', ['breadcrumbs' => $breadcrumbs])


 
<h1>Allegati - Elenco</h1>

<div class="well">
    
    <div class="row">
        <div class="col-sm-4">
            Record trovati: <strong>{!! $rows->total() !!}</strong> - <a href="{!!URL::route('admin.Attachment.create')!!}" class="btn btn-primary m-1">Nuovo Record</a>
        </div>
        <div class="col-sm-8">
            <div class="pull-right">
                {!! Form::open(array('role' => 'form', 'route' => 'admin.Attachment.index', 'method' => 'GET', 'class' => 'form-inline'))!!}

                <div class="form-group">
                    {!! Form::select("order", Array('' => 'Inserimento', 'label ASC' => 'Etichetta +', 'label DESC' => 'Etichetta -', 'original_filename ASC' => 'Nome file +', 'original_filename DESC' => 'Nome file -'), Input::get('order'), Array('class' => 'form-control m-1')) !!}
                </div>
                
                <div class="form-group">

                    {!! Form::select("type", Array('' => 'Tutti', 'images' => 'Solo immagini', 'no_images' => 'Tranne immagini'), null, Array('class' => 'form-control m-1')) !!}
                    
                </div>
                <div class="form-group">

                    <div class="form-check">
                        {!! Form::checkbox('no_relations', 1, null, ['class' => 'form-check-input']) !!}
                        <label class="form-check-label" for="active">
                        Solo non associati
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    
                    <div class="input-group">
                    <input type="text" name="search" class="form-control m-1" value="{!! $search !!}">
                        <span class="input-group-btn">
                            <button class="btn btn-default m-1" type="submit">Cerca</button>
                        </span>
                    </div>


                </div>
                {!! Form::close() !!}
           </div>
        </div>
    </div>

</div>

{!! Notification::showAll() !!}

@if (count($rows) > 0)
    <table class="table table-hover table-attachments">
        <thead>
            <tr>
                <th>&nbsp;</th>
                <!-- <th>Online</th> -->
                <th>Etichetta</th>
                <th>Filename originale</th>
                <th>Links</th>
                
                
                <th>&nbsp;</th>
            </tr>
        </thead>
        
    @foreach ($rows as $row)
        <tr>
            <td>
                @if($row->is_image)
                <a href="{{ asset($row->filename) }}" target="_blank"><img class="img-thumbnail" src="{{ Image::resize($row->filename, Option::value('admin_thumbnail_width'), Option::value('admin_thumbnail_height'), true) }}"></a>
                @else
                <div class="text-center attachment-extension">{{ $row->extension }}</div>
                @endif
                
            </td>

            <!--
            <td>{!! $row->online?'<span class="label label-icon label-success"><span class=" glyphicon glyphicon-ok-circle"></span></span>':'<span class="label label-danger label-icon"><span class="glyphicon glyphicon-remove-circle"></span></span>'!!}</td>
            -->

            <td>
                <p>
                    <strong>
                        {{ $row->label }}
                    </strong>
                </p>
                <p>
                    <span class="badge badge-primary">{{ $row->id }}</span>
                    <small><strong>{{ floor($row->size / 1024) }}  Kb</strong>  ({{ $row->mime_type }})</small>
                    &nbsp;<a href="{{ asset($row->filename) }}" target="_blank"><i class="fa fa-external-link"></i></a>
                </p>
            </td>

            <td>
                <span class="label label-info">{{ $row->original_filename }}</span>
            </td>

            <td>
                @foreach($row->link as $item)
                    <small><a href="{{ URL::to($item->link) }}" target="_blank">{{ URL::to($item->link) }}</a></small><br/>
                @endforeach
            </td>
           
            <td class="text-right">
                <a href="{!!URL::route('admin.Attachment.edit', $row->id)!!}" class="btn btn-primary">Modifica</a>
                {!! Form::open(array('route' => array('admin.Attachment.destroy', $row->id), 'method' => 'delete', 'class' => 'form-delete')) !!}
                        <button type="submit" href="{!! URL::route('admin.Attachment.destroy', $row->id) !!}" class="btn btn-danger btn-mini">Cancella</button>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </table>
    {!! $rows->appends(Input::except('page'))->render() !!}
@else
    <div class="alert alert-info">Nessun record trovato</div>
@endif


@stop