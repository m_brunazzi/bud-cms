@extends('bud::admin._layouts.content_form')
 
@section('form_content')




 
@if (isset($record))
    {!! Form::model($record, array('route' => Array('admin.Category.update', $record->id), 'role' => 'form', 'method' => 'PUT')) !!}
@else
    {!! Form::open(array('role' => 'form', 'route' => 'admin.Category.store', 'method' => 'POST')) !!}
@endif
          

            <div class="row">
                <div class="col-sm-9">
                    
                    <div class="form-group">
                        {!! Form::label('label', 'Etichetta') !!}
                        {!! Form::text('label', null, Array('class'=> 'form-control input-lg ' ,'placeholder' => 'label')) !!}
                    </div>

                    <div class="form-group">
                    {!! Form::label('name', 'Nome') !!}
                    {!! Language::multilanguageWidget('name',  Category::decodeTranslations(@$record->name), null, false) !!}
                    
                    </div>


                    <div class="form-group">
                    {!! Form::label('description', 'Descrizione') !!}
                    {!! Language::multilanguageWidget('description',  Category::decodeTranslations(@$record->description)) !!}
                    </div>
                </div>
                
                <!-- SIDEBAR -->
                <div class="col-sm-3">
                    <aside class="well">
                        <div class="checkbox">
                            <label for="online">{!! Form::checkbox('online', 1) !!} Online</label>
                        </div>
                       
                        <hr>
                        <h4>Genitore</h4>
                        @if(isset($record))
                            <typeahead-input rpc-url="{{  route('rpc.typeahead.query', ['article'])   }}" input-name="parent_id"  value-id="{{ @$record->father->id  }}" value-label="{{  @$record->father->label  }}"></typeahead-input>
                        @else

                            <div class="alert alert-info">&Egrave; necessario salvare il record prima di specificare il genitore</div>

                        @endif


                        <hr>
                        <h4>Tags</h4>
                        @include('bud::admin._partials.vue.tags-widget')


                    </aside>
                </div>
            </div>
            
            <div class="form-actions">
                {!! Form::submit('Salva', array('class' => 'btn btn-block btn-primary')) !!}
            </div>
 
        {!! Form::close() !!}
@stop


