@extends('bud::admin._layouts.not_logged')
 
@section('main')
 
<div class="row">
    <div class="col-sm-4 offset-sm-4">
        <div class="card card-login">
        <div class="card-header">
            <div class="card-title">LOGIN PAGE</div>
        </div>
        <div class="card-body">
            {!!  Notification::showAll() !!}

            {!! Form::open(array('role' => 'form', 'route' => 'rpc.login', 'method' => 'POST')) !!}

            <div class="form-group">
                {!! Form::label('email', 'Email') !!}
                {!! Form::text('email', null, Array('class'=> 'form-control' ,'placeholder' => 'Email')) !!}
            </div>

            <div class="form-group">
                {!! Form::label('password', 'Password') !!}
                {!! Form::password('password', Array('class'=> 'form-control' ,'placeholder' => 'Password')) !!}
            </div>

            <div class="form-actions">
                {!! Form::submit('Login', array('class' => 'btn btn-block btn-primary')) !!}
            </div>

            {!! Form::close() !!}
            </div>
        </div>
        
        
    </div>
</div>
@stop
