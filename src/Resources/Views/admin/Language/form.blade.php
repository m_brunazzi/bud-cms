@extends('bud::admin._layouts.content_form')
 
@section('form_content')
 


        @if (isset($record))
            {!! Form::model($record, array('route' => Array('admin.Language.update', $record->id), 'role' => 'form', 'method' => 'PUT', 'files' => true)) !!}
        @else
            {!! Form::open(array('role' => 'form', 'route' => 'admin.Language.store', 'method' => 'POST', 'files' => true)) !!}
        @endif
                    
            <div class="row">
                <div class="col-sm-7">
                    <div class="form-group">
                        {!! Form::label('language', 'Lingua') !!}
                        {!! Form::text('language', null, Array('class'=> 'form-control' ,'placeholder' => 'language')) !!}
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        {!! Form::label('code', 'Codice') !!}
                        {!! Form::text('code', null, Array('class'=> 'form-control' ,'placeholder' => 'code')) !!}
                    </div>
                </div>
                <div class="col-sm-2">
                <br>
                   <div class="form-check">
                        {!! Form::checkbox('online', 1, null, ['class' => 'form-check-input']) !!}
                        <label class="form-check-label" for="online">
                        Online
                        </label>
                    </div>
                </div>
                
            </div>

            
                    

            <div class="form-actions">
                {!! Form::submit('Salva', array('class' => 'btn btn-block btn-primary')) !!}
            </div>
               
 
        {!! Form::close() !!}
@stop


