@extends('bud::admin._layouts.default')
 
@section('main')
 
@php
    $breadcrumbs = [
        'Dashboard' => route('admin.dashboard'),
        'Lingue' => ''
    ];
@endphp

@include('bud::admin._partials.breadcrumbs', ['breadcrumbs' => $breadcrumbs])


<h1>Lingue - Elenco</h1>

<div class="">
    
    <div class="row">
        <div class="col-sm-9">
            Record trovati: <strong>{!! $rows->total() !!}</strong> - <a href="{!!URL::route('admin.Language.create')!!}" class="btn btn-primary m-1">Nuovo Record</a>
        </div>
        <div class="col-sm-3">
            <div class="pull-right">
                {!! Form::open(array('role' => 'form', 'route' => 'admin.Language.index', 'method' => 'GET')) !!}
                    <div class="input-group">
                    <input type="text" name="search" class="form-control m-1" value="{!! $search !!}">
                        <span class="input-group-btn">
                            <button class="btn btn-default m-1" type="submit">Cerca</button>
                        </span>
                    </div>
                {!! Form::close() !!}
           </div>
        </div>
    </div>

</div>

{!! Notification::showAll() !!}

@if (count($rows) > 0)
    <table class="table table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>Lingua</th>
                <th>Online</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        
    @foreach ($rows as $row)
        <tr>
            <td><span class="badge badge-primary">{!! $row->id !!}</span></td>
            <td><strong>{!! $row->language !!}</strong></td>
            <td class="text-center"><i class="fa {!! $row->online?'fa-check-square':'fa-remove' !!}"></i></td>
            
            <td class="text-right">
                
                <a href="{!!URL::route('admin.Language.edit', $row->id)!!}" class="btn btn-primary">Modifica</a>
                
                {!! Form::open(array('route' => array('admin.Language.destroy', $row->id), 'method' => 'delete', 'class' => "form-delete")) !!}
                        <button type="submit" href="{!! URL::route('admin.Language.destroy', $row->id) !!}" class="btn btn-danger btn-mini">Cancella</button>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </table>
    {!! $rows->appends($query_string)->render() !!}
@else
    <div class="alert alert-info">Nessun record trovato</div>
@endif


@stop