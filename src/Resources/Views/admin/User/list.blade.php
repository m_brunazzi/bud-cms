@extends('bud::admin._layouts.default')
 
@section('main')

@php
    $breadcrumbs = [
        'Dashboard' => route('admin.dashboard'),
        'Utenti' => ''
    ];
@endphp

@include('bud::admin._partials.breadcrumbs', ['breadcrumbs' => $breadcrumbs])


<h1>Utenti - Elenco</h1>
<p><strong>Attenzione</strong>: L'account <strong>{!! Option::value('admin_email');!!}</strong> non pu&ograve; essere modificato o cancellato.</p>

<div class="well well-sm">
    <div class="row">
        <div class="col-sm-12">
            Record trovati: <strong>{!! $rows->total() !!}</strong> - <a href="{!!URL::route('admin.User.create')!!}" class="btn btn-primary m-1">Nuovo Record</a>
        </div>
        
    </div>
</div>

{!! Notification::showAll() !!}

@if (count($rows) > 0)
    <table class="table  table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>Username</th>
                <th>Nome</th>
                <th>Attivo</th>
                <th>Amministratore</th>
                
                <th>&nbsp;</th>
            </tr>
        </thead>
    @foreach ($rows as $row)
        <tr>
            <td class="text-center"><span class="badge badge-primary">{!! $row->id !!}</span></td>
            
            <td><strong>{!! $row->email !!}</strong></td>
            <td>{!! $row->name !!}</td>
            <td class="text-center">
            @if($row->active)
            <span class="glyphicon glyphicon-ok-circle"></span>
            @endif  
            </td>
            <td class="text-center">
            @if($row->admin)
            <span class="glyphicon glyphicon-ok-circle"></span>
            @endif  
            </td>
          
          
          
            
            
            <td class="text-right">
                
                
                @if($row->id != $admin_user_id)
                <a href="{!!URL::route('admin.User.edit', $row->id)!!}" class="btn btn-primary">Modifica</a>
                {!! Form::open(array('route' => array('admin.User.destroy', $row->id), 'method' => 'delete', 'class' => "form-delete")) !!}
                        <button type="submit" href="{!! URL::route('admin.User.destroy', $row->id) !!}" class="btn btn-danger btn-mini">Cancella</button>
                {!! Form::close() !!}
                @else
                    <input type="button" disabled class="btn btn-danger btn-mini" value="Modifica">
                    <input type="button" disabled class="btn btn-danger btn-mini" value="Cancella">
                @endif
            </td>
                
        </tr>
    @endforeach
    </table>
    {!! $rows->appends($query_string)->render() !!}
@else
    <div class="alert alert-info">Nessun record trovato</div>
@endif


@stop