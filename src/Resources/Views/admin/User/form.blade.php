@extends('bud::admin._layouts.default')

@section('main')


@include('bud::admin._partials.breadcrumbs', ['breadcrumbs' => $form['breadcrumbs'] ])


{!! Notification::showAll() !!}

@if ($errors->any())
<div class="alert alert-danger">
    {!! implode('<br>', $errors->all()) !!}
</div>
@endif


{{-- INIZIO SEZIONE MAIN --}}


@if (isset($record->id))
{!! Form::model($record, array('role' => 'form', 'route' => Array('admin.User.update', $record->id), 'method' => 'PUT')) !!}
@else
{!! Form::open(array('role' => 'form', 'route' => 'admin.User.store', 'method' => 'POST')) !!}
@endif



@if ($errors->has('login'))
<div class="alert alert-error">{!! $errors->first('login', ':message') !!}</div>
@endif

<fieldset>
    <legend>
        Informazioni Utente
    </legend>

    <div class="row">
        <div class="col-sm-5">
            <div class="form-group">
                {!! Form::label('email', 'Email') !!}
                {!! Form::text('email', null, Array('class'=> 'form-control' ,'placeholder' => 'Email')) !!}
            </div>
        </div>
        <div class="col-sm-5">
            <div class="form-group">
                {!! Form::label('name', 'Nome completo') !!}
                {!! Form::text('name', null, Array('class'=> 'form-control' ,'placeholder' => 'Nome completo')) !!}
            </div>
        </div>


        <div class="col-sm-2">    
            <div class="form-group">
                <br/>
                <div class="form-check">
                    {!! Form::checkbox('active', 1, null, ['class' => 'form-check-input']) !!}
                    <label class="form-check-label" for="active">
                    Attivo
                    </label>
                </div>
            </div>
        </div>
        
    </div>

    <div class="row">


        <div class="col-sm-5">    
            <div class="form-group">
                {!! Form::label('password', 'Password') !!}
                {!! Form::password('password',  Array('class'=> 'form-control' ,'placeholder' => 'Password')) !!}
            </div>
        </div>
        <div class="col-sm-5">    
            <div class="form-group">
                {!! Form::label('password_confirmation', 'Conferma password') !!}
                {!! Form::password('password_confirmation',  Array('class'=> 'form-control' ,'placeholder' => 'Conferma password')) !!}
            </div>
        </div>
        <div class="col-sm-2">    
            <div class="form-group">
                <br/>
                <div class="form-check">
                    {!! Form::checkbox('admin', 1, null, ['class' => 'form-check-input']) !!}
                    <label class="form-check-label" for="admin">
                    Amministratore
                    </label>
                </div>
            </div>
        </div>

    </div>
</fieldset>

<fieldset>
    <legend>
        Gruppi
    </legend>

    @if(isset($groups) && count($groups) > 0)
    <div class="card">
        <div class="card-body">
            @foreach($groups as $item)
            <div class="col-sm-3 col-lg-2">
               
                <div class="form-check py-3">
                    {!! Form::checkbox('group['.$item->id.']', $item->id, in_array($item->id, $groups_ids)) !!}
                    <label class="form-check-label">
                    {{ $item->name }}
                    </label>
                </div>
            </div>
            @endforeach        
            
        </div>
    </div>
    

    @else
    <div class="alert alert-info">Nessun gruppo disponibile.</div>

    @endif
</fieldset>












<div class="form-actions">
    {!! Form::submit('Salva', array('class' => 'btn btn-block btn-primary')) !!}
</div>
<script type="text/javascript">
    $(document).ready(function () {

        $('.nav-pills li a').click(function () {
            $('#tabs_notification').removeClass('hidden');
        });

    });

</script>
{!! Form::close() !!}



@stop


