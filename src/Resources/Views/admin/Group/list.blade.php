@extends('bud::admin._layouts.default')
 
@section('main')
 
@include('bud::admin._partials.breadcrumbs', [
    'breadcrumbs' => [
        "Dashboard" => route('admin.dashboard'),
        "Gruppi" => ""
        ]
])

 

<h1>Gruppi di Account - Elenco</h1>
<!-- p><strong>Attenzione</strong>: Il gruppo degli amministratori non pu&ograve; essere modificato o cancellato.</p> -->

<div class="well well-sm">
    <div class="row">
        <div class="col-sm-12">
            Record trovati: <strong>{!! $rows->total() !!}</strong> - <a href="{!!URL::route('admin.Group.create')!!}" class="btn btn-primary m-1">Nuovo Record</a>
        </div>
        
    </div>
</div>

{!! Notification::showAll() !!}

@if (count($rows) > 0)
    <table class="table  table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nome</th>
                <th>Account</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
    @foreach ($rows as $row)
        <tr>
            <td><span class="badge">{!! $row->id !!}</span></td>
            
            <td><strong>{!! $row->name !!}</strong></td>
            <td class="text-center"><span class="badge badge-primary">{!! count($row->user) !!}</span></td>
            
            
            <td class="text-right">
                @if($row->id != $admin_group_id)
                <a href="{!!URL::route('admin.Group.edit', $row->id)!!}" class="btn btn-primary">Modifica</a>
                {!! Form::open(array('route' => array('admin.Group.destroy', $row->id), 'method' => 'delete', 'class' => "form-delete")) !!}
                        <button type="submit" href="{!! URL::route('admin.Group.destroy', $row->id) !!}" class="btn btn-danger btn-mini">Cancella</button>
                {!! Form::close() !!}
                @else
                    <input type="button" disabled class="btn btn-danger btn-mini" value="Modifica">
                    <input type="button" disabled class="btn btn-danger btn-mini" value="Cancella">
                @endif
            </td>
                
        </tr>
    @endforeach
    </table>
    {!! $rows->appends($query_string)->render() !!}
@else
    <div class="alert alert-info">Nessun record trovato</div>
@endif


@stop