@extends('bud::admin._layouts.default')

@section('main')





@if(@$form['breadcrumbs'])
  @include('bud::admin._partials.breadcrumbs', ['breadcrumbs' => $form['breadcrumbs']])
@endif

@include('bud::admin._partials.form_header', ['title' => @$form["title"]])

{!! Notification::showAll() !!}

@if ($errors->any())
<div class="alert alert-danger">
  {!! implode('<br>', $errors->all()) !!}
</div>
@endif

<ul class="nav nav-pills admin-pills" role="tablist">

  <li class="nav-item"><a href="#main" class="nav-link active" data-toggle="tab">Informazioni generali</a></li>
  <li class="nav-item"><a href="#taxonomy" class="nav-link" data-toggle="tab">Struttura</a></li>
</ul>

<div class="tab-content">
  <div class="tab-pane fade show active" id="main">
    @if (isset($record))
    {!! Form::model($record, array('route' => Array('admin.Menu.update', $record->id), 'role' => 'form', 'method' => 'PUT', 'files' => true)) !!}
    @else
    {!! Form::open(array('role' => 'form', 'route' => 'admin.Menu.store', 'method' => 'POST', 'files' => true)) !!}
    @endif
    @if ($errors->has('login'))
    <div class="alert alert-error">{!! $errors->first('login', ':message') !!}</div>
    @endif

    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          {!! Form::label('label', 'Etichetta') !!}
          {!! Form::text('label', null, Array('class'=> 'form-control' ,'placeholder' => 'label')) !!}
        </div>
      </div>

    </div>

                            <!--
                           <div class="form-group">
                            {!! Form::label('name', 'Nome') !!}
                            {!! Language::multilanguageWidget('name',  Article::decodeTranslations(@$record->name), null, false) !!}
                            </div>

                            <div class="form-group">
                            {!! Form::label('name', 'Descrizione') !!}
                            {!! Language::multilanguageWidget('description',  Article::decodeTranslations(@$record->description)) !!}
                            </div>

                          -->

                          <div class="form-actions">
                            {!! Form::submit('Salva', array('class' => 'btn btn-block btn-primary')) !!}
                          </div>
                          {!! Form::close() !!}
                        </div>

                        <div class="tab-pane fade" id="taxonomy">
                          @if(isset($record->id))



                          {{-- INIZIO PARTE DINAMICA PER NESTABLE --}}
                          <?php $counter = 0; ?>
                          <ul class="nav nav-tabs nav-tabs-translation">
                            @foreach($languages as $key => $value)

                            <li class="nav-item"><a href="#language_{!! $key !!}" class="nav-link{!! $counter==0?' active':'' !!}" data-toggle="tab">{!! $value !!}</a></li>


                            <?php $counter++ ?>
                            @endforeach
                          </ul>
                          <?php $counter = 0; ?>

                          <div class="tab-content">
                            @foreach($languages as $key => $value)
                            <div role="tabpanel" class="tab-pane tab-pane-translation{!! $counter==0?' active':'' !!}" id="language_{!! $key !!}">
                              <div id="menu_nodes_{!! $key !!}" data-language-id="{!! $key !!}">


                                <menu-editor menu-id="<?php echo $record->id; ?>" language-id="{!! $key !!}" rpc-url="<?php echo route('rpc.menu.manager'); ?>"></menu-editor>
                              </div>

                            </div>
                            <?php $counter++ ?>
                            @endforeach
                          </div>  




                          @else
                          <div class="alert alert-info">Prima di procedere con la creazione delle voci è necessario salvare il menu</div>
                          @endif
                        </div>
                      </div>
















                      @stop







