@if(isset($carousel) && count($carousel) > 0)
<div id="carousel--generic" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
    @for($i = 0; $i < count($carousel); $i++)
    <li data-target="#carousel--generic" data-slide-to="{{ $i }}" {{ $i == 0?' class="active"':''}}></li>
    @endfor
    </ol>
    
    
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        
        @for($i = 0; $i < count($carousel); $i++)
        <div class="item {{ $i == 0?' active':''}}">
            <img src="{{ Image::resize($carousel[$i]->image, 900,300) }}" alt="{{ $carousel[$i]->label }}" class="img-responsive">
            <div class="carousel-caption">
                <h2>{{ $carousel[$i]->name }}</h2>
                <p>{{ $carousel[$i]->summary }}</p>
            </div>
        </div>
        @endfor
          
        
    </div>
    
    
    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-generic" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-generic" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
    
</div>
@endif


