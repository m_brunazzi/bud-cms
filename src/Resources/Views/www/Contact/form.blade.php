@extends('www._layouts.default')

@section('title')
Contatti
@stop



@section('main')


<h1>Contatti</h1>




<div class="text">
    <p>Per richiedere informazioni vi preghiamo di compilare il modulo sottostante. I nostri incaricati vi risponderanno al più presto.</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <div>

        {!! Form::open(Array('route' => 'public.contact.result','role' => 'form', 'class' => 'form-contact')) !!}
        {!! Form::hidden('type', 'contact') !!}
        {!! Notification::showAll() !!}

        <div class="row">
            <div class="col-sm-4">

                 {!! Form::label('full_name','Nome e cognome *') !!}
                 {!! Form::text('full_name',null, Array('class' => 'form-control')) !!}

            </div>

            <div class="col-sm-4">

                 {!! Form::label('phone','Numero di telefono') !!}
                 {!! Form::text('phone',null, Array('class' => 'form-control')) !!}

            </div>

            <div class="col-sm-4">

                 {!! Form::label('email','Email *') !!}
                 {!! Form::text('email',null, Array('class' => 'form-control')) !!}

            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">

                 {!! Form::label('subject','Oggetto della richiesta') !!}
                 {!! Form::text('subject',null, Array('class' => 'form-control')) !!}

            </div>
        </div>

         <div class="row">
            <div class="col-sm-12">

                 {!! Form::label('body','Messaggio * ') !!}
                 {!! Form::textarea('body',null, Array('class' => 'form-control')) !!}

            </div>
        </div>
        <br/>
        <div class="form-actions">
            {!! Form::submit('Procedi', Array('class' => 'btn btn-primary btn-block btn-lg')) !!}
        </div>

        {!! Form::close() !!}

        <small><em>I campi indicati con l'asterisco * sono obbligatori.</em></small>
    </div>
    </div>
@stop