@extends('www._layouts.default')


@section('title')
Contatti - Esito Richiesta
@stop

@section('main')

<h1>Contatti</h1>

<div class="text">
    <div class="row">
        <div class="col-sm-9 text-left">
           {{ @$message }}
        </div>
    </div>
</div>

@stop