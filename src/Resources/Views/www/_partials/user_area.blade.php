<div class="pull-right">
    @if(!isset($user))

    {!! Form::open() !!}
    <div class="form-group">
        <label for="email">Email</label>
        {!! Form::text('email', '', ['class' => 'form-control input-sm']) !!}
    </div>
    
    <div class="form-group">
        <label for="password">Password</label>
        {!! Form::password('password',  ['class' => 'form-control input-sm']) !!}
    </div>

    {!! Form::close() !!}
    <a class="btn btn-info btn-sm btn-block" href="{{ route('facebook.login') }}">Login with Facebook</a>

    @else
    {{ $user->email }} - <a href="{!! route('rpc.logout') !!}">Esci</a>
    @endif
</div>
